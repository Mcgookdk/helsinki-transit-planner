//
//  FavoritePlace.m
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 03/11/2012.
//  Copyright (c) 2012 David McGookin. All rights reserved.
//

#import "FavoritePlace.h"

@implementation FavoritePlace



-(id) initWithDictionary:(NSDictionary *) dictionary{
    self = [super init];
    
    [self setTitle:[dictionary valueForKey:@"title"]];
    [self setPlaceCodes:[dictionary valueForKey:@"idCodes"]];
    [self setCoordinate:CLLocationCoordinate2DMake([(NSNumber *)[dictionary valueForKey:@"lat"] doubleValue],[(NSNumber *)[dictionary valueForKey:@"lon"] doubleValue])];
    [self setPlaceType:[(NSNumber *)[dictionary valueForKey:@"type"] intValue]];
    
    return self;
}
-(NSDictionary *) dictionaryRepresentationOfPlace{
    NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] init];
    
    [tempDict setValue:[self title] forKey:@"title"];
    [tempDict setValue:[self placeCodes] forKey:@"idCodes"];
    [tempDict setValue:[NSNumber numberWithDouble:self.coordinate.latitude] forKey:@"lat"];
    [tempDict setValue:[NSNumber numberWithDouble:self.coordinate.longitude] forKey:@"lon"];
    [tempDict setValue:[NSNumber numberWithInt:[self placeType]] forKey:@"type"];
    return tempDict;
}

-(NSString *) stringRepresentation{
    return [NSString stringWithFormat:@"%@ lat:%f lon%f indexCodes%@, placeType=%d", self.title, self.coordinate.latitude, self.coordinate.longitude, self.placeCodes, self.placeType];
}


//returns true iff the stop code given is in this stop
-(BOOL) hasStopCode:(NSString *) code{
    return [_placeCodes containsObject:code];
}

-(BOOL) hasStopCodes:(NSArray *) codes{
    for(NSString * code in codes){
        if([self hasStopCode:code]){
            return YES;
        }
    }
    return NO;
}



-(NSString*) subtitle{
    return @"";
}



@end

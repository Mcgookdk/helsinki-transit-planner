//
//  RecentGeoPlace.m
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 25/03/2013.
//  Copyright (c) 2013 David McGookin. All rights reserved.
//

#import "RecentGeoPlace.h"

@implementation RecentGeoPlace


-(id) initWithGeoPlace:(GeoPlace *) place andDate:(NSDate *) date{
    self = [super init];
    [self setLastUsedDate:date];
    [self setCoordinate:[place coordinate]];
    [self setName:[place name]];
    [self setAddress:[place address]];
    return self;
}

-(id)initWithDictionary:(NSDictionary *) dictionary{
    self = [super init];
    
    
    [self setName:[dictionary valueForKey:@"name"]];
    [self setCoordinate:CLLocationCoordinate2DMake([(NSNumber *)[dictionary valueForKey:@"lat"] doubleValue],[(NSNumber *)[dictionary valueForKey:@"lon"] doubleValue])];
    [self setAddress:[dictionary valueForKey:@"address"]];
    if ([self address] == nil){
        [self setAddress:@""];
    }
    [self setLastUsedDate:[dictionary valueForKey:@"last_used_date"]];

    return self;
}
-(NSDictionary *) dictionaryRepresentationOfRecentGeoPlace{
    NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] init];
    
    [tempDict setValue:[self name] forKey:@"name"];
    [tempDict setValue:[NSNumber numberWithDouble:self.coordinate.latitude] forKey:@"lat"];
    [tempDict setValue:[NSNumber numberWithDouble:self.coordinate.longitude] forKey:@"lon"];
    [tempDict setValue:[self address] forKey:@"address"];
    [tempDict setValue:self.lastUsedDate forKey:@"last_used_date"];
    return tempDict;
}




@end

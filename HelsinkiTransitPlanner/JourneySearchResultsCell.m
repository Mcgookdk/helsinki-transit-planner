//
//  JourneySearchResultsCell.m
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 24/07/2013.
//  Copyright (c) 2013 David McGookin. All rights reserved.
//

#import "JourneySearchResultsCell.h"
#import "TransitIconAndServiceSubViewController.h"
@implementation JourneySearchResultsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void) updateJourneyCell{
// Do any additional setup after loading the view from its nib.
    
    [_currentContentView removeFromSuperview];
    _currentContentView = nil;
    
   
if(_journeyRoute != nil){
    JourneyLeg *firstLeg = [[_journeyRoute journeyLegs] objectAtIndex:0];
    JourneyLeg *lastLeg = [[_journeyRoute journeyLegs] objectAtIndex:[[_journeyRoute journeyLegs] count]-1 ];
    
    NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
    [timeFormat setDateFormat:@"HH:mm"];
    
    [_arrivalTimeLabel setText:[timeFormat stringFromDate:[lastLeg arrivalDate]]];
    [_departureTimeLabel setText:[timeFormat stringFromDate:[firstLeg departureDate]]];
    NSTimeInterval duration = [[lastLeg arrivalDate] timeIntervalSinceDate:[firstLeg departureDate]];
    int durationHours = (int) duration/(60*60);
    int durationMinutes = ((int) duration % (60*60))/60;
    
    NSString *durationString = [NSString stringWithFormat:@"%d%@ %d%@", durationHours, NSLocalizedString(@"hrs", @"hrs abreviation"), durationMinutes, NSLocalizedString(@"min", @"min abreviation")];
    [_journeyTimeLabel setText:durationString];
    
    float xSet = 15.0f;
    float ySet = 0.0f;
    float xMod = 35.0f;
    
    //create a subview and place the individual transit legs into this
    CGRect contentFrame = CGRectMake(0.0, 30.0, self.frame.size.width, 50.0);
    _currentContentView = [[UIView alloc] initWithFrame:contentFrame];
    for(JourneyLeg *leg in [_journeyRoute journeyLegs]){
     
     
     NSLog(@"ADDING THE SUBVIEW for leg %@", leg);
     TransitIconAndServiceSubViewController *transitSvc = [[TransitIconAndServiceSubViewController alloc] init];
     [transitSvc setTransitType:[leg transitType]];
     if([leg transitType] != kWalk){
     NSLog(@"NOT WALKING");
     [transitSvc setTransitServiceCode:[[leg service] userReadableCode]];
     }else{
     [transitSvc setTransitServiceCode:[NSString stringWithFormat:@"%0.1f", [leg legLengthInMeters]/1000.0]]; //convert to km for display
     
     }
     CGRect frame = transitSvc.view.frame;
     frame.origin.x=xSet;
     frame.origin.y=ySet;
     xSet+=xMod;
     [transitSvc.view setFrame:frame];
     
     [_currentContentView addSubview:transitSvc.view];
     }
    [self addSubview:_currentContentView];
}


}
-(NSString *) reuseIdentifier{
    return @"JourneySearchResultsCell";
}
@end

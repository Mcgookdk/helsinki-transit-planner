//
//  FavoritesViewController.m
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 03/11/2012.
//  Copyright (c) 2012 David McGookin. All rights reserved.
//

#import "FavoritesViewController.h"
#import "AddNewFavoriteViewController.h"
#import "ApplicationUtilities.h"
#import "ReittiopasQueryProcessor.h"
#import "JourneySearchResultsViewController.h"
#import "TransitModelUtilities.h"
#import "ApplicationUtilities.h"
//#import "GADMasterViewController.h"

#define NO_TABLE_ROW_SELECTED -1

@interface FavoritesViewController()
-(void) showNoFavoritesView:(BOOL) showView;
-(void) checkFavoritesStatus;
@end

@implementation FavoritesViewController


-(void) viewDidLoad{
    [super viewDidLoad];
   
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    _locationServer = [[LocationServer alloc] init];
    [_locationServer setDelegate:self];
    _currentAuthorisationStatus = kCLAuthorizationStatusNotDetermined;
    
    _favoritesTableDelegate = [[FavoritesTableDelegate alloc] init];
    [_favoritesTableDelegate setDelegate:self];
    [_favoritesTableView setDataSource:_favoritesTableDelegate];
    [_favoritesTableView setDelegate:_favoritesTableDelegate];
    _currentlySelectedRowIndex = NO_TABLE_ROW_SELECTED; //i.e. there isn't one selected.
    
    [[self view] setBackgroundColor:[ApplicationUtilities defaultImageColorForTextureBackround]];
    [[self navigationItem] setTitle:NSLocalizedString(@"Favorites", @"Favorites View Controller Header Title")];

    
    [_addFavoritesLabel1 setText:NSLocalizedString(@"Favorites allow fast searches for places you visit most often." , @"How to add favorite String pt1")];
    [_addFavoritesLabel2 setText:NSLocalizedString(@"Tap the + button to get started, or favorite any transit stop by tapping the star icon in the top bar." , @"How to add favorite String pt2")];
    
     _editFavoritesButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Edit", @"Edit Button Title") style:UIBarButtonItemStyleBordered target:self action:@selector(editTable:)];
    
    [_routeToButton setTitle:NSLocalizedString(@"Route To", @"Route To Button Text") forState:UIControlStateNormal];
    
    UIBarButtonItem *addFavorites = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addNewFavorite:)];
    [[self navigationItem] setLeftBarButtonItem:_editFavoritesButton];
    [[self navigationItem] setRightBarButtonItem:addFavorites];
    
    _progressHUD =  [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.view addSubview:_progressHUD];
   
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillResign) name:UIApplicationWillResignActiveNotification object:NULL];
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [_favoritesTableView reloadData]; //incase the favorites have changed
    [self checkFavoritesStatus]; //update the view

    UIImage *buttonImage = [[UIImage imageNamed:@"greenButton.png"]
                            resizableImageWithCapInsets:UIEdgeInsetsMake(18, 18, 18, 18)];
    UIImage *buttonImageHighlight = [[UIImage imageNamed:@"greenButtonHighlight.png"]
                                     resizableImageWithCapInsets:UIEdgeInsetsMake(18, 18, 18, 18)];
    // Set the background for any states you plan to use
    [_routeToButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [_routeToButton setBackgroundImage:buttonImageHighlight forState:UIControlStateHighlighted];
    


}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
   // [Flurry logEvent:@"FAVORITES_VIEW_DID_APPEAR"];
}

-(void) favoritePlaceWasSelected:(FavoritePlace *) place{
    _currentlySelectedFavoritePlace = place;
}

-(void) favoriteWasDeletedWithNumberRemaining:(int) favoritesRemaining{
    if(favoritesRemaining == 0){ //there are no favorites, so we must force end editing
        [self editTable:nil];
        [self checkFavoritesStatus];
    }
}


-(void) checkFavoritesStatus{ //check if we have favorites and update accordingly
    if([[[FavoritePlacesStore sharedFavoritePlacesStore] favoritePlaces] count] == 0){
        [self showNoFavoritesView:YES];
    }else{
        [self showNoFavoritesView:NO];
    }
}

-(void) showNoFavoritesView:(BOOL) showView{
    
    if (showView) {
        [_favoritesTableView setAlpha:0.0];
        [_favoritesTableView setUserInteractionEnabled:NO];
        [_routeToButton setUserInteractionEnabled:NO];
        [_routeToButton setAlpha:0.0];
        [_noFavoritesExplanationView setAlpha:1.0];
        [[self navigationItem] setLeftBarButtonItem:nil];
    }else{
        [_favoritesTableView setAlpha:1.0];
        [_favoritesTableView setUserInteractionEnabled:YES];
        [_routeToButton setUserInteractionEnabled:YES];
        [_routeToButton setAlpha:1.0];
        [_noFavoritesExplanationView setAlpha:0.0];
        [[self navigationItem] setLeftBarButtonItem:_editFavoritesButton];
    }
}


- (IBAction)editTable:(id)sender {
    
    if([_favoritesTableView isEditing]){
        [_favoritesTableView setEditing:NO animated:YES];
      [_editFavoritesButton setTitle:NSLocalizedString(@"Edit", @"Edit Button Title") ];
        [_editFavoritesButton setTintColor:[ApplicationUtilities defaultApplicationHighlightColor]];
      
    }else{
        [_favoritesTableView setEditing:YES animated:YES];
        [_editFavoritesButton setTitle:NSLocalizedString(@"Done", @"Done Button Title")];
        [_editFavoritesButton setTintColor:[UIColor redColor]];
     }
}

- (IBAction)routeToFavorite:(id)sender {
    if(_currentlySelectedFavoritePlace == nil){// == NO_TABLE_ROW_SELECTED){
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"No Favorite Selected", @"No Favorite Selected Alert Title") message:NSLocalizedString(@"You must select a favorite from the table first", @"No Favorite Alert Message") delegate:nil cancelButtonTitle:NSLocalizedString(@"Dismiss", @"Dismiss Button Title") otherButtonTitles: nil];
        [alert show];
        return;
    }
    
    //do we have a location???
    if(_lastUserLocation == nil){
        [_locationServer startUpdatingLocation];
        [_progressHUD setLabelText:NSLocalizedString(@"Searching for Location", @"Searching for Location Message")];
        [_progressHUD show:YES];
    }else{
        [self searchForJourneysFromDeparture:_lastUserLocation.coordinate toFavoritePlace:_currentlySelectedFavoritePlace]; //we use Suomalenia sea fort as the origin for this
        _lastUserLocation = nil; //we wipe this out
    }
}

- (IBAction)addNewFavorite:(id)sender {

    AddNewFavoriteViewController * anfvc = [[AddNewFavoriteViewController alloc] init];
    [anfvc setInitialLocation:[_locationServer getLocation]];
    
    UINavigationController *tempNavController = [[UINavigationController alloc] init];
    [tempNavController setViewControllers:[NSArray arrayWithObject:anfvc]];
    
        [[tempNavController navigationBar] setTranslucent:NO];
    
    [self presentViewController:tempNavController animated:YES completion:^(void){
        [_favoritesTableView reloadData];
        [self checkFavoritesStatus];
    }]; //we reload the table incase of changes
}


//At the moment this is a duplicate method from PlanJourney. Keep these in sync.  We should change at some point to remove this duplicate code
-(void) searchForJourneysFromDeparture:(CLLocationCoordinate2D) departureLocation toFavoritePlace:(FavoritePlace *) destination{
    
    [_progressHUD setLabelText:NSLocalizedString(@"Searching for Journey", @"Searching for Journey Message")];
    [_progressHUD show:YES];

    
    ReittiopasQueryProcessor * queryProcessor = [[ReittiopasQueryProcessor alloc] init];
    NSString *arrivalDepartureString = @"departure"; //HMMMMM
      [queryProcessor findJourneyPlanFrom:departureLocation toLocation:destination.coordinate withTime:[NSDate date] arrivalDeparture:arrivalDepartureString withCompletionBlock:^(NSArray * arr, ReturnCodeType retCode){
               
            [_progressHUD hide:YES];
          if(retCode == kNetworkError){
              [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Error", @"Network Error Alert Title") message:NSLocalizedString(@"Your search could not be completed. Check your network connection and try again", @"Network Error Alert Message Unknown Network Error") delegate:nil cancelButtonTitle:NSLocalizedString(@"Dismiss", @"Dismiss Button Title") otherButtonTitles: nil] show];
            return;
        }else if (retCode == kNoResults){
            [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"No Results Found",@"No Results Found Alert Title") message:NSLocalizedString(@"No results could be found for your search",@"No Results Found Alert Message") delegate:nil cancelButtonTitle:NSLocalizedString(@"Dismiss", @"Dismiss Button Title") otherButtonTitles: nil] show];
            return;
        }
        NSLog(@"DUMP OF JOURNEY RESULTS");
        for(JourneyRoute *route in arr){
            NSLog(@"%@", [route stringRepresentation]);
            JourneyLeg *tempLeg = [[route journeyLegs] objectAtIndex:0];
            tempLeg.departureName =NSLocalizedString(@"Current Location", @"Current Location String");
            
            tempLeg = [[route journeyLegs] objectAtIndex:[[route journeyLegs] count]-1 ];
            tempLeg.arrivalName = destination.title;
        }
          
      
        //push a new view controller
        JourneySearchResultsViewController * journeySearchResults = [[JourneySearchResultsViewController alloc] init];
        [journeySearchResults setJourneys:arr];
        [self.navigationController pushViewController:journeySearchResults animated:YES];
    }];
}



-(void) didUpdateLocation:(CLLocation *) location{
    _lastUserLocation = location;
    [_locationServer stopUpdatingLocation];
    [self routeToFavorite:nil];
  
}

-(void) locationUpdateDidFail{
    NSLog(@"NEARBY STOPS LOCATION UPDATE FAILED");
    [_locationServer stopUpdatingLocation];
    [_progressHUD hide:NO];
    if(_currentAuthorisationStatus == kCLAuthorizationStatusDenied || _currentAuthorisationStatus == kCLAuthorizationStatusRestricted){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Location Services are Turned Off", @"Location Services Are Off Alert Title") message: NSLocalizedString(@"You cannot use Current Location as your departure when Location Services are turned off.", @"Location Services are Turned Off Message") delegate:nil cancelButtonTitle:NSLocalizedString(@"Dismiss", @"Dismiss Button Title") otherButtonTitles: nil];
        [alert show];
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Location Error", @"Location Error Alert Title") message:NSLocalizedString(@"Your location could not be determined. Please try again later", @"Location Error Alert Message") delegate:nil cancelButtonTitle:NSLocalizedString(@"Dismiss", @"Dismiss Button Title") otherButtonTitles: nil];
        [alert show];
    }
}
-(void) locationServicesAuthorisationStatusDidChange:(CLAuthorizationStatus) status{
    
    _currentAuthorisationStatus = status;
    NSLog(@"NEARBY STOPS AUTHORISATION STATUS CHANGED");
}

-(void) applicationWillResign{ //if we are searching for location whilst the app goes background. Bad things will happen
    
    if([_locationServer isActive]){
        [_progressHUD hide:NO];
        [_locationServer stopUpdatingLocation];
    }
}


- (void)viewDidUnload {
    [self setEditFavoritesButton:nil];
    [self setRouteToButton:nil];
    [self setNoFavoritesExplanationView:nil];
    [self setAddFavoritesLabel1:nil];
    [self setAddFavoritesLabel2:nil];
    [super viewDidUnload];
}




@end

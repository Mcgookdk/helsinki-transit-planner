//
//  HSLDisruptionsResponseParserDelegate.m
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 04/02/2013.
//  Copyright (c) 2013 David McGookin. All rights reserved.
//

#import "HSLDisruptionsResponseParserDelegate.h"

@implementation HSLDisruptionsResponseParserDelegate 

-(id) init{
    self = [super init];
    _disruptions = [[NSMutableArray alloc] init];
    _infos = [[NSMutableArray alloc] init];
    
   /* DUMMIES FOR TESTING
    HSLInfo *inf = [[HSLInfo alloc] init];
    [inf setText:@"BALLS"];
    [_infos addObject:inf];
    
    HSLDisruption *dis = [[HSLDisruption alloc] init];
    [dis setText:@"Lorem ipsum dolor sit amet, ligula suspendisse nulla pretium, rhoncus tempor placerat fermentum, enim integer ad vestibulum volutpat. Nisl rhoncus turpis est, vel elit, congue wisi enim nunc ultricies sit, magna tincidunt. Maecenas aliquam maecenas ligula nostra, accumsan taciti. Sociis mauris in integer, And Another 2"];
    [_disruptions addObject:dis];
    */
    return self;
}

//NSXMLParserDelegates
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict{
 //   NSLog(@"STARTED ELEMENT:%@", elementName);
    if ([elementName isEqualToString:DISRUPTION_STR]) {
   //     NSLog(@"STARTING TO PARSE DISRUPTION");
        _parsingDisruption   = YES;
        _currentDisruption = [[HSLDisruption alloc] init];
    }else if([elementName isEqualToString:INFO_STR]){ //There is an Info inside each disruption. We don't want it included as a seperate entity
     //   NSLog(@"STARTING TO PARSE INFO");
        _parsingInfo = YES;
        _currentInfo = [[HSLInfo alloc]init];
    }else if ([elementName isEqualToString:TEXT_STR]){
        _currentlyParsedTag = TEXT_ID;
    }
    else{
       // NSLog(@"UNKONWN PARSER TAG");
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    //NSLog(@"FOUND CHARACTERS:%@", string);
    if(_currentlyParsedTag == TEXT_ID){ //parsing text
        if(_parsingInfo){
            [_currentInfo setText:[[_currentInfo text] stringByAppendingString: string]];
        }else if(_parsingDisruption){
            [_currentDisruption setText:[[_currentDisruption text] stringByAppendingString: string]];
        }
    }
}



- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
   // NSLog(@"FINISHED ELEMENT:%@ %@", elementName, qName);
    if ([elementName isEqualToString:DISRUPTION_STR]) {
     //   NSLog(@"FINSIHED PARSING DISRUPTION");
        _parsingDisruption   = NO;
        [_disruptions addObject:_currentDisruption];
        _currentDisruption =nil;
    }else if([elementName isEqualToString:INFO_STR] && !_parsingDisruption){
       // NSLog(@"FINISHED PARSING INFO");
        _parsingInfo = NO;
        [_infos addObject:_currentInfo];
        _currentInfo = nil;
    }else if([elementName isEqualToString:INFO_STR] && _parsingDisruption){    
        //NSLog(@"FINISHED PARSING INFO AS PART OF DISRUPTION");
        _parsingInfo = NO;
        [_currentDisruption setInformation:_currentInfo];
        _currentInfo = nil;
    }else{
        //NSLog(@"UNKONWN PARSER TAG");
    }

}



@end

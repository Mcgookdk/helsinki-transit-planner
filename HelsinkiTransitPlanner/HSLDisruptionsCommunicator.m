//
//  HSLDisruptionsCommunicator.m
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 04/02/2013.
//  Copyright (c) 2013 David McGookin. All rights reserved.
//

#import "HSLDisruptionsCommunicator.h"
#import "HSLDisruptionsResponseParserDelegate.h"

@implementation HSLDisruptionsCommunicator


-(id) init{
    self = [super init];
    _backgroundQueue = dispatch_queue_create("uk.co.kerrsoftware.helsinkitransitplanner.disruptionqueue", NULL);
    return self;
}


-(void) sendBlockingQueryInLanguage:(NSString *) language fillDisruptionsArray:(NSArray **) disruptions fillInformationsArray: (NSArray **) informations withReturnedCode: (ReturnCodeType *) retCode{
    
    NSString *requestString = [NSString stringWithFormat:@"http://www.poikkeusinfo.fi/xml/v2/%@", language];
    NSLog(@"Sending Request %@", requestString);
    NSURL *requestURL = [NSURL URLWithString:[requestString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]; //spaces cause a problem in the request, so replace with explicit characters
    NSError *error = nil;
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:requestURL];
    NSData *serverReply = [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:nil error:&error];
    if(serverReply != nil){
        NSData * dataResponse = serverReply;//
    
        NSXMLParser *xmlParser = [[NSXMLParser alloc] initWithData:dataResponse];
        HSLDisruptionsResponseParserDelegate *parserDelegate = [[HSLDisruptionsResponseParserDelegate alloc]init];
        [xmlParser setDelegate:parserDelegate];
        [xmlParser parse];
        *disruptions = [NSArray arrayWithArray:[parserDelegate disruptions]];
        *informations= [NSArray arrayWithArray:[parserDelegate infos]];
    
        if(dataResponse == nil){ //if the output is null then there was a sucessful response, but no data in the response
            *retCode =  kNoResults;
            NSLog(@"NO RESULTS");
        }else{
            *retCode = kResultOK;
        }
        return;
    }else{
        NSLog(@"Error is %@", error);
        *retCode = kNetworkError;
        return;
    }
    
}

-(void)sendQueryInLanguage:(NSString *) language withCallbackBlock:(void (^)(NSArray * disruptions, NSArray * informations, ReturnCodeType retCode)) communicationRequestCompleted{
    dispatch_async(_backgroundQueue, ^{
        
        NSArray *disruptionsArr;
        NSArray *informationsArr;

        ReturnCodeType returnCode;
         [self sendBlockingQueryInLanguage:language fillDisruptionsArray:&disruptionsArr fillInformationsArray:&informationsArr withReturnedCode:&returnCode];
        
        dispatch_async(dispatch_get_main_queue(),^{
            communicationRequestCompleted(disruptionsArr, informationsArr, returnCode); //call on main  queue
        });
    }); //end background block
}



@end

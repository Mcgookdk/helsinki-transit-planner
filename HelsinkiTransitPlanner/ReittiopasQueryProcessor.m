//
//  ReittiopasQueryProcessor.m
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 21/10/2012.
//  Copyright (c) 2012 David McGookin. All rights reserved.
//
//Handles the queries for View Controllers of the web service

#import "ReittiopasQueryProcessor.h"
#import "JourneyLegStop.h"

#import <CoreLocation/CoreLocation.h>


#define RQP_NOT_FOUND  -99999
@implementation ReittiopasQueryProcessor

-(void) findJourneyPlanFrom:(CLLocationCoordinate2D) departureLocation toLocation: (CLLocationCoordinate2D) arrivalLocation withTime:(NSDate *) date arrivalDeparture:(NSString *) arrivalDeparture withCompletionBlock:(void(^)(NSArray *, ReturnCodeType retCode)) journeyPlanningCompletionBlock{
    ReittiopasCommunicator *communicator = [[ReittiopasCommunicator alloc] init];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyyMMdd"];
    NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
    [timeFormat setDateFormat:@"HHmm"];
    
    NSString *queryString = [NSString stringWithFormat:@"route&from=%f,%f&to=%f,%f&date=%@&time=%@&timetype=%@&detail=full&show=5", departureLocation.longitude, departureLocation.latitude, arrivalLocation.longitude, arrivalLocation.latitude,[dateFormat stringFromDate:date], [timeFormat stringFromDate:date], arrivalDeparture];//remember rettio revereses lat lon order
    
    [communicator sendQuery:queryString withCallbackBlock:^(NSData * dataResponse, ReturnCodeType retCode){
     //   NSLog(@"JOURNEY PLANNING COMPLETION %@", dataResponse);
        if(retCode == kNetworkError){
            journeyPlanningCompletionBlock(nil, kNetworkError);
        }else if (retCode == kNoResults){
           journeyPlanningCompletionBlock(nil, kNoResults);
        }else{
            NSArray * journeys = [self processJourneySearchResults:(NSArray *)dataResponse];
            journeyPlanningCompletionBlock(journeys, kResultOK);
        }
    }];
}


-(void) findFullRouteForService:(NSString *) serviceCode withCompletionBlockHandler:(void(^)(TransitRoute *, ReturnCodeType)) servicesFromStopCompletionBlock{
    
    ReittiopasCommunicator *communicator = [[ReittiopasCommunicator alloc] init];
    NSString *queryString =[NSString stringWithFormat:@"lines&query=%@", serviceCode];
    
    [communicator sendQuery:queryString withCallbackBlock:^(NSData * dataResponse, ReturnCodeType retCode){
        // NSLog(@"RESPONSE WAS %@", dataResponse);
        if(retCode == kNetworkError){
            servicesFromStopCompletionBlock(nil, kNetworkError);
        }else if (retCode == kNoResults){
            servicesFromStopCompletionBlock(nil, kNoResults);
        }else{
            TransitRoute * transitRoute = [self processTransitService:(NSArray *) dataResponse];
            servicesFromStopCompletionBlock(transitRoute, kResultOK);
        }
    }];
    
}
-(void) findStopsNearLocation:(CLLocationCoordinate2D) loc withCompletionBlockHandler:(void(^)(NSArray *, ReturnCodeType)) nearbyStopCompletionBlock{
   
    ReittiopasCommunicator *communicator = [[ReittiopasCommunicator alloc] init];
    NSString *queryString =[NSString stringWithFormat:@"reverse_geocode&coordinate=%f,%f&limit=200&result_contains=stop", loc.longitude, loc.latitude];//remember rettio revereses lat lon order

    [communicator sendQuery:queryString withCallbackBlock:^(NSData * dataResponse, ReturnCodeType retCode){
        
        if(retCode == kNetworkError){
            nearbyStopCompletionBlock(nil, kNetworkError);
        }else if (retCode == kNoResults){
            nearbyStopCompletionBlock(nil, kNoResults);
        }else{
            NSArray * resultsFromServer = (NSArray *) dataResponse;
            NSArray *processedResults= [self processNearbyStopResults:resultsFromServer];
            if([processedResults count] == 0){
                nearbyStopCompletionBlock(nil, kNoResults); //if we can't get the results out this may still be returned as no results
            }else{
                nearbyStopCompletionBlock(processedResults, kResultOK);
            }
        }
    }];
}


-(void) findServicesDepartingFromStop: (TransitStop *) stop withCompletionBlockHandler:(void(^)(NSArray *, ReturnCodeType)) servicesFromStopCompletionBlock{
    
    //we need a background queue to do this
    dispatch_queue_t  _backgroundQueue  = dispatch_queue_create("uk.co.kerrsoftware.helsinkitransitplanner.reittiopassqueryprocessor.backgroundqueue", NULL);
    
    dispatch_async(_backgroundQueue, ^{
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyyMMdd"];
        NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
        [timeFormat setDateFormat:@"HHmm"];
        NSDate *currentDate = [NSDate date];
        
        int numberOfFailedNetworkCalls = 0;
        ReittiopasCommunicator *communicator = [[ReittiopasCommunicator alloc] init];
        NSMutableArray * services = [[NSMutableArray alloc] init];
        for(NSString * codeStr in [stop codes]){
            ReturnCodeType retCode;
            NSString *queryString =[NSString stringWithFormat:@"stop&code=%@&date=%@&time=%@", codeStr, [dateFormat stringFromDate:currentDate], [ timeFormat stringFromDate:currentDate]];
            NSData * dataResponse =   [communicator sendBlockingQuery:queryString withReturnedCode:&retCode];
              // NSLog(@"RESPONSE WAS %@", dataResponse);
                if(dataResponse == nil){
                    numberOfFailedNetworkCalls++;
                }else{
                    [services addObjectsFromArray:[self processServiceResults:(NSArray *)dataResponse fromStop:codeStr withName:[stop name]]];
                }
        }
        
        [services sortUsingSelector:@selector(departureDateComparator:)];
        
     //   for(TransitService *ts in services){
     //       NSLog([ts stringRepresentation]);
      //  }
        dispatch_async(dispatch_get_main_queue(), ^{
            if (numberOfFailedNetworkCalls == [[stop codes] count]){ //all calls failed
                servicesFromStopCompletionBlock(nil, kNetworkError);
            }
            else if([services count]>0){
                servicesFromStopCompletionBlock(services, kResultOK);
            }else{
                servicesFromStopCompletionBlock(nil, kNoResults);
            }
        }); //end of callback block
    }); //end of background block
}

//Methods to process results from the queries before they are passed up to the the completion blocks


-(NSArray *) processJourneySearchResults:(NSArray *) results{
    NSMutableArray * journeys = [[NSMutableArray alloc] init];
    //NSArray *resultsInner = (NSArray *)[results objectAtIndex:0];
  //  NSLog(@"PROCESS JOURNEY SEARCH RESULTS");
    for(int i =0; i < [results count]; i++){
       // NSLog(@"Processing Journey");
       [journeys addObject:[self processJourney:(NSDictionary *)[[results objectAtIndex:i] objectAtIndex:0]]];
    }
    return [NSArray arrayWithArray:journeys];
}

-(JourneyRoute *) processJourney:(NSDictionary *) journeyRoute{
    JourneyRoute *tempJourney = [[JourneyRoute alloc] init];
  //  NSLog(@"PROCESS JOURNEY");
    //get the legs array
    
    NSArray *legs = [journeyRoute objectForKey:@"legs"];
    NSMutableArray *journeyLegs = [[NSMutableArray alloc] init];
    for(NSArray *leg in legs){
      //  NSLog(@"ITErATION");
        JourneyLeg *jLeg = [self processJourneyLeg:(NSDictionary *)leg];
        [journeyLegs addObject:jLeg];
    }
    
    
    //post process the journey. Add the start of each polyline route onto the end of the previous.
    for (int i = 1; i < [journeyLegs count]; i++){
        JourneyLeg *currentLeg = [journeyLegs objectAtIndex:i];
        JourneyLeg *previousLeg = [journeyLegs objectAtIndex:i-1];
        CLLocation * startOfCurrentRoute = [[currentLeg polyline] objectAtIndex:0];
        [previousLeg setPolyline:[[previousLeg polyline] arrayByAddingObject:startOfCurrentRoute]];
    }
    
    [tempJourney setJourneyLegs:[NSArray arrayWithArray:journeyLegs]];
    return tempJourney;
}

-(JourneyLeg *) processJourneyLeg:(NSDictionary *) legRaw{
    JourneyLeg * tempLeg = [[JourneyLeg alloc] init];
   // NSLog(@"Processing Leg%@ ", legRaw);
    
    NSArray * pathData = (NSArray *)[legRaw objectForKey:@"locs"];
  
    [tempLeg setLegLengthInMeters:[[legRaw objectForKey:@"length"] doubleValue]];
    [tempLeg setLegDurationInSeconds:[[legRaw objectForKey:@"duration"] integerValue]];
    //Parse out the necessary data from the array
    NSDateFormatter *dateFormatter= [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyyMMddHHmm"];
    [tempLeg setDepartureDate:[dateFormatter dateFromString:[(NSDictionary *)[pathData objectAtIndex:0] objectForKey:@"depTime"]]];
    [tempLeg setArrivalDate:[dateFormatter dateFromString:[(NSDictionary *)[pathData objectAtIndex:[pathData count]-1] objectForKey:@"arrTime"]]];
    [tempLeg setDepartureName: [(NSDictionary *)[pathData objectAtIndex:0] objectForKey:@"name"]];
    if([tempLeg departureName] == [NSNull null]){
        [tempLeg setDepartureName:@"DEPARTURE WAS NULL"];
    }
    [tempLeg setArrivalStopCode:[(NSDictionary *)[pathData objectAtIndex:[pathData count]-1] objectForKey:@"shortCode"]];
    [tempLeg setDepartureStopCode:[(NSDictionary *)[pathData objectAtIndex:0] objectForKey:@"shortCode"]];
    [tempLeg setArrivalName: [(NSDictionary *)[pathData objectAtIndex:[pathData count]-1] objectForKey:@"name"]];
    if([tempLeg arrivalName] == [NSNull null]){
        [tempLeg setArrivalName:@"ARRIVAL WAS NULL"];
    }
    NSString *transportType = [legRaw objectForKey:@"type"]; //get the transport type for this leg
    if([transportType compare:@"walk"] == NSOrderedSame){
        //we are walking so there are no stop codes
        [tempLeg setTransitType:kWalk];
        [tempLeg setService:nil]; //we are walking there is no service code
    }else{
        //we need to determine the transport type
        TransitService *legService = [[TransitService alloc] init];
        [legService setCode:[legRaw objectForKey:@"code"]];
        [tempLeg setService:legService];
        [tempLeg setTransitType:[legService type]];
    }
  
    //parse the intermediate stops
    //if([tempLeg transitType] != kWalk){
        NSMutableArray *stopsArray = [[NSMutableArray alloc] init];
        
        double lat,lon;
        NSString *stopName;
        NSDate * tempDepartureDate;
        
        for(NSDictionary * legPoint in pathData){
            NSLog(@"DIC: %@", legPoint);
            lat = [[[legPoint objectForKey:@"coord"] objectForKey:@"y"] doubleValue];
            lon = [[[legPoint objectForKey:@"coord"] objectForKey:@"x"] doubleValue];
            stopName = [legPoint objectForKey:@"name"];
            tempDepartureDate = [dateFormatter dateFromString:[legPoint objectForKey:@"depTime"]];
            JourneyLegStop * jls = [[JourneyLegStop alloc] init];
            [jls setStopCode:[legPoint objectForKey:@"shortCode"]];
            [jls setName:stopName];
            [jls setDepartureDate: tempDepartureDate];
            [jls setTransitType: [tempLeg transitType]];
            CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(lat, lon);
            [jls setLocation:coord];
            [jls setStopType:kIntermediateStop];
            [stopsArray addObject:jls];
        }
        [stopsArray removeObjectAtIndex:[stopsArray count]-1]; //we take the last stop off as this is really the end point
            [stopsArray removeObjectAtIndex:0]; //we need to do the same with te first one, as this is the start location
         [tempLeg setStopsOnLeg:[[NSArray alloc] initWithArray:stopsArray]];
   // }
    
   
    
    //parse the poly line
    NSMutableArray *shapeArray = [[NSMutableArray alloc] init];
   // double lat,lon;
    NSArray * routeArray = [legRaw objectForKey:@"shape"];
    for (NSDictionary * shapePoint in routeArray){
        lat = [[shapePoint objectForKey:@"y"] doubleValue];
        lon = [[shapePoint objectForKey:@"x"] doubleValue];
        [shapeArray addObject:[[CLLocation alloc] initWithLatitude:lat longitude:lon]];
    }
    
    
    /*JourneyLegStop *stop = [[tempLeg stopsOnLeg] objectAtIndex:0];
   
    //add the first and last stop location into the polyline/ Rettipass doesn't always go to stop on a route leaving dead segments between legs so we connect them
    CLLocation *coord =[[CLLocation alloc] initWithLatitude: stop.location.latitude longitude:stop.location.longitude];
    [shapeArray insertObject:coord atIndex:0];
    
    //add the last stop
    stop = [[tempLeg stopsOnLeg] lastObject];
    coord = [[CLLocation alloc] initWithLatitude:stop.location.latitude longitude:stop.location.longitude];
    [shapeArray insertObject:coord atIndex:[shapeArray count]];
    */
    [tempLeg setPolyline:[[NSArray alloc] initWithArray:shapeArray]];
    return tempLeg;
}

-(TransitRoute *) processTransitService:(NSArray *) results{
    NSDictionary *rawResults = [results objectAtIndex:0];
    
    NSMutableArray *route = [[NSMutableArray alloc] init];
    NSMutableArray *serviceStops = [[NSMutableArray alloc] init];
    
    
    TransitType transitTypeForLine = [self determineTransitTypeFromTransitTypeCode: [[rawResults valueForKey:@"transport_type_id"] intValue]];
    
    
    
    //process the raw line geometry
    NSArray * geoCoords = [[rawResults valueForKey:@"line_shape"] componentsSeparatedByString:@"|"];
    for(NSString * str in geoCoords){
        NSArray * latLonStr = [str componentsSeparatedByString:@","];
        [route addObject:[[CLLocation alloc] initWithLatitude:[[latLonStr objectAtIndex:1] doubleValue] longitude:[[latLonStr objectAtIndex:0] doubleValue]]]; //remember rettio revereses lat lon order
    }
    
    //process the stops on the line
    NSArray *stops = [rawResults valueForKey:@"line_stops"];
    for(NSDictionary *stop in stops){
       // NSLog(@"DIC IS %@", stop);
        TransitStop  * tempStop = [[TransitStop alloc] init];
        [tempStop setType:transitTypeForLine];
        [tempStop setCodes:[NSArray arrayWithObject:[stop valueForKey:@"codeShort"]]];
        [tempStop setName:[stop valueForKey:@"name"]];
        [tempStop setAddress:[stop valueForKey:@"address"]];
        NSArray * stopLatLon = [[stop valueForKey:@"coords"] componentsSeparatedByString:@","];
        tempStop.location = CLLocationCoordinate2DMake([[stopLatLon objectAtIndex:1] doubleValue], [[stopLatLon objectAtIndex:0] doubleValue]);
        [serviceStops addObject:tempStop];
        tempStop = nil;
    }
    
    TransitRoute *transitRoute = [[TransitRoute alloc] init];
    transitRoute.stops = serviceStops;
    transitRoute.route = route;
    
    return transitRoute;
}
-(NSArray *) processServiceResults:(NSArray *) results fromStop:(NSString *) stopCode withName:(NSString *) name{

    NSDictionary *rawResults = [results objectAtIndex:0];
    NSMutableArray * services = [[NSMutableArray alloc] init];
   
    //parse the lines field to get the codes and their destinations
    NSMutableDictionary *linesFieldDic = [[NSMutableDictionary alloc] init];
    NSArray * linesStrings = [rawResults objectForKey:@"lines"];

    for(NSString *lineStr in linesStrings){
        NSArray *tokenisedName = [lineStr componentsSeparatedByString:@":"];
        [linesFieldDic setValue:[tokenisedName objectAtIndex:1] forKey:[tokenisedName objectAtIndex:0]];
    }
   
   //work through the departures array and parse the departures
    NSArray *departuresArray = [rawResults valueForKey:@"departures"];
  
    if(departuresArray !=  [NSNull null]){ //must be [NSNull null] not nil
        for(NSDictionary *currentDeparture in departuresArray){
            TransitService *tempService = [[TransitService alloc]init];
            [tempService setCode:[currentDeparture valueForKey:@"code"]];
            [tempService setDestination:[linesFieldDic valueForKey:tempService.code]];
            
            //add in the date and time of departure (as formatted NSDate)
           
            
            //the service can and will return services beyond 24:00hrs!
            //so we must resort to this hackery to adjust the times we get so they process as dates
            NSString *timeString;
            BOOL timeIsCorrected = NO;
            int timeValue = [[currentDeparture valueForKey:@"time"] intValue];
            if(timeValue>2359){
                timeIsCorrected = YES;// we need to add a day on
                timeValue = timeValue - 2359;
                int hours = timeValue/60;
                int minutes = timeValue%60;
                timeString = [NSString stringWithFormat:@"%02d%02d",hours,minutes];
                NSLog(@"Corrected Time is :%@:", timeString);
            }else{
                timeString = [currentDeparture valueForKey:@"time"];
               // NSLog(@"Time is ok");
            }
            
            NSString *tempDataTimeString = [NSString stringWithFormat:@"%@:%@", [currentDeparture valueForKey:@"date"], timeString];
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"yyyyMMdd:HHmm"];
            NSDate *date = [dateFormat dateFromString:tempDataTimeString];
            if(timeIsCorrected){ //as the time was > 2400 we need to add a day onto the date to make it correct
               date = [date dateByAddingTimeInterval:(60*60*24)];
            }
            [tempService setDepartureDate:date];
        
            
            //some results are returning arrivals if this is the terminal stop we are serching for. We check and do not add servies where the service destination == the stop we are serching for services from.
            if([tempService.destination compare:name] != NSOrderedSame){
                [services addObject:tempService];
            }
            tempService = nil;
        }
    }
    return services;
}


-(NSArray *) processNearbyStopResults:(NSArray *) results{
    
    NSMutableArray *nearbyStops = [[NSMutableArray alloc] init];
    
    for(NSDictionary *currentStop in results){
        TransitStop *tempStop = [[TransitStop alloc] init];
        NSArray *tokenisedName = [[currentStop valueForKey:@"name"] componentsSeparatedByString:@","];
        [tempStop setName:[tokenisedName objectAtIndex:0]];
        [tempStop setDistanceFromSearchInMeters:[[currentStop valueForKey:@"distance"] doubleValue]];
        [tempStop setAddress:[currentStop valueForKey:@"address"]];
        
        //get lat and lon from the single string rep. What a palaver. They really should have these as seperate
        //also not the swapping of the first and second coords as the api returns these the other way around
        NSString * lonLat = [currentStop valueForKey:@"coords"];
        NSArray * lonLatArr = [lonLat componentsSeparatedByString:@","];
        tempStop.location = CLLocationCoordinate2DMake([[lonLatArr objectAtIndex:1] doubleValue], [[lonLatArr objectAtIndex:0] doubleValue]);
        
        //go into the details field and get the code and line details
        NSDictionary * currentStopDetails = [currentStop valueForKey:@"details"];
        [tempStop.codes  addObject:[currentStopDetails valueForKey:@"code"]];
        
        //go through and add the lines (Transit Services) in
        NSMutableSet *transitServices = [tempStop services];
        NSArray *currentStopServices = [currentStopDetails valueForKey:@"lines"];
        
        for(NSString * service in currentStopServices){
            TransitService *tempService = [[TransitService alloc] init];
            NSArray * tokenisedService = [service componentsSeparatedByString:@":"];
            [tempService setDestination:[tokenisedService objectAtIndex:1]];
            [tempService setCode:[tokenisedService objectAtIndex:0]];
            [[tempStop uniqueUserReadableServiceCodes] addObject:[tempService userReadableCode]];
            [transitServices addObject:tempService];
            tempService = nil;
        }
        
        //set the tempStop service type code to the service type of the first service.  We assume only busses will be at the bus station etc
        TransitService *tempService = [transitServices anyObject]; //we assume there is at least 1
        [tempStop setType:tempService.type];
       
        
        int mergeStopIndex = RQP_NOT_FOUND;
        //This should merge nearby lines but it is removed and currently not working. We need to work to put it back in
        
       // mergeStopIndex = [nearbyStops indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
         //   TransitStop *tempTransitStop =(TransitStop *) obj;
           // if( [self shouldMergeStop:tempStop toStop:tempTransitStop]){
             //   return idx;
            //}else{
             //   return RQP_NOT_FOUND;
            //}
           // }; //lossy processing
   // }]; //returns the index where this transit stop is
        
        if(mergeStopIndex != RQP_NOT_FOUND){ //i.e. we found an object
            TransitStop *primaryStop = [nearbyStops objectAtIndex:mergeStopIndex];
            [primaryStop.codes addObjectsFromArray:tempStop.codes];
            //merge the transit stops. We may need to revisit this for some pruni
            [primaryStop.services unionSet:tempStop.services];
            [primaryStop.uniqueUserReadableServiceCodes unionSet:tempStop.uniqueUserReadableServiceCodes];
        }else{
            [nearbyStops addObject:tempStop];
        }
        tempStop = nil;
    }
    
    
    return [NSArray arrayWithArray:nearbyStops];
}


-(BOOL) shouldMergeStop:(TransitStop *) stopA toStop:(TransitStop *) stopB{ //returns YES iff these two stops should be merged,
    if(([[stopA name] isEqualToString:stopB.name]) && (stopA.type == stopB.type)){//if both these match, merge the stop
        
        if(stopA.type == kMetro || stopA.type == kFerry){ //metros and ferries should always match.
            return YES;
        }
        
        if(stopA.type == kTrain){// && [stopA distanceInMetersFromStop:stopB] <= 1.0){
            return YES;
        }
        
        if((stopA.type == kBus && [stopA distanceInMetersFromStop:stopB] < 50.0) || ([stopA.services count] > 10) || [stopB.services count] >10){ //if there are a large number of services from each stop, then we assume that this is a bus station and merge
            return YES;
        }
        
        if(stopA.type == kTram && [stopA distanceInMetersFromStop:stopB] < 80.0){
            return YES;
        }
    }
    return NO;
}
    
-(TransitType) determineTransitTypeFromJoreCode:(NSString *) joreCode{
    
    unichar typeCode = [joreCode characterAtIndex:0];
    
    switch(typeCode){
        case '1':
        case '3':
        case '4':
        case '5':
            return kBus;
            break;
        case '2':
            return kTram;
            break;
        case '6':
            return kMetro;
            break;
        case '9':
            return kTrain;
            break;
        case '7':
            return kFerry;
            break;
        default:
            return kUnknownTransitType;
            break;
    }
}

-(TransitType) determineTransitTypeFromTransitTypeCode:(int) code{
    if (code == 2){
        return kTram;
        
    }else if(code == 6){
        return kMetro;
    }else if(code == 7){
        return kFerry;
    }else if(code == 12){
        return kTrain;
        
    }else{
        return kBus;
    }
}
@end

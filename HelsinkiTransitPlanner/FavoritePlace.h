//
//  FavoritePlace.h
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 03/11/2012.
//  Copyright (c) 2012 David McGookin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "TransitStop.h"

@interface FavoritePlace : NSObject<MKAnnotation>


@property (strong, nonatomic) NSString * title;
@property (readwrite, nonatomic) CLLocationCoordinate2D coordinate;
@property (strong, nonatomic) NSArray * placeCodes;
//to allow the MKAnnotation
@property (readonly, nonatomic) NSString * subtitle;
@property (readwrite, nonatomic) TransitType placeType;



-(id) initWithDictionary:(NSDictionary *) dictionary;
-(NSDictionary *) dictionaryRepresentationOfPlace;
-(NSString *) stringRepresentation;
-(BOOL) hasStopCode:(NSString *) code;
-(BOOL) hasStopCodes:(NSArray *) codes;

@end

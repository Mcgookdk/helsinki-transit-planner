//
//  ApplicationUtilities.m
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 02/11/2012.
//  Copyright (c) 2012 David McGookin. All rights reserved.
//


#import "ApplicationUtilities.h"

#define DEFAULT_FONT_NAME @"BanglaSangamMN-Bold"
#define DEFAULT_HEADING_FONT_SIZE 22.0f
#define DEFAULT_BODY_TEXT_FONT_SIZE 12.0f
@implementation ApplicationUtilities

+(UIColor *) defaultApplicationHighlightColor{
    return [UIColor colorWithRed:0.094 green:0.267 blue:0.494 alpha:1.000]; //almost finnish blue
}


+(UIFont *) defaultApplicationFontForHeader{
    return [UIFont fontWithName:DEFAULT_FONT_NAME size:DEFAULT_HEADING_FONT_SIZE];
}

+(UIFont *) defaultApplicationFontForBody{
    return [UIFont fontWithName:DEFAULT_FONT_NAME size:DEFAULT_BODY_TEXT_FONT_SIZE];
}

+(UIFont *) defaultApplicationFontForTableCell{
    return [UIFont fontWithName:@"Helvetica" size:22.0f];
}

+(NSDateFormatter *) dateFormatterForDateDisplay{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm  d MMMM yyyy"]; //full month name
    return dateFormatter;
}
+(UIColor *) defaultImageColorForTextureBackround{
    return [UIColor colorWithPatternImage:[UIImage imageNamed:@"white_wall_hash.png"]];//imageNamed:@"kindajean.png"]];
}

+(UIColor *) defaultImageColorForTableViewTextureBackround{
    return [UIColor colorWithPatternImage:[UIImage imageNamed:@"white_wall_hash.png"]];
}

+(NSString *) shortCopyrightNoticeFull{
    return @"HELTran Version 1.8 Copyright 2013-2017\n Twitter: @HELTran_support";
}

+(NSString *) shortCopyrightNoticeLite{
    return @"HELTran Lite Version 1.8 Copyright 2013-2017\n Twitter: @HELTran_support";
}

@end

//
//  ServiceCellView.h
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 28/10/2012.
//  Copyright (c) 2012 David McGookin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServiceCellView : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *serviceCodeLabel;
@property (weak, nonatomic) IBOutlet UILabel *destinationLabel;
@property (weak, nonatomic) IBOutlet UILabel *departureTimeLabel;


@end

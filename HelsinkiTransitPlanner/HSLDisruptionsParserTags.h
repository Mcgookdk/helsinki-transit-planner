//
//  HSLDisruptionsParserTags.h
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 04/02/2013.
//  Copyright (c) 2013 David McGookin. All rights reserved.
//

#ifndef HelsinkiTransitPlanner_HSLDisruptionsParserTags_h
#define HelsinkiTransitPlanner_HSLDisruptionsParserTags_h

//Numerical defines

#define DISRUPTIONS_ID 1
#define DISRUPTION_ID 2
#define INFO_ID 3
#define TEXT_ID 4
#define VALIDITY_ID 7
#define TARGETS_ID 8

#define DISRUPTIONS_STR @"DISRUPTIONS"
#define DISRUPTION_STR @"DISRUPTION"
#define INFO_STR @"INFO"
#define TEXT_STR @"TEXT"
#define VALIDITY_STR @"VALIDITY"
#define TARGETS_STR @"TARGETS"


#endif

//
//  ChangeTimeSubViewController.h
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 30/12/2012.
//  Copyright (c) 2012 David McGookin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangeTimeSubViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@end

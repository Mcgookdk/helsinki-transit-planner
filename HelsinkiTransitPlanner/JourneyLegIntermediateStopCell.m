//
//  JourneylegIntermediateStopCell.m
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 26/02/2013.
//  Copyright (c) 2013 David McGookin. All rights reserved.
//

#import "JourneylegIntermediateStopCell.h"

@implementation JourneyLegIntermediateStopCell

-(NSString *) reuseIdentifier{
    return @"JourneyLegIntermediateStopCell";
}

@end

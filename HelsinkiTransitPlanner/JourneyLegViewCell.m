//
//  JourneyLegViewCell.m
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 05/01/2013.
//  Copyright (c) 2013 David McGookin. All rights reserved.
//

#import "JourneyLegViewCell.h"

@implementation JourneyLegViewCell

-(NSString *) reuseIdentifier{
    return @"JourneyLegViewCell";
}

@end

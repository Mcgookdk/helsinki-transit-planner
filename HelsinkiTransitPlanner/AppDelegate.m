//
//  AppDelegate.m
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 19/10/2012.
//  Copyright (c) 2012 David McGookin. All rights reserved.
//

#import "AppDelegate.h"
#import "NearbyStopsViewController.h"
#import "FavoritePlacesStore.h"
#import "FavoritesViewController.h"
#import "PlanJourneyViewController.h"
#import "AboutAndDisruptionsViewController.h"

#import "RecentJourneyStore.h"

@implementation AppDelegate

-(void) setDefaultLookAndFeel{
    NSLog(@"VERSION 7");
    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName,[UIColor whiteColor],NSForegroundColorAttributeName,  nil]];
    [[UINavigationBar appearance] setBarTintColor:[ApplicationUtilities defaultApplicationHighlightColor]];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [self.window setTintColor:[ApplicationUtilities defaultApplicationHighlightColor]];
    [self.window setBackgroundColor:[UIColor whiteColor]];
    [[_tabBarController view] setOpaque:YES];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

void uncaughtExceptionHandler(NSException *exception) {
   // [Flurry logError:@"Uncaught" message:@"Crash!" exception:exception];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
#ifndef DEBUG
    NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);
  //  [Flurry setSecureTransportEnabled:YES]; // secure the packets
    
#ifdef COMPILED_AS_FULL_VERSION
   // [Flurry startSession:@"Y4P96BKPDBQNRZ8378CW"]; //start analytics for the full version
#else
   // [Flurry startSession:@"XWYZ6S9RKX63J9H7C672"]; //start analytics for the lite version
#endif
    
   // [Flurry setUserID:[[UIDevice currentDevice] name]];
#endif
    
    
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    _tabBarController = [[UITabBarController alloc] init];
        //for iOS 7 put look and feel code here
        [self setDefaultLookAndFeel];
       
       NearbyStopsViewController * nearbyStopsViewController = [[NearbyStopsViewController alloc] init];
    
  //  [Flurry logAllPageViews:_tabBarController];
    
    UINavigationController * navController = [[UINavigationController alloc] init];
    [[navController tabBarItem] setTitle:NSLocalizedString(@"Nearby", @"Nearby_tab_title")];
    
        [[navController tabBarItem] setImage:[UIImage imageNamed:@"789-map-location.png"]];
        [[navController navigationBar] setTranslucent:NO];
       [navController setViewControllers:[NSArray arrayWithObject:nearbyStopsViewController]];
    
    
    //the favorites controller
    FavoritesViewController *fvc = [[FavoritesViewController alloc] init];
    UINavigationController *favesNavController = [[UINavigationController alloc] init];
    
    [favesNavController setViewControllers:[NSArray arrayWithObject:fvc]];
    
    [[favesNavController tabBarItem] setTitle:NSLocalizedString(@"Favorites", @"Favorites_tab_title")];
    
   
    [[favesNavController tabBarItem] setImage:[UIImage imageNamed:@"726-star.png"]];
        [[favesNavController navigationBar] setTranslucent:NO];
    
    //the search journey
    UINavigationController * searchJourneyNavController = [[UINavigationController alloc] init];
    [[searchJourneyNavController navigationBar] setTranslucent:NO];
    PlanJourneyViewController *planJourney = [[PlanJourneyViewController alloc] init];
    [searchJourneyNavController setViewControllers:[NSArray arrayWithObject:planJourney]];
    [[searchJourneyNavController tabBarItem] setTitle:NSLocalizedString(@"Plan", @"Plan Journey Tab Title")];
  
           [[searchJourneyNavController tabBarItem] setImage:[UIImage imageNamed:@"708-search.png"]];
        [[searchJourneyNavController navigationBar] setTranslucent:NO];
   
    
    
    UINavigationController * disruptionsNavController = nil; 
    //the disruptions controller. note we need to hold onto this
    _disruptionsViewController = [[AboutAndDisruptionsViewController alloc] init];
    disruptionsNavController = [[UINavigationController alloc] init];
    [[_disruptionsViewController tabBarItem] setTitle:NSLocalizedString(@"Disruptions", @"Disruptions Tab Title")];
   
        [[disruptionsNavController navigationBar] setTranslucent:NO];
    [[_disruptionsViewController tabBarItem] setImage:[UIImage imageNamed:@"791-warning.png"]];
       [disruptionsNavController setViewControllers:[NSArray arrayWithObject:_disruptionsViewController]];

    NSArray *viewControllers;
    
#ifdef COMPILED_AS_LITE_VERSION
    viewControllers = [NSArray arrayWithObjects:navController, searchJourneyNavController, disruptionsNavController, nil]; //LITE version does not support favorites
#else
    viewControllers = [NSArray arrayWithObjects:navController, favesNavController, searchJourneyNavController, disruptionsNavController, nil];
#endif
    
    [_tabBarController setViewControllers:viewControllers];
   
    self.window.rootViewController = _tabBarController;
    [self.window makeKeyAndVisible];
    
    [_disruptionsViewController updateDisruptionInformation];
    
    return YES;
}


-(void) userIsOutsideHelsinkiRegion{
    if(! _havePresentedUsingDummyLocationAlert){
        _havePresentedUsingDummyLocationAlert = YES;
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"User Outside Helsinki", @"User Outside Helsinki Alert Title") message:NSLocalizedString(@"This app provides public transport journey planning within Helsinki. As you aren't in Helsinki, the application will assume you are right in the middle of town so you can try it out.", @"User Outside HElsinki Alert Message") delegate:nil cancelButtonTitle:NSLocalizedString(@"Dismiss", @"Dismiss Button Title") otherButtonTitles: nil];
        [alert show];
    }
    
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [[FavoritePlacesStore sharedFavoritePlacesStore] saveFavoritesToFile];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [_disruptionsViewController updateDisruptionInformation];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end

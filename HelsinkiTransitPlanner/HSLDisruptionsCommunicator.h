//
//  HSLDisruptionsCommunicator.h
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 04/02/2013.
//  Copyright (c) 2013 David McGookin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ReittiopasCommunicator.h"


#define LANGUAGE_ENGLISH @"en"
#define LANGUAGE_FINNISH @"fi"
#define LANGUAGE_SWEDISH @"se"

@interface HSLDisruptionsCommunicator : NSObject<NSURLConnectionDelegate>{
    
    dispatch_queue_t  _backgroundQueue;
}

-(void)sendQueryInLanguage:(NSString *) language withCallbackBlock:(void (^)(NSArray * disruptions, NSArray * informations, ReturnCodeType retCode)) communicationRequestCompleted;


//should not be called in the foreground thread as this requires a block
-(void) sendBlockingQueryInLanguage:(NSString *) language fillDisruptionsArray:(NSArray **) disruptions fillInformationsArray: (NSArray **) informations withReturnedCode: (ReturnCodeType *) retCode;

@end

//
//  ChangeTimeSubViewController.m
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 30/12/2012.
//  Copyright (c) 2012 David McGookin. All rights reserved.
//

#import "ChangeTimeSubViewController.h"


@implementation ChangeTimeSubViewController

- (void)viewDidUnload {
    [self setTimeLabel:nil];
    [super viewDidUnload];
}
@end

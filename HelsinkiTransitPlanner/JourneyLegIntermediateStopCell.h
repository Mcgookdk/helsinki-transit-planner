//
//  JourneylegIntermediateStopCell.h
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 26/02/2013.
//  Copyright (c) 2013 David McGookin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JourneyLegIntermediateStopCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *destinationLabel;
@property (weak, nonatomic) IBOutlet UILabel *departureTimeLabel;

@end

//
//  AboutAndDisruptionsViewController.m
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 04/02/2013.
//  Copyright (c) 2013 David McGookin. All rights reserved.
//

#import "AboutAndDisruptionsViewController.h"
#import "HSLInfo.h"
#import "HSLDisruption.h"
#import "ApplicationUtilities.h"
//#import "GADMasterViewController.h"

@implementation AboutAndDisruptionsViewController


- (void)viewDidLoad{
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;

    
    //UIPopoverController *popCon = [[UIPopoverController alloc] init];
    
    
    // Do any additional setup after loading the view from its nib.
     [[self navigationItem] setTitle:NSLocalizedString(@"Disruptions", @"Disruptions_view_title")];
    [_disruptionsTable setDelegate:self];
    [_disruptionsTable setDataSource:self];
    [[self view] setBackgroundColor:[ApplicationUtilities defaultImageColorForTextureBackround]];
#ifdef COMPILED_AS_FULL_VERSION
    [_copyrightNoticeLabel setText:[ApplicationUtilities shortCopyrightNoticeFull]];
#else
    [_copyrightNoticeLabel setText:[ApplicationUtilities shortCopyrightNoticeLite]];
#endif
#ifdef COMPILED_AS_FULL_VERSION
    [_dummyAdSpacer removeFromSuperview]; //if this is the full version, remove the spacer bar for the add, and adjust the rest of the view accordingly
    CGRect tempFrame = [_disruptionsTable frame];
   // to fix  tempFrame.origin.y -=kAdBannerHeight;
   // to fix  tempFrame.size.height += kAdBannerHeight;
   [_disruptionsTable setFrame:tempFrame];
#endif
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
#ifdef COMPILED_AS_LITE_VERSION
    //if it is the free version, put in the add view
    [[GADMasterViewController sharedGADMasterViewController] resetAdView:self inFrame:[_dummyAdSpacer frame]];
#endif
}

-(void) updateDisruptionInformation{
    HSLDisruptionsCommunicator *communicator = [[HSLDisruptionsCommunicator alloc] init];
    
   // NSString * currentlanguageStr = [[NSLocale currentLocale] objectForKey:NSLocaleLanguageCode];
    NSString * languageToUse = LANGUAGE_ENGLISH;
    NSString *userLanguageStr = [[NSLocale preferredLanguages] objectAtIndex:0];
    NSLog(@"LANGUAGE IS %@", userLanguageStr);
    if([userLanguageStr isEqual:LANGUAGE_FINNISH]){
        languageToUse = LANGUAGE_FINNISH;
    }else if([userLanguageStr isEqual:@"sv"]){ //Reitto uses a different code than ISO
        languageToUse = LANGUAGE_SWEDISH;
    }
    
    [communicator sendQueryInLanguage:languageToUse withCallbackBlock:^(NSArray *disruptions, NSArray *informations, ReturnCodeType retCode){
        if(retCode != kNetworkError){
            _currentDisruptions = disruptions;
            _currentInformations = informations; 
            if ([_currentDisruptions count] != 0){ //we always get a vaild array, but it might be empty
            [[self tabBarItem] setBadgeValue:[NSString stringWithFormat:@"%d", [_currentDisruptions count]]];
            }else{
                [[self tabBarItem] setBadgeValue:nil]; // t remove any previous value that was there
            }
            [_disruptionsTable reloadData];
        }
    }];
    
}





-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
//#ifdef COMPILED_AS_LITE_VERSION
    //if it is the free version, put in the add view
  //  [[GADMasterViewController sharedGADMasterViewController] resetAdView:self];
//#endif
}

-(void)viewDidUnload {
    [self setDisruptionsTable:nil];
    [self setCopyrightNoticeLabel:nil];
    [super viewDidUnload];
}


//uitableview datasource delegates
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //if it isn't a special cell keep going
    UITableViewCell * cell;
    
    cell = [tableView dequeueReusableCellWithIdentifier:@"DisruptionCellView"];
    	if (cell == nil){ //i.e. there isn't a cell already available, make a new one
    		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"DisruptionCellView"];//
            
            UILabel *cellLabel = [cell textLabel];
            [cellLabel setLineBreakMode:NSLineBreakByWordWrapping];
            [cellLabel setAdjustsFontSizeToFitWidth:YES];
           // [cellLabel setMinimumScaleFactor:0.5];
            [cellLabel setNumberOfLines:4];
            [cellLabel setFont:[UIFont fontWithName:@"helvetica" size:16.0f]];
    	}
      [cell setBackgroundColor:[ApplicationUtilities defaultImageColorForTableViewTextureBackround]];    
    int cellId =[indexPath indexAtPosition:1];
    
    //the following two lines probably look odd. Why have another variable? Well, it turns out the compiler is incapapble of figuring out when 0>-1 if you don't have one :(
    int numDisruptions = [_currentDisruptions count];
    if (cellId > numDisruptions-1){ //return an information
        HSLInfo * currentInfo = [_currentInformations objectAtIndex: (cellId - ([_currentDisruptions count] ))];
        
        /* This is where we define the ideal font that the Label wants to use.
         Use the font you want to use and the largest font size you want to use. */
        UIFont *font = [[cell textLabel] font];
        
        int i;
        /* Time to calculate the needed font size.
         This for loop starts at the largest font size, and decreases by two point sizes (i=i-2)
         Until it either hits a size that will fit or hits the minimum size we want to allow (i > 10) */
        for(i = 16; i >= 12; i=i-1)
        {
            // Set the new font size.
            font = [font fontWithSize:i];
            // You can log the size you're trying: NSLog(@"Trying size: %u", i);
            
            /* This step is important: We make a constraint box
             using only the fixed WIDTH of the UILabel. The height will
             be checked later. */
            CGSize constraintSize = CGSizeMake(230.0, MAXFLOAT); //Width of the label
            
            // This step checks how tall the label would be with the desired font.
            CGSize labelSize = [[currentInfo text] sizeWithFont:font constrainedToSize:constraintSize lineBreakMode:NSLineBreakByWordWrapping];
            
            /* Here is where you use the height requirement!
             Set the value in the if statement to the height of your UILabel
             If the label fits into your required height, it will break the loop
             and use that font size. */
            if(labelSize.height <= 80.0f){ //height of the label in the Cell
                break;
            }
        }
        // You can see what size the function is using by outputting: NSLog(@"Best size is: %u", i);
        
        // Set the UILabel's font to the newly adjusted font.
        [[cell textLabel] setFont:font];
        
        
        [[cell textLabel] setText:[currentInfo text]];
              [[cell imageView] setImage:[UIImage imageNamed:@"blue_info_icon.png"]];        
    }else{ // its a disruption
        HSLDisruption * currentDisruption = [_currentDisruptions objectAtIndex:cellId];
        
            /* This is where we define the ideal font that the Label wants to use.
             Use the font you want to use and the largest font size you want to use. */
            UIFont *font = [[cell textLabel] font];
            
            int i;
            /* Time to calculate the needed font size.
             This for loop starts at the largest font size, and decreases by two point sizes (i=i-2)
             Until it either hits a size that will fit or hits the minimum size we want to allow (i > 10) */
            for(i = 16; i >= 12; i=i-1)
            {
                // Set the new font size.
                font = [font fontWithSize:i];
                // You can log the size you're trying: NSLog(@"Trying size: %u", i);
                
                /* This step is important: We make a constraint box
                 using only the fixed WIDTH of the UILabel. The height will
                 be checked later. */
                CGSize constraintSize = CGSizeMake(230.0, MAXFLOAT); //Width of the label
                
                // This step checks how tall the label would be with the desired font.
                CGSize labelSize = [[[currentDisruption information] text] sizeWithFont:font constrainedToSize:constraintSize lineBreakMode:NSLineBreakByWordWrapping];
                
                /* Here is where you use the height requirement!
                 Set the value in the if statement to the height of your UILabel
                 If the label fits into your required height, it will break the loop
                 and use that font size. */
                if(labelSize.height <= 80.0f){ //height of the label in the Cell
                    break;
                }
            }
            // You can see what size the function is using by outputting: NSLog(@"Best size is: %u", i);
            
            // Set the UILabel's font to the newly adjusted font.
            [[cell textLabel] setFont:font];
        
        
        
        [[cell textLabel] setText:[[currentDisruption information]text]];
        [[cell imageView] setImage:[UIImage imageNamed:@"yellow_warning_icon.png"]];
    }
    return cell;
    
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_currentInformations count] + [_currentDisruptions count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80.0;
}

@end

//
//  FavoritePlacesStore.h
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 03/11/2012.
//  Copyright (c) 2012 David McGookin. All rights reserved.
//
//  Stores the user favorite places 

#import <Foundation/Foundation.h>
#import "FavoritePlace.h"

@interface FavoritePlacesStore : NSObject

@property (strong, nonatomic) NSMutableArray * favoritePlaces;
+(FavoritePlacesStore *) sharedFavoritePlacesStore; //don't create using init. ask for the shared instance

-(void) addNewPlace:(FavoritePlace *) place;
-(void) removeFavoritePlaceWithCodes:(NSArray *) codes;
-(void) loadFavoritesFromFile;
-(void) saveFavoritesToFile;

-(BOOL) areFavoritePlaces:(NSArray *) stopCodes;
@end

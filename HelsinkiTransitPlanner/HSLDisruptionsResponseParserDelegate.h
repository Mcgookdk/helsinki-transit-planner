//
//  HSLDisruptionsResponseParserDelegate.h
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 04/02/2013.
//  Copyright (c) 2013 David McGookin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HSLDisruption.h"
#import "HSLInfo.h"
#import "HSLDisruptionsParserTags.h"

@interface HSLDisruptionsResponseParserDelegate : NSObject<NSXMLParserDelegate>{
    HSLInfo *_currentInfo;
    HSLDisruption *_currentDisruption;
    BOOL _parsingDisruption;
    BOOL _parsingInfo;
    int _currentlyParsedTag;
}

@property (strong, nonatomic, readonly) NSMutableArray *disruptions;
@property (strong, nonatomic, readonly) NSMutableArray *infos;

@end

//
//  RecentGeoPlace.h
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 25/03/2013.
//  Copyright (c) 2013 David McGookin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GeoPlace.h"

@interface RecentGeoPlace : GeoPlace

@property (strong, nonatomic) NSDate *lastUsedDate;

-(id) initWithGeoPlace:(GeoPlace *) place andDate:(NSDate *) date;

-(id)initWithDictionary:(NSDictionary *) dictionary;

-(NSDictionary *) dictionaryRepresentationOfRecentGeoPlace;

@end

//
//  JourneySearchResultsCell.h
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 24/07/2013.
//  Copyright (c) 2013 David McGookin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JourneyRoute.h"

@interface JourneySearchResultsCell : UITableViewCell{
    
    UIView *_currentContentView;
}

@property (strong, nonatomic) JourneyRoute * journeyRoute;
@property (weak, nonatomic) IBOutlet UILabel *arrivalTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *departureTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *journeyTimeLabel;


-(void) updateJourneyCell; //should be called after the journey has been changed 
@end

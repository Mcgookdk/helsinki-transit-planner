//
//  ApplicationUtilities.h
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 02/11/2012.
//  Copyright (c) 2012 David McGookin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ApplicationUtilities : NSObject

//the color tint for the application. Used for Nav bars etc.
+(UIColor *) defaultApplicationHighlightColor;

//the main font used through the application (with size for header)
+(UIFont *) defaultApplicationFontForHeader;

//the main font used through the application (with size for body)
+(UIFont *) defaultApplicationFontForBody;

//for use is single row (standard) table cells
+(UIFont *) defaultApplicationFontForTableCell;

//returns a date formatter to provide a standard display.
+(NSDateFormatter *) dateFormatterForDateDisplay;

//returns the default image to use for a textured background in UIView
+(UIColor *) defaultImageColorForTextureBackround;

//returns the default image to use for a textured background in a TableView
+(UIColor *) defaultImageColorForTableViewTextureBackround;

//returns the copyright notice for the full version
+(NSString *) shortCopyrightNoticeFull;

//returns the copyright notice for the lite version
+(NSString *) shortCopyrightNoticeLite;

@end

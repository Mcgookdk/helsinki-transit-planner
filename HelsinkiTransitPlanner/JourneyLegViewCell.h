//
//  JourneyLegViewCell.h
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 05/01/2013.
//  Copyright (c) 2013 David McGookin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JourneyLegViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *stopLabel;
@property (weak, nonatomic) IBOutlet UILabel *serviceCodelabel;
@property (weak, nonatomic) IBOutlet UIImageView *transitTypeImage;
@property (weak, nonatomic) IBOutlet UILabel *stopTimeLabel;

@end

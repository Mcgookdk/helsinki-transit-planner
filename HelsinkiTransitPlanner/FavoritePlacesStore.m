//
//  FavoritePlacesStore.m
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 03/11/2012.
//  Copyright (c) 2012 David McGookin. All rights reserved.
//

#import "FavoritePlacesStore.h"

#define FAVORITES_FILENAME @"favorite_places.plist"

@implementation FavoritePlacesStore

static FavoritePlacesStore * sharedStore = nil; //the shared instance

+(FavoritePlacesStore *) sharedFavoritePlacesStore{
    if(sharedStore == nil){
        sharedStore = [[FavoritePlacesStore alloc] init];
        [sharedStore loadFavoritesFromFile]; //init the store
    }
    return sharedStore;
}

-(id) init{
    self = [super init];
    _favoritePlaces = [[NSMutableArray alloc] init];
    return self;
}

-(void) addNewPlace:(FavoritePlace *) place{
    [_favoritePlaces addObject:place];
}


-(void) loadFavoritesFromFile{
    NSString *errorDesc = nil;
    NSPropertyListFormat format;
    NSString *plistPath;
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                              NSUserDomainMask, YES) objectAtIndex:0];
    plistPath = [rootPath stringByAppendingPathComponent:FAVORITES_FILENAME];
    if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath]) {
        plistPath = [[NSBundle mainBundle] pathForResource:FAVORITES_FILENAME ofType:nil]; //load it from the default file in the application bundle
    }
    NSData *plistXML = [[NSFileManager defaultManager] contentsAtPath:plistPath];
    NSArray *temp = (NSArray *)[NSPropertyListSerialization
                                          propertyListFromData:plistXML
                                          mutabilityOption:NSPropertyListMutableContainersAndLeaves
                                          format:&format
                                          errorDescription:&errorDesc];
    if (!temp) {
        NSLog(@"Error reading plist: %@, format: %d", errorDesc, format);
    }
    
    for(NSDictionary *dic in temp){
        FavoritePlace *fp = [[FavoritePlace alloc] initWithDictionary:dic];
        [_favoritePlaces addObject:fp];
    }
}

-(void) saveFavoritesToFile{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    for(FavoritePlace *fp in _favoritePlaces){
        [array addObject:[fp dictionaryRepresentationOfPlace]];
    }
    
    NSString *error;
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *plistPath = [rootPath stringByAppendingPathComponent:FAVORITES_FILENAME];

    NSData *plistData = [NSPropertyListSerialization dataFromPropertyList:array
                                                                   format:NSPropertyListXMLFormat_v1_0
                                                         errorDescription:&error];
    if(plistData) {
        [plistData writeToFile:plistPath atomically:YES];
    }
    else {
        NSLog(error);
    }
}


-(BOOL) areFavoritePlaces:(NSArray *) stopCodes{
    for(FavoritePlace *favePlace in _favoritePlaces){
        if([favePlace hasStopCodes:stopCodes]){
            return YES;
        }
    }
    return NO;
}

-(void) removeFavoritePlaceWithCodes:(NSArray *) codes{
    for(FavoritePlace * favePlace in _favoritePlaces){
        if([favePlace hasStopCodes:codes]){
            [_favoritePlaces removeObject:favePlace];
            return;
        }
    }
}
@end

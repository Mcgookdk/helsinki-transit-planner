//
//  JourneyDetailsListViewController.h
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 05/01/2013.
//  Copyright (c) 2013 David McGookin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JourneyRoute.h"

@protocol JourneyDetailsListViewControllerDelegate <NSObject>
-(void) userDidSelectJourneyLegStop:(JourneyLegStop *) stop;
@end

@interface JourneyDetailsListViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>{
    NSArray * journeyStops;
}

@property (weak, nonatomic) id<JourneyDetailsListViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITableView *journeyDetailsTableView;
@property (strong, nonatomic) JourneyRoute *journeyToDisplay;

@end

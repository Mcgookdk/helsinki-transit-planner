//
//  FavoritesViewController.h
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 03/11/2012.
//  Copyright (c) 2012 David McGookin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FavoritePlacesStore.h"
#import "MBProgressHUD.h"
#import "LocationServer.h"
#import "FavoritesTableDelegate.h"


@interface FavoritesViewController : UIViewController<LocationServerDelegate, FavoritesTableDelegateDelegate>{
    int _currentlySelectedRowIndex;
    LocationServer *_locationServer;
    CLLocation *_lastUserLocation;
    MBProgressHUD *_progressHUD;
    CLAuthorizationStatus _currentAuthorisationStatus;
    FavoritesTableDelegate *_favoritesTableDelegate;
    FavoritePlace *_currentlySelectedFavoritePlace;
}
@property (weak, nonatomic) IBOutlet UILabel *addFavoritesLabel1;
@property (weak, nonatomic) IBOutlet UILabel *addFavoritesLabel2;

@property (weak, nonatomic) IBOutlet UITableView *favoritesTableView;
@property (strong, nonatomic) UIBarButtonItem * editFavoritesButton;
@property (weak, nonatomic) IBOutlet UIButton *routeToButton;
@property (weak, nonatomic) IBOutlet UIView *noFavoritesExplanationView;

- (IBAction)editTable:(id)sender;
- (IBAction)routeToFavorite:(id)sender;
- (IBAction)addNewFavorite:(id)sender;

@end

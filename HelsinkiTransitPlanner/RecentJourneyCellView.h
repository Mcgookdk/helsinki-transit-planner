//
//  RecentJourneyCellView.h
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 25/03/2013.
//  Copyright (c) 2013 David McGookin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecentJourneyCellView : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *fromLabel;
@property (weak, nonatomic) IBOutlet UILabel *toLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *fromLabelDescriptor;
@property (weak, nonatomic) IBOutlet UILabel *toLabelDescriptor;

@end

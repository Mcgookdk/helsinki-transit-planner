//
//  AppDelegate.h
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 19/10/2012.
//  Copyright (c) 2012 David McGookin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NearbyStopsViewController.h"
#import "ApplicationUtilities.h"
#import "AboutAndDisruptionsViewController.h"
#import "LocationServer.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>{
    UITabBarController *_tabBarController;
    BOOL _havePresentedUsingDummyLocationAlert;
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) AboutAndDisruptionsViewController * disruptionsViewController;


-(void) userIsOutsideHelsinkiRegion; //called by Locationmanager whenever the user is detected with high accuracy outside the Helsinki region, and a dummy location is being returned.

@end

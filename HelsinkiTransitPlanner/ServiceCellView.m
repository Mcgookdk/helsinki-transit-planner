//
//  ServiceCellView.m
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 28/10/2012.
//  Copyright (c) 2012 David McGookin. All rights reserved.
//

#import "ServiceCellView.h"

@implementation ServiceCellView

-(NSString *) reuseIdentifier{
    return @"ServiceCellView";
}

@end

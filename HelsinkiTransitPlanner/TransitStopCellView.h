//
//  TransitStopCellView.h
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 21/10/2012.
//  Copyright (c) 2012 David McGookin. All rights reserved.
//
//Custom class to represent the Transit Stops in a table view

#import <UIKit/UIKit.h>

@interface TransitStopCellView : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *title;
@property (strong, nonatomic) IBOutlet UILabel *services;
@property (strong, nonatomic) IBOutlet UILabel *distanceToStop;
@property (strong, nonatomic) IBOutlet UIImageView *thumbnailImage;

@end

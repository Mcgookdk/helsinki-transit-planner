//
//  TransitIconAndServiceSubViewController.h
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 30/12/2012.
//  Copyright (c) 2012 David McGookin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransitStop.h"
#import "TransitModelUtilities.h"

@interface TransitIconAndServiceSubViewController : UIViewController



@property (strong, nonatomic) NSString * transitServiceCode;
@property (readwrite, nonatomic) TransitType transitType;


@property (weak, nonatomic) IBOutlet UIImageView *transitIcon;
@property (weak, nonatomic) IBOutlet UILabel *servieLabel;

@end

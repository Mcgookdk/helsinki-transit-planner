//
//  JourneyDetailsListViewController.m
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 05/01/2013.
//  Copyright (c) 2013 David McGookin. All rights reserved.
//

#import "JourneyDetailsListViewController.h"
#import "JourneyLegViewCell.h"
#import "JourneyLegIntermediateStopCell.h"
#import "TransitModelUtilities.h"
//#import "GADMasterViewController.h"

//height of the table cells for intercahge and intermediate stops
#define HEIGHT_OF_INTERMEDIATE_CELL 36.0f
#define HEIGHT_OF_TERMINAL_CELL 60.0f

@interface JourneyDetailsListViewController ()
-(UITableViewCell *) intermediateCellForJourneyLegStop:(JourneyLegStop *) leg;
-(UITableViewCell *) headerCellForJourneyLegStop:(JourneyLegStop *) leg asArrival:(BOOL) isArrival;
@end

@implementation JourneyDetailsListViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.automaticallyAdjustsScrollViewInsets = NO;
    
    journeyStops = [_journeyToDisplay journeyAsJourneyLegStops];
    [_journeyDetailsTableView setDelegate:self];
    [_journeyDetailsTableView setDataSource:self];
    [_journeyDetailsTableView registerNib:[UINib nibWithNibName:@"JourneyLegViewCell" bundle:nil]
                   forCellReuseIdentifier:@"JourneyLegViewCell"];
    [_journeyDetailsTableView registerNib:[UINib nibWithNibName:@"JourneyLegIntermediateStopCell" bundle:nil]
                   forCellReuseIdentifier:@"JourneyLegIntermediateStopCell"];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    int cellIndex = [indexPath indexAtPosition:1];
    
    if([[journeyStops objectAtIndex:cellIndex] stopType] == kDepartureStop){
        return [self headerCellForJourneyLegStop:[journeyStops objectAtIndex:cellIndex] asArrival:NO];
    }else if(([[journeyStops objectAtIndex:cellIndex] stopType] == kArrivalStop) && (cellIndex == [journeyStops count]-1)){ //if it is an arrival stop and the last one in the array
           return [self headerCellForJourneyLegStop:[journeyStops objectAtIndex:cellIndex] asArrival:YES];
    }else{
        //must be an intermediate stop or an arrival stop we treat as intermediate
        return [self intermediateCellForJourneyLegStop:[journeyStops objectAtIndex:cellIndex]];
    }
}

-(UITableViewCell *) intermediateCellForJourneyLegStop:(JourneyLegStop *) leg{
    JourneyLegIntermediateStopCell *cell = [_journeyDetailsTableView dequeueReusableCellWithIdentifier:@"JourneyLegIntermediateStopCell"];
    if(cell == nil){
        cell = [[JourneyLegIntermediateStopCell alloc] init];
     //   cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    [[cell departureTimeLabel] setText:[dateFormatter stringFromDate:[leg departureDate]]];
    [[cell destinationLabel]  setText:[leg name]];
    return cell;
}
-(UITableViewCell *) headerCellForJourneyLegStop:(JourneyLegStop *) leg asArrival:(BOOL) isArrival{
    JourneyLegViewCell *cell = [_journeyDetailsTableView dequeueReusableCellWithIdentifier:@"JourneyLegViewCell"];
    if(cell == nil){
        cell = [[JourneyLegViewCell alloc] init];
    }
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    
    if ([leg transitType] == kWalk){
        [[cell serviceCodelabel] setText:[NSString stringWithFormat:@"%0.1fkm", [leg legLengthInMeters]/1000.0]]; //turn into km
    }else{
        [[cell serviceCodelabel] setText:[leg userReadableServiceCode]];
    }
    
    
    if(isArrival){ //all the departure dates are arrival dateas
        [[cell stopLabel] setText:[leg name]];
        [[cell stopTimeLabel] setText:[NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"Arrive", @"Arrive String"), [dateFormatter stringFromDate:[leg departureDate]]]];
        [[cell imageView] setImage:[UIImage imageNamed:@"destination_icon.png"]];
        [[cell serviceCodelabel] setText:@""];
    }else{
        [[cell stopLabel] setText:[leg name]];
        [[cell stopTimeLabel] setText:[dateFormatter stringFromDate:[leg departureDate]]];
        [[cell imageView] setImage:[[TransitModelUtilities sharedTransitModelUtilities] smallIconForTransitType:[leg transitType]]];
    }
    
    return cell;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [journeyStops count];
}

-(void) tableView:(UITableView *) tableView didSelectRowAtIndexPath: (NSIndexPath *) indexPath{
    NSLog(@"USER Tapped Cell");
    if([_delegate respondsToSelector:@selector(userDidSelectJourneyLegStop:)]){
        [_delegate userDidSelectJourneyLegStop:[journeyStops objectAtIndex:[indexPath indexAtPosition:1]]];
    }
    //return; // you can't select rows
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if([[journeyStops objectAtIndex:[indexPath indexAtPosition:1]] stopType] == kDepartureStop){
        return HEIGHT_OF_TERMINAL_CELL; //check this with JourneySearchResultsCellViewController. They need to be the same
    }else if(([[journeyStops objectAtIndex:[indexPath indexAtPosition:1]] stopType] == kArrivalStop) && ([indexPath indexAtPosition:1] == [journeyStops count]-1)){ //if it is an arrival stop and the last one in the array treat as departure
        return HEIGHT_OF_TERMINAL_CELL;
    }else{
        return HEIGHT_OF_INTERMEDIATE_CELL;
    }
}


-(void)viewDidUnload {
    [self setJourneyDetailsTableView:nil];
    [super viewDidUnload];
}
@end

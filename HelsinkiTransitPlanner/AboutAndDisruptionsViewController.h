//
//  AboutAndDisruptionsViewController.h
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 04/02/2013.
//  Copyright (c) 2013 David McGookin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HSLDisruptionsCommunicator.h"

@interface AboutAndDisruptionsViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *disruptionsTable;
@property (nonatomic, strong) NSArray *currentDisruptions;
@property (nonatomic, strong) NSArray *currentInformations;
@property (weak, nonatomic) IBOutlet UILabel *copyrightNoticeLabel;
@property (weak, nonatomic) IBOutlet UIView *dummyAdSpacer;

-(void) updateDisruptionInformation;
@end

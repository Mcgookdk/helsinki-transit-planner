//
//  TransitIconAndServiceSubViewController.m
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 30/12/2012.
//  Copyright (c) 2012 David McGookin. All rights reserved.
//

#import "TransitIconAndServiceSubViewController.h"

@implementation TransitIconAndServiceSubViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [_transitIcon setImage:[[TransitModelUtilities sharedTransitModelUtilities] smallIconForTransitType:_transitType]];
    [_servieLabel setText:_transitServiceCode];
    // Do any additional setup after loading the view from its nib.
}
-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setTransitIcon:nil];
    [self setServieLabel:nil];
    [super viewDidUnload];
}
@end

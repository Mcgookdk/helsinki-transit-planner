//
//  ReittiopasQueryProcessor.h
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 21/10/2012.
//  Copyright (c) 2012 David McGookin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "ReittiopasCommunicator.h"
#import "TransitStop.h"
#import "TransitService.h"
#import "TransitRoute.h"
#import "JourneyRoute.h"
#import "JourneyLeg.h"




typedef void (^nearbyStopCompletionBlock)(NSArray * arr, ReturnCodeType retCode);
typedef void (^servicesFromStopCompletionBlock)(NSArray * arr, ReturnCodeType retCode);
typedef void (^fullRouteForServiceCompletionBlock)(TransitRoute * transitRoute, ReturnCodeType retCode);
typedef void (^journeyPlanningCompletionBlock)(NSArray *arr, ReturnCodeType retCode);


@interface ReittiopasQueryProcessor : NSObject{
}

-(void) findFullRouteForService:(NSString *) serviceCode withCompletionBlockHandler:(void(^)(TransitRoute * transitRoute, ReturnCodeType retCode)) servicesFromStopCompletionBlock;
-(void) findStopsNearLocation:(CLLocationCoordinate2D) loc withCompletionBlockHandler:(void(^)(NSArray * arr, ReturnCodeType retCode)) nearbyStopCompletionBlock;
-(void) findServicesDepartingFromStop: (TransitStop *) stop withCompletionBlockHandler:(void(^)(NSArray *, ReturnCodeType retCode)) servicesFromStopCompletionBlock;
-(void) findJourneyPlanFrom:(CLLocationCoordinate2D) departureLocation toLocation: (CLLocationCoordinate2D) arrivalLocation withTime:(NSDate *) date arrivalDeparture:(NSString *) arrivalDeparture withCompletionBlock:(void(^)(NSArray *, ReturnCodeType retCode)) journeyPlanningCompletionBlock;

//private methods

-(NSArray *) processServiceResults:(NSArray *) results fromStop:(NSString *) stopCode withName:(NSString *) name;
-(NSArray *) processNearbyStopResults:(NSArray *) results;
-(TransitType) determineTransitTypeFromJoreCode:(NSString *) joreCode;
-(BOOL) shouldMergeStop:(TransitStop *) stopA toStop:(TransitStop *) stopB; //does the lossy merging of stops

@end

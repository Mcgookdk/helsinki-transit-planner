//
//  TransitStopCellView.m
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 21/10/2012.
//  Copyright (c) 2012 David McGookin. All rights reserved.
//

#import "TransitStopCellView.h"

@implementation TransitStopCellView

-(NSString *) reuseIdentifier{
    return @"TransitStopCellView";
}

@end

//
//  JourneyDetailsViewController.h
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 13/02/2013.
//  Copyright (c) 2013 David McGookin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "JourneyRoute.h"
#import "JourneyDetailsListViewController.h"
#import "JourneyDetailsMapViewController.h"


@interface JourneyDetailsViewController : UIViewController<JourneyDetailsListViewControllerDelegate, UIActionSheetDelegate, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate>{
    
    JourneyDetailsListViewController * _journeyDetailsList;
    JourneyDetailsMapViewController * _journeyDetailsMap;
    UIBarButtonItem *_showHideListViewButton;
    UIBarButtonItem *_shareJourneyDetailsButton;
    BOOL _listViewIsVisible;
    BOOL _firstTime;
}

@property (weak, nonatomic) IBOutlet UIView *dummyAdSpacer;
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (strong, nonatomic) JourneyRoute *journeyToDisplay;
@property (weak, nonatomic) IBOutlet UILabel *departureLocationLabel;
@property (weak, nonatomic) IBOutlet UILabel *destinationLocationLabel;
@property (weak, nonatomic) IBOutlet UILabel *fromLabel;
@property (weak, nonatomic) IBOutlet UILabel *toLabel;


@end

//
//  AddNewFavoriteViewController.m
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 03/11/2012.
//  Copyright (c) 2012 David McGookin. All rights reserved.
//

#import "AddNewFavoriteViewController.h"
#import "FavoritePlacesStore.h"
#import "ApplicationUtilities.h"

@implementation AddNewFavoriteViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _tempFavoritePlace = [[FavoritePlace alloc] init];
    _tempFavoritePlace.coordinate = _initialLocation.coordinate;
    [_favoriteTitleTextView setDelegate:self];
    [[self view] setBackgroundColor:[ApplicationUtilities defaultImageColorForTextureBackround]];
    [_mapView setDelegate:self];
    [_mapView addAnnotation:_tempFavoritePlace];
    MKCoordinateRegion mapRegion;
    mapRegion.center = [_tempFavoritePlace coordinate];
    mapRegion.span.latitudeDelta =0.001;
    mapRegion.span.longitudeDelta=0.001;
    [_mapView setCenterCoordinate:[_tempFavoritePlace coordinate]];
    [_mapView setRegion:mapRegion animated:NO];
    
            self.edgesForExtendedLayout = UIRectEdgeNone;
        [_toolbarMapTypeSegmentedControl setTitle:NSLocalizedString(@"Map", @"Map Segmented Control Label") forSegmentAtIndex:0];
        [_toolbarMapTypeSegmentedControl setTitle:NSLocalizedString(@"Satellite", @"Satellite Segmented Control Label") forSegmentAtIndex:1];
        [_mapTypeSelectionControl removeFromSuperview];
        
        //set labels for localization
    [_addNewFavoriteInstructionlabel setText:NSLocalizedString(@"Drag the pin to the location of the place you want to save as a favorite", @"Add New Favorite Instruction String")];
    
    [_favoriteTitleTextView setPlaceholder:NSLocalizedString(@"Place Name", @"Place Name Text View Placeholder Text")];
    
    [_mapTypeSelectionControl setTitle:NSLocalizedString(@"Map", @"Map Segmented Control Label") forSegmentAtIndex:0];
    [_mapTypeSelectionControl setTitle:NSLocalizedString(@"Satellite", @"Satellite Segmented Control Label") forSegmentAtIndex:1];

    [[self navigationItem] setTitle:NSLocalizedString(@"New Favorite", @"Add New Favorite Title String")];
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel",@"Cancel Button Text") style:UIBarButtonItemStylePlain target:self action:@selector(cancelNewFavorite:)];
    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Save",@"Save Button Text") style:UIBarButtonItemStylePlain target:self action:@selector(saveNewFavorite:)];
    [[self navigationItem] setLeftBarButtonItem:cancelButton];
    [[self navigationItem] setRightBarButtonItem:saveButton];
  
    
    
}

- (IBAction)userSelectedMapType:(id)sender {
    if([sender selectedSegmentIndex] == 0){
        [_mapView setMapType:MKMapTypeStandard];
    }else{
        [_mapView setMapType:MKMapTypeHybrid];
    }
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
  //  [Flurry logEvent:@"ADD_NEW_FAVORITES_VIEW_DID_APPEAR"];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)saveNewFavorite:(id)sender {
    if ([_favoriteTitleTextView.text length] == 0){
        UIAlertView *alertView =  [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"No Title Error", @"No title for new favorites") message:NSLocalizedString(@"You must supply a name for your favorite before saving it", @"You must supply a name for your favorite before saving it") delegate:nil cancelButtonTitle:NSLocalizedString(@"Dismiss", @"Dismiss Button Title") otherButtonTitles:nil];
        [alertView show];
        return;
    }
    
    [_tempFavoritePlace setTitle:_favoriteTitleTextView.text];
    NSString *uuid = [NSString stringWithFormat:@"POI%d", (int)[NSDate timeIntervalSinceReferenceDate]];
    [_tempFavoritePlace setPlaceCodes:[NSArray arrayWithObject:uuid]];
    [_tempFavoritePlace setPlaceType:kPOI];
    [[FavoritePlacesStore sharedFavoritePlacesStore] addNewPlace:_tempFavoritePlace];
    [self dismissViewControllerAnimated:YES completion: nil];//dismissModalViewControllerAnimated:YES];
}

- (IBAction) doneButtonOnKeyboardPressed:(id)sender {
    [_favoriteTitleTextView resignFirstResponder];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    if ([_favoriteTitleTextView isFirstResponder] && [touch view] != _favoriteTitleTextView) {
        [_favoriteTitleTextView resignFirstResponder];
    }
}
- (IBAction)cancelNewFavorite:(id)sender {
    [self dismissViewControllerAnimated:YES completion: nil];
}
- (void)viewDidUnload {
    [self setMapView:nil];
    [self setFavoriteTitleTextView:nil];
    [self setMapTypeSelectionControl:nil];
    [self setAddNewFavoriteInstructionlabel:nil];
    [super viewDidUnload];
}


#pragma MKMapViewDelegateMethods
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation{
    MKPinAnnotationView *pinView = nil;
    
    if([annotation isMemberOfClass:[FavoritePlace class]]){
		pinView = (MKPinAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:[[FavoritePlace class] description]];
		if(pinView == nil) {
			pinView =  [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:[[FavoritePlace class] description]];
        }
        [pinView setAnnotation:annotation];
        [pinView setCanShowCallout:NO];
        [pinView setDraggable:YES];
        return pinView;
    }
    return nil; //we don't want to deal with this
}

@end

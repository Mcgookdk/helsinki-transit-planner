//
//  RecentGeoPlaceStore.h
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 25/03/2013.
//  Copyright (c) 2013 David McGookin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RecentGeoPlace.h"

@interface RecentGeoPlaceStore : NSObject


@property (strong, nonatomic) NSMutableArray * recentGeoPlaces;

+(RecentGeoPlaceStore *) sharedRecentGeoPlaceStore; //don't create using init. ask for the shared instance

-(void) addRecentGeoPlace:(RecentGeoPlace *) place;
@end

//
//  RecentJourneyStore.m
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 21/03/2013.
//  Copyright (c) 2013 David McGookin. All rights reserved.
//

#import "RecentJourneyStore.h"

#define RECENT_JOURNEYS_FILENAME @"recent_journeys.plist"
#define MAX_RECENT_JOURNEYS_ALLOWED 10

@interface RecentJourneyStore()
-(void) loadJourneysFromFile;
-(void) saveJourneysToFile;
@end

@implementation RecentJourneyStore

static RecentJourneyStore * sharedStore = nil; //the shared instance

+(RecentJourneyStore *) sharedRecentJourneyStore{
    if(sharedStore == nil){
        sharedStore = [[RecentJourneyStore alloc] init];
        [sharedStore loadJourneysFromFile]; //init the store
    }
    return sharedStore;
}

-(id) init{
    self = [super init];
    _recentJourneys = [[NSMutableArray alloc] init];
    return self;
}

-(void) addJourney:(RecentJourney *) journey{
    
    if([_recentJourneys count] == MAX_RECENT_JOURNEYS_ALLOWED){
        RecentJourney *oldestJourney = [_recentJourneys objectAtIndex:0];
        for (RecentJourney *j in _recentJourneys){
            if([[j lastSearchDate] compare:[oldestJourney lastSearchDate]] == NSOrderedDescending){
                oldestJourney = j;
            }
        }
        [_recentJourneys removeObject:oldestJourney];
    }
    [_recentJourneys addObject:journey]; // needs to limit these
    [self saveJourneysToFile];
}



-(void) loadJourneysFromFile{
    NSString *errorDesc = nil;
    NSPropertyListFormat format;
    NSString *plistPath;
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                              NSUserDomainMask, YES) objectAtIndex:0];
    plistPath = [rootPath stringByAppendingPathComponent:RECENT_JOURNEYS_FILENAME];
    if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath]) {
        return; //there is no default file, we must be running for the first time
    }
    NSData *plistXML = [[NSFileManager defaultManager] contentsAtPath:plistPath];
    NSArray *temp = (NSArray *)[NSPropertyListSerialization
                                propertyListFromData:plistXML
                                mutabilityOption:NSPropertyListMutableContainersAndLeaves
                                format:&format
                                errorDescription:&errorDesc];
    if (!temp) {
        NSLog(@"Error reading plist: %@, format: %d", errorDesc, format);
    }
    
    for(NSDictionary *dic in temp){
        RecentJourney *rj = [[RecentJourney alloc] initWithDictionary:dic];
        [_recentJourneys addObject:rj];
    }
}

-(void) saveJourneysToFile{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    for(RecentJourney *rj in _recentJourneys){
        [array addObject:[rj dictionaryRepresentationOfJourney]];
    }
    
    NSString *error;
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *plistPath = [rootPath stringByAppendingPathComponent:RECENT_JOURNEYS_FILENAME];
    
    NSData *plistData = [NSPropertyListSerialization dataFromPropertyList:array
                                                                   format:NSPropertyListXMLFormat_v1_0
                                                         errorDescription:&error];
    if(plistData) {
        [plistData writeToFile:plistPath atomically:YES];
    }
    else {
        NSLog(error);
    }
}






@end

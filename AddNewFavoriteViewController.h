//
//  AddNewFavoriteViewController.h
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 03/11/2012.
//  Copyright (c) 2012 David McGookin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FavoritePlace.h"

@interface AddNewFavoriteViewController : UIViewController <MKMapViewDelegate,UITextFieldDelegate>{
    FavoritePlace * _tempFavoritePlace;
}

@property (weak, nonatomic) IBOutlet UITextField *favoriteTitleTextView;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) CLLocation *initialLocation;
@property (weak, nonatomic) IBOutlet UISegmentedControl *mapTypeSelectionControl;
@property (weak, nonatomic) IBOutlet UISegmentedControl *toolbarMapTypeSegmentedControl;
@property (weak, nonatomic) IBOutlet UILabel *addNewFavoriteInstructionlabel;
@property (weak, nonatomic) IBOutlet UIToolbar *toolbar;

- (IBAction)userSelectedMapType:(id)sender;
- (IBAction)saveNewFavorite:(id)sender;
- (IBAction)cancelNewFavorite:(id)sender;

@end

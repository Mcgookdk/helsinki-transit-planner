//
//  HSLDisruption.m
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 04/02/2013.
//  Copyright (c) 2013 David McGookin. All rights reserved.
//

#import "HSLDisruption.h"

@implementation HSLDisruption
-(id) init{
    self = [super init];
    _text = @"";
    return self;
}

-(NSString *) stringRepresentation{
    return _text;
}
@end

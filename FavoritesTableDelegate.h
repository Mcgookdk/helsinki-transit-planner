//
//  FavoritesTableDelegate.h
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 14/03/2013.
//  Copyright (c) 2013 David McGookin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FavoritePlace.h"

@protocol FavoritesTableDelegateDelegate <NSObject>
-(void) favoritePlaceWasSelected:(FavoritePlace *) place;
-(void) favoriteWasDeletedWithNumberRemaining:(int) favoritesRemaining;
@end

@interface FavoritesTableDelegate : NSObject<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) id<FavoritesTableDelegateDelegate> delegate;

@end




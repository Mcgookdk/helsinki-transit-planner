//
//  JourneyDetailsMapAnnotation.m
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 14/02/2013.
//  Copyright (c) 2013 David McGookin. All rights reserved.
//

#import "JourneyDetailsMapAnnotation.h"
#import <MapKit/MapKit.h>

@implementation JourneyDetailsMapAnnotation

-(id) initWithCoordinate:(CLLocationCoordinate2D) coordinate title:(NSString *) title subTitle:(NSString *) subtitle code:(NSString *) stopCode andTransitType:(TransitType) type{
    
    self = [super init];
    _coordinate = coordinate;
    _title = title;
    _subtitle = subtitle;
    _type = type;
    _stopCode = stopCode;
    _annotationType = kMajorMarker;
    return self;
}

-(NSString *) title{
    //for buses and trams we supply the transit code printed on the stop. Otherwise we don't
    if(_type == kBus || _type == kTram){
        return [NSString stringWithFormat:@"%@ (%@)", _title, _stopCode];
    }else{
        return _title;
    }
}


@end

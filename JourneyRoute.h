//
//  JourneyRoute.h
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 27/12/2012.
//  Copyright (c) 2012 David McGookin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JourneyLeg.h"

@interface JourneyRoute : NSObject

@property (strong, nonatomic) NSArray * journeyLegs; //stroes an array of each leg of the journey. Between 1..N

-(NSString *) journeyDepartureName;
-(NSString *) journeyDestinationName;
-(NSString *) stringRepresentation;
-(NSArray *) journeyAsJourneyLegStops;
@end

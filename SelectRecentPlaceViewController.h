//
//  SelectFavoritePlaceViewController.h
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 14/03/2013.
//  Copyright (c) 2013 David McGookin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FavoritesTableDelegate.h"
#import "FavoritePlacesStore.h"

#import "GeoPlace.h"

@protocol SelectRecentPlaceDelegate <NSObject>
-(void) userDidSelectPlace:(GeoPlace *) place withCallbackString:(NSString *) callbackStr; //nil if no place was selected
@end

@interface SelectRecentPlaceViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>


@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSString * callbackString;

@property (strong, nonatomic) NSString *titleString;

@property (nonatomic, weak) id<SelectRecentPlaceDelegate> delegate;

- (IBAction)userDidCancelSelection:(id)sender;
@end

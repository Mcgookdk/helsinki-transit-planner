//
//  ServicesFromStopViewController.m
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 27/10/2012.
//  Copyright (c) 2012 David McGookin. All rights reserved.
//

#import "ServicesFromStopViewController.h"
#import "ServiceCellView.h"
#import "ApplicationUtilities.h"
#import "TransitRouteViewController.h"
#import "FavoritePlacesStore.h"
#import "TransitModelUtilities.h"
//#import "GADMasterViewController.h"
@implementation ServicesFromStopViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _queryProcessor = [[ReittiopasQueryProcessor alloc] init];
   }
    return self;
}

-(void) viewDidLoad{
    [super viewDidLoad];
    [[self view] setBackgroundColor:[ApplicationUtilities defaultImageColorForTextureBackround]];    
   
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    if (_refreshHeaderView == nil) {
        _refreshHeaderView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - self.servicesTableView.bounds.size.height, 320.0f, self.servicesTableView.bounds.size.height)];
        _refreshHeaderView.backgroundColor = [UIColor colorWithRed:226.0/255.0 green:231.0/255.0 blue:237.0/255.0 alpha:1.0];
        _refreshHeaderView.bottomBorderThickness = 1.0;
        [self.servicesTableView addSubview:_refreshHeaderView];
    }
    
    [_servicesTableView setDelegate:self];
    [_servicesTableView setDataSource:self];
    [_servicesTableView registerNib:[UINib nibWithNibName:@"ServiceCellView" bundle:nil]
            forCellReuseIdentifier:@"ServiceCellView"];
     [_servicesTableView setBackgroundColor:[ApplicationUtilities defaultImageColorForTableViewTextureBackround]];
    
    
#ifdef COMPILED_AS_FULL_VERSION
    //add the favorite button to the navigation bar
    _isFavoriteStop = NO;
    _favoriteStopButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"favorite_place_unselected.png"] style:UIBarButtonItemStylePlain target:self action:@selector(favouriteStopButtonAction:)];
    [[self navigationItem] setRightBarButtonItem:_favoriteStopButton];
    
    
    [_dummyAdViewSpacer removeFromSuperview]; //if this is the full version, remove the spacer bar for the add, and adjust the rest of the view accordingly
    CGRect tempFrame = [self.stopMapView frame];
   // TO FIX tempFrame.origin.y -=kAdBannerHeight;
    [self.stopMapView setFrame:tempFrame];
    
    tempFrame = [self.servicesTableView frame];
 //  to fix   tempFrame.origin.y -=kAdBannerHeight;
 //  to fix  tempFrame.size.height +=kAdBannerHeight;
    [self.servicesTableView setFrame:tempFrame];
    
    
#endif

    [[self navigationItem] setTitle:[_currentStop name]];
    [_stopMapView setDelegate:self];
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
#ifdef COMPILED_AS_LITE_VERSION
    //if it is the free version, put in the add view
   // [[GADMasterViewController sharedGADMasterViewController] resetAdView:self inFrame:[_dummyAdViewSpacer frame]];
#endif
  
    //check if this stop is favorite or not and set the favorite button approrpiately
    if([[FavoritePlacesStore sharedFavoritePlacesStore] areFavoritePlaces:[_currentStop codes]]){ //we set the default as no earlier
        _isFavoriteStop = YES;
        [_favoriteStopButton setImage:[UIImage imageNamed:@"favorite_place_selected.png"]];
    }
    
    
    if(_servicesFromThisStop == nil){
        [self activateAutoPullToRefresh];
        [self refreshServices];
           
    [_stopMapView addAnnotation:_currentStop];
    MKCoordinateRegion mapRegion;
    mapRegion.center = [_currentStop location];
    mapRegion.span.latitudeDelta =0.001;
    mapRegion.span.longitudeDelta=0.001;
    [_stopMapView setCenterCoordinate:[_currentStop location]];
    [_stopMapView setRegion:mapRegion animated:NO];
    }
}
-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
 //   [Flurry logEvent:@"SERVICES_FROM_STOPS_VIEW_ACTIVATED"];
}


-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation{
	
	MKPinAnnotationView *pinView = nil;
	
	if([annotation isMemberOfClass:[TransitStop class]]){
		pinView = (MKPinAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:[[TransitStop class] description]];
		if(pinView == nil) {
			MKAnnotationView *annotationView;
			annotationView =  [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:[[TransitStop class] description]];
			annotationView.opaque = NO;
            annotationView.image = [[TransitModelUtilities sharedTransitModelUtilities] smallIconForTransitType:[_currentStop type]];
         // Just show the image   annotationView.canShowCallout = YES;
            return  annotationView;
        }else{
            pinView.annotation = annotation;
            pinView.image = [[TransitModelUtilities sharedTransitModelUtilities] smallIconForTransitType:[_currentStop type]];
        }
    }
    return nil;
}
-(void) refreshServices{
    _lastUpdateTime = [NSDate date];
    [_queryProcessor findServicesDepartingFromStop:_currentStop withCompletionBlockHandler:^(NSArray * arr, ReturnCodeType retCode) {
       _lastReturnCode = retCode;
        _servicesFromThisStop = arr;
        [_refreshHeaderView setLastRefreshDate:_lastUpdateTime];
      
         [_servicesTableView reloadData];
        
        if(retCode == kNetworkError){
            [self dataSourceDidFinishLoadingNewDataWithSucessStatus:NO];
            UIAlertView *alertView =  [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Error", @"Network Error Alert Title") message:NSLocalizedString(@"Failed to download nearby stops. Check your internet connection.", @"Network Error Alert Message Failed to Download Nearby Stops") delegate:nil cancelButtonTitle:NSLocalizedString(@"Dismiss", @"Dismiss Button Title") otherButtonTitles:nil];
            [alertView show];
        }else{
            [self dataSourceDidFinishLoadingNewDataWithSucessStatus:YES];
        }
    }];
}

-(IBAction) favouriteStopButtonAction: (id) caller{
    if(_isFavoriteStop){ // remove it as a favorite
        [_favoriteStopButton setImage:[UIImage imageNamed:@"favorite_place_unselected.png"]];
        _isFavoriteStop = NO;
        [[FavoritePlacesStore sharedFavoritePlacesStore] removeFavoritePlaceWithCodes:[_currentStop codes]];
    }else{ //add it as a favorite
        [_favoriteStopButton setImage:[UIImage imageNamed:@"favorite_place_selected.png"]];        
        _isFavoriteStop = YES;
        FavoritePlace *newFavePlace = [[FavoritePlace alloc] init];
        [newFavePlace setTitle:[_currentStop name]]; //use name not title so we aren't including the stop code in there
        [newFavePlace setCoordinate:[_currentStop coordinate]];
        [newFavePlace setPlaceCodes:[_currentStop codes]];
        [newFavePlace setPlaceType:[_currentStop type]];
        [[FavoritePlacesStore sharedFavoritePlacesStore] addNewPlace:newFavePlace];
    }
}


//table view /date delegates
-(void) tableView:(UITableView *) tableView didSelectRowAtIndexPath: (NSIndexPath *) indexPath{
    if(_lastReturnCode == kResultOK){
        TransitRouteViewController *routeViewController = [[TransitRouteViewController alloc] init];
        TransitService *service = [_servicesFromThisStop objectAtIndex:[indexPath indexAtPosition:1]];
        [routeViewController setService:service];
        [self.navigationController pushViewController:routeViewController animated:YES];
    }
}


//uitableview datasource delegates
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(_lastReturnCode == kNoResults){
        //return a cell that tells the user this
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"InformationCell"];
        if(cell == nil){
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"InformationCell"];
        }
        [[cell textLabel] setText:NSLocalizedString(@"No Services Were Found", @"No Services Were Found String")];
        [[cell imageView] setImage:[UIImage imageNamed:@"yellow_warning_icon.png"]];
        return cell;
    }
    
    ServiceCellView *cell;
    cell = [_servicesTableView dequeueReusableCellWithIdentifier:@"ServiceCellView"]; //we registered this reuse identifier with a cellView type, so if there isn't one it will automaticaly create a new one.
    
    TransitService *service = [_servicesFromThisStop objectAtIndex:[indexPath indexAtPosition:1]];
    [[cell serviceCodeLabel]  setText:[service userReadableCode]];
    [[cell destinationLabel]  setText:[service destination]];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    [[cell departureTimeLabel] setText:[dateFormatter stringFromDate:[service departureDate]]];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50.0;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(_lastReturnCode == kNoResults){ //no results were found we have a special cell
        return 1;
    }else if(_lastReturnCode == kNetworkError){ //netowrk error no data
        return 0;
    }else{
        return [_servicesFromThisStop count];
    }
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{ //we only have one section
    return [NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"Last Updated", @"Last Updated String"),  [[ApplicationUtilities dateFormatterForDateDisplay] stringFromDate: _lastUpdateTime]];
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
	
	if (scrollView.isDragging) {
		if (_refreshHeaderView.state == EGOOPullRefreshPulling && scrollView.contentOffset.y > -65.0f && scrollView.contentOffset.y < 0.0f && !_reloading) {
			[_refreshHeaderView setState:EGOOPullRefreshNormal];
		} else if (_refreshHeaderView.state == EGOOPullRefreshNormal && scrollView.contentOffset.y < -65.0f && !_reloading) {
			[_refreshHeaderView setState:EGOOPullRefreshPulling];
		}
	}
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
	
	if (scrollView.contentOffset.y <= - 65.0f && !_reloading) {
		_reloading = YES;
		[self refreshServices];
		[_refreshHeaderView setState:EGOOPullRefreshLoading];
		[UIView beginAnimations:nil context:NULL];
		[UIView setAnimationDuration:0.2];
		self.servicesTableView.contentInset = UIEdgeInsetsMake(60.0f, 0.0f, 0.0f, 0.0f);
		[UIView commitAnimations];
	}
}

-(void) activateAutoPullToRefresh{
    _reloading = YES;
    [_refreshHeaderView setState:EGOOPullRefreshLoading];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.2];
    self.servicesTableView.contentInset = UIEdgeInsetsMake(60.0f, 0.0f, 0.0f, 0.0f);
    [UIView commitAnimations];
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    [_customTableHeaderView setBackgroundColor: [ApplicationUtilities defaultApplicationHighlightColor]];
    [_customTableHeaderViewLabel setText:[NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"Last Updated", @"Last Updated String"), [[ApplicationUtilities dateFormatterForDateDisplay] stringFromDate: _lastUpdateTime]]];
    _customTableHeaderViewLabel.textColor = [UIColor whiteColor];
    _customTableHeaderViewLabel.shadowColor = [UIColor blackColor];
    _customTableHeaderViewLabel.shadowOffset = CGSizeMake(0.0f, 1.0f);
    return _customTableHeaderView;
}

- (void)dataSourceDidFinishLoadingNewDataWithSucessStatus:(BOOL) updateSuceeded{
    _reloading = NO;
    _refreshHeaderView.backgroundColor = [UIColor colorWithRed:226.0/255.0 green:231.0/255.0 blue:237.0/255.0 alpha:1.0];
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.3];
	[self.servicesTableView setContentInset:UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f)];
	[UIView commitAnimations];
	
    if(updateSuceeded){
        [_refreshHeaderView setState:EGOOPullRefreshNormal];
    }else{
        [_refreshHeaderView setState:EGOPullRefreshFailed];
    }
}

@end

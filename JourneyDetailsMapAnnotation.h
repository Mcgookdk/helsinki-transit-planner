//
//  JourneyDetailsMapAnnotation.h
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 14/02/2013.
//  Copyright (c) 2013 David McGookin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "TransitStop.h"

typedef enum map_annotation_type_enum {kMajorMarker, kMinorMarker, kMajorAndDestinationMarker} MapAnnotationType;

@interface JourneyDetailsMapAnnotation : NSObject <MKAnnotation>{
    NSString * _title;
}

@property (strong, nonatomic) NSString * stopCode;
@property (readonly, nonatomic) NSString * subtitle;
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (readwrite, nonatomic) TransitType type;
@property (readwrite, nonatomic) MapAnnotationType annotationType; //default kMajorMarker

-(id) initWithCoordinate:(CLLocationCoordinate2D) coordinate title:(NSString *) title subTitle:(NSString *) subtitle code:(NSString *) stopCode andTransitType:(TransitType) type;


@end

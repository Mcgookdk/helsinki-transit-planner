//
//  TransitStop.h
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 20/10/2012.
//  Copyright (c) 2012 David McGookin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "TransitModelUtilities.h"
#import "TransitServiceCodeComparator.h"

@interface TransitStop : NSObject<MKAnnotation>

@property(readwrite, nonatomic) CLLocationCoordinate2D location;
//@property (strong, nonatomic) NSString *code;
@property (strong, nonatomic) NSMutableArray *codes;// if we are merging the stops (as Rettio returns each platform or bus stand as an individual stop) additional relevant stop codes appear here. Otherwise there will be at least 1 entry
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *address; //holds the address of this stop. Also acts as subtitle
@property (readwrite, nonatomic) TransitType type;
@property (strong, nonatomic) NSMutableSet * services; // A list of transit service objects the call at this stop
@property (strong, nonatomic) NSMutableSet * uniqueUserReadableServiceCodes; //a list of unique user readable service codes for the transit stop.  Note that this should not be used for further processing and is for display only
@property (readwrite, nonatomic) int distanceFromSearchInMeters; //only valid as a return from a nearby serach of the area, 0 otherwise

@property (readonly, nonatomic) NSString *title;
@property (readonly, nonatomic) NSString * subtitle;
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
-(NSString *) stringRepresentation;
-(double) distanceInMetersFromStop:(TransitStop *) otherStop;

-(NSArray *) sortedListOfServiceCodes;
@end

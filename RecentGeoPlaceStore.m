//
//  RecentGeoPlaceStore.m
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 25/03/2013.
//  Copyright (c) 2013 David McGookin. All rights reserved.
//

#import "RecentGeoPlaceStore.h"

#define RECENT_GEOPLACES_FILENAME @"recent_geoplaces.plist"
#define MAX_RECENT_GEOPLACES_ALLOWED 15

@interface RecentGeoPlaceStore()
-(void) loadGeoPlacesFromFile;
-(void) saveGeoPlacesToFile;
@end


static RecentGeoPlaceStore * sharedStore = nil; //the shared instance


@implementation RecentGeoPlaceStore


+(RecentGeoPlaceStore *) sharedRecentGeoPlaceStore{
    if(sharedStore == nil){
        sharedStore = [[RecentGeoPlaceStore alloc] init];
        [sharedStore loadGeoPlacesFromFile]; //init the store
    }
    return sharedStore;
}

-(id) init{
    self = [super init];
    _recentGeoPlaces = [[NSMutableArray alloc] init];
    return self;
}

-(void) addRecentGeoPlace:(RecentGeoPlace *) place{
    
    
    //Is there already a geoPlace with this name in the store
    //if so, just update the last used time
    for(RecentGeoPlace *p in _recentGeoPlaces){
        if([p.name compare:place.name options:NSCaseInsensitiveSearch] == NSOrderedSame && [p.address compare:place.address options:NSCaseInsensitiveSearch] == NSOrderedSame){ // compare on the name and address. If either is different then this is a new place. If not, just update the last used date
            p.lastUsedDate = place.lastUsedDate;
            return;
        }
    }
    
    if([_recentGeoPlaces count] == MAX_RECENT_GEOPLACES_ALLOWED){
        RecentGeoPlace *oldestGeoPlace = [_recentGeoPlaces objectAtIndex:0];
        for (RecentGeoPlace *p in _recentGeoPlaces){
            if([[p lastUsedDate] compare:[oldestGeoPlace lastUsedDate]] == NSOrderedDescending){
                oldestGeoPlace = p;
            }
        }
        [_recentGeoPlaces removeObject:oldestGeoPlace];
    }
    [_recentGeoPlaces addObject:place]; // needs to limit these
    [self saveGeoPlacesToFile];
}



-(void) loadGeoPlacesFromFile{
    NSString *errorDesc = nil;
    NSPropertyListFormat format;
    NSString *plistPath;
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                              NSUserDomainMask, YES) objectAtIndex:0];
    plistPath = [rootPath stringByAppendingPathComponent:RECENT_GEOPLACES_FILENAME];
    if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath]) {
        return; //there is no default file, we must be running for the first time
    }
    NSData *plistXML = [[NSFileManager defaultManager] contentsAtPath:plistPath];
    NSArray *temp = (NSArray *)[NSPropertyListSerialization
                                propertyListFromData:plistXML
                                mutabilityOption:NSPropertyListMutableContainersAndLeaves
                                format:&format
                                errorDescription:&errorDesc];
    if (!temp) {
        NSLog(@"Error reading plist: %@, format: %d", errorDesc, format);
    }
    
    for(NSDictionary *dic in temp){
        RecentGeoPlace *rp = [[RecentGeoPlace alloc] initWithDictionary:dic];
        [_recentGeoPlaces addObject:rp];
    }
}

-(void) saveGeoPlacesToFile{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    for(RecentGeoPlace *rp in _recentGeoPlaces){
        [array addObject:[rp dictionaryRepresentationOfRecentGeoPlace]];
    }
    
    NSString *error;
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *plistPath = [rootPath stringByAppendingPathComponent:RECENT_GEOPLACES_FILENAME];
    
    NSData *plistData = [NSPropertyListSerialization dataFromPropertyList:array
                                                                   format:NSPropertyListXMLFormat_v1_0
                                                         errorDescription:&error];
    if(plistData) {
        [plistData writeToFile:plistPath atomically:YES];
    }
    else {
        NSLog(error);
    }
}

@end


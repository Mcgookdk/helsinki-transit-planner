//
//  TransitModelUtilities.m
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 30/12/2012.
//  Copyright (c) 2012 David McGookin. All rights reserved.
//

#import "TransitModelUtilities.h"

@implementation TransitModelUtilities

static TransitModelUtilities *sharedInstance = nil;


-(id) init{
    self = [super init];
    
    _largeIconImages = [[NSArray alloc] initWithObjects:[UIImage imageNamed:@"subway_icon.png"],
                                                    [UIImage imageNamed:@"bus_icon.png"],
                                                    [UIImage imageNamed:@"tram_icon.png"],
                                                    [UIImage imageNamed:@"train_icon.png"],
                                                    [UIImage imageNamed:@"ferry_icon.png"],
                                                    [UIImage imageNamed:@"person_location_image.png"],
                                                    [UIImage imageNamed:@"point_of_interest_icon.png"],
                                                    [UIImage imageNamed:@"NO ICON EXISTS FOR UNKNOWN TYPE"], //unknown type
                                                    nil];

    
    _microIconImages = [[NSArray alloc] initWithObjects:[UIImage imageNamed:@"metro_icon_micro.png"],
                       [UIImage imageNamed:@"bus_icon_micro.png"],
                       [UIImage imageNamed:@"tram_icon_micro.png"],
                       [UIImage imageNamed:@"train_icon_micro.png"],
                       [UIImage imageNamed:@"ferry_icon_micro.png"],
                       [UIImage imageNamed:@"walk_icon_micro.png"],
                       [UIImage imageNamed:@"NO MICRO ICON EXISTS FOR POIs"],
                       [UIImage imageNamed:@"NO ICON EXISTS FOR UNKNOWN TYPE"], //unknown type
                       nil];
 
    _transitColors= [[NSArray alloc] initWithObjects:[UIColor colorWithRed:0.944 green:0.281 blue:0.083 alpha:1.000],
                     [UIColor colorWithRed:0.055 green:0.115 blue:0.344 alpha:1.000],
                      [UIColor colorWithRed:0.124 green:0.594 blue:0.326 alpha:1.000],
                       [UIColor colorWithRed:0.847 green:0.000 blue:0.094 alpha:1.000],
                       [UIColor colorWithRed:0.138 green:0.459 blue:0.673 alpha:1.000],
                        [UIColor colorWithRed:0.038 green:0.000 blue:0.983 alpha:1.000],
                        @"POI HAS NO COLOR",
                        @"UNKNOWN TYPE HAS NO COLOR",
                        nil];
    
    _transitTypeNames = [[NSArray alloc] initWithObjects:@"Metro", @"Bus", @"Tram", @"Train", @"Ferry", @"Walk", @"Point of Interest", @"Unkown Transit Type", nil];
    
    
    
    NSMutableArray * smallIcons = [[NSMutableArray alloc] init];
    
    for(UIImage *currentIcon in _largeIconImages){
        CGRect resizeRect;
        resizeRect.size.width = 40.0;
        resizeRect.size.height = 40.0;//image should be about this many pixels on each side
        resizeRect.origin = (CGPoint){0.0f, 0.0f};
        UIGraphicsBeginImageContextWithOptions(resizeRect.size,NO,[[UIScreen mainScreen ]scale]);         CGContextSetShouldAntialias(UIGraphicsGetCurrentContext(), YES);
        [currentIcon drawInRect:resizeRect];
        UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
        [smallIcons addObject:img];
        UIGraphicsEndImageContext();
    }
    _smallIconImages = [NSArray arrayWithArray:smallIcons];
    
    
    
    
    
    
    
    //smallestimages. These are resized from the LargeIcons
    NSMutableArray * vsmallIcons = [[NSMutableArray alloc] init];
    
    for(UIImage *currentIcon in _largeIconImages){
        CGRect resizeRect;
        resizeRect.size.width = 35.0;
        resizeRect.size.height = 35.0;//image should be about this many pixels on each side
        resizeRect.origin = (CGPoint){0.0f, 0.0f};
        UIGraphicsBeginImageContextWithOptions(resizeRect.size,NO,[[UIScreen mainScreen ]scale]);         CGContextSetShouldAntialias(UIGraphicsGetCurrentContext(), YES);
        [currentIcon drawInRect:resizeRect];
        UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
        [vsmallIcons addObject:img];
        UIGraphicsEndImageContext();
    }
    _smallestIconImages = [NSArray arrayWithArray:vsmallIcons];
    
    return self;
}


+(id) sharedTransitModelUtilities{
	if(sharedInstance == nil){
		sharedInstance = [[TransitModelUtilities alloc] init];
	}
	return sharedInstance;
}

-(UIImage *) largeIconForTransitType:(TransitType) type{
    return [_largeIconImages objectAtIndex:type];
}

-(UIImage *) smallestIconForTransitType:(TransitType) type{
    return [_smallestIconImages objectAtIndex:type];
}

-(UIImage *) smallIconForTransitType:(TransitType) type{
    return [_smallIconImages objectAtIndex:type];
}

-(UIImage *) microIconForTransitType:(TransitType) type{
    return[_microIconImages objectAtIndex:type];
}

-(UIColor *) colorForTransitType:(TransitType) type{
    return [_transitColors  objectAtIndex:type];
}

-(NSString *) readableNameForTransitType:(TransitType) type{
    return [_transitTypeNames objectAtIndex:type];
}
@end

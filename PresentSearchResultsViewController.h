//
//  PresentSearchResultsViewController.h
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 16/05/2013.
//  Copyright (c) 2013 David McGookin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GeoPlace.h"
#import "RecentJourney.h"

@protocol PresentSearchResultsViewControllerDelegate <NSObject>
-(void) userSelectedPlace:(GeoPlace *) place forLocationType:(ArrivalDepartureType) type;
@end

@interface PresentSearchResultsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UINavigationBar *navigationBar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *results;
@property (readwrite, nonatomic) ArrivalDepartureType locationType;
@property (weak, nonatomic) IBOutlet UIView *dummyAdSpacer;

@property (weak, nonatomic) id<PresentSearchResultsViewControllerDelegate>  delegate;
@end

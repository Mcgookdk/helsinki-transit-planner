//
//  JourneyLegStop.m
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 14/02/2013.
//  Copyright (c) 2013 David McGookin. All rights reserved.
//

#import "JourneyLegStop.h"

@implementation JourneyLegStop


-(id) init{
    self = [super init];
    _name =@"";
    _userReadableServiceCode = @"";
    
    return self;
}

@end

//
//  HSLInfo.h
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 04/02/2013.
//  Copyright (c) 2013 David McGookin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HSLInfo : NSObject
@property (strong, nonatomic) NSString * text;

-(NSString *) stringRepresentation;
@end

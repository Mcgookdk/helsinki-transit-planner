//
//  RecentJourney.m
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 21/03/2013.
//  Copyright (c) 2013 David McGookin. All rights reserved.
//
//  Stores a recent journey

#import "RecentJourney.h"

@implementation RecentJourney


-(id) initWithDeparturePlace:(GeoPlace *) departurePlace toArrivalPlace:(GeoPlace *) arrivalPlace onDate:(NSDate *) date asArrivalOrDeparture:(ArrivalDepartureType) arrDep withlastSearchDate:(NSDate *) sDate{
    self = [super init];
    
    _departurePlace = departurePlace;
    _arrivalPlace = arrivalPlace;
    _transitDate = date;
    _arrivalDeparture = arrDep;
    _lastSearchDate = sDate;
    
    return self;
}

-(id) initWithDictionary:(NSDictionary *) dictionary{
    self = [super init];
    
    _arrivalPlace = [[GeoPlace alloc] init];
    _departurePlace = [[GeoPlace alloc] init];
    [_departurePlace setName:[dictionary valueForKey:@"departure_name"]];
    [_arrivalPlace setName:[dictionary valueForKey:@"arrival_name"]];
    [_departurePlace setAddress:[dictionary valueForKey:@"departure_place_address"]];
    [_arrivalPlace setAddress:[dictionary valueForKey:@"arrival_place_address"]];
    [_departurePlace setCoordinate:CLLocationCoordinate2DMake([(NSNumber *)[dictionary valueForKey:@"dep_lat"] doubleValue],[(NSNumber *)[dictionary valueForKey:@"dep_lon"] doubleValue])];
    [_arrivalPlace setCoordinate:CLLocationCoordinate2DMake([(NSNumber *)[dictionary valueForKey:@"arr_lat"] doubleValue],[(NSNumber *)[dictionary valueForKey:@"arr_lon"] doubleValue])];
    [self setArrivalDeparture:[(NSNumber *)[dictionary valueForKey:@"arrival_departure_type"] intValue]];
    [self setLastSearchDate:[dictionary valueForKey:@"last_search_date"]];
    [self setTransitDate:[dictionary valueForKey:@"arrival_departure_date"]];
    
    return self;
}
-(NSDictionary *) dictionaryRepresentationOfJourney{
    NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] init];
    
    [tempDict setValue:[_departurePlace name] forKey:@"departure_name"];
    [tempDict setValue:[_arrivalPlace name] forKey:@"arrival_name"];
    [tempDict setValue:[_departurePlace address] forKey:@"departure_place_address"];
    [tempDict setValue:[_arrivalPlace address] forKey:@"arrival_place_address"];
    [tempDict setValue:[NSNumber numberWithDouble:self.departurePlace.coordinate.latitude] forKey:@"dep_lat"];
    [tempDict setValue:[NSNumber numberWithDouble:self.departurePlace.coordinate.longitude] forKey:@"dep_lon"];
    [tempDict setValue:[NSNumber numberWithDouble:self.arrivalPlace.coordinate.latitude] forKey:@"arr_lat"];
    [tempDict setValue:[NSNumber numberWithDouble:self.arrivalPlace.coordinate.longitude] forKey:@"arr_lon"];
    [tempDict setValue:[NSNumber numberWithInt:[self arrivalDeparture]] forKey:@"arrival_departure_type"];
    [tempDict setValue:self.lastSearchDate forKey:@"last_search_date"];
    [tempDict setValue:self.lastSearchDate forKey:@"arrival_departure_date"];
    return tempDict;
}

@end

//
//  TransitServiceCodeComparator.m
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 06/03/2013.
//  Copyright (c) 2013 David McGookin. All rights reserved.
//

#import "TransitServiceCodeComparator.h"

@implementation TransitServiceCodeComparator


- (NSComparisonResult)compareObject:(id)object1 toObject:(id)object2{
    
    NSString * str1 = (NSString *) object1;
    NSString * str2 = (NSString *) object2;
    
    int code1, code2; //the numerical part of each code
    
    NSString* codeMod1, *codeMod2;
    
    code1 = [[str1 stringByTrimmingCharactersInSet:[NSCharacterSet letterCharacterSet]] intValue]; //remove any letters
    code2 = [[str2 stringByTrimmingCharactersInSet:[NSCharacterSet letterCharacterSet]] intValue]; //remove any letters
    codeMod2 = [str2 stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]]; //get the letter modifiers
    codeMod1 = [str1 stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]]; //get the letter modifiers
    
    
    if(code1 == 0 && code2 == 0){ //then there is no numerical part. This is either the metro or a train service
        return [str1 compare:str2];
    }
    
    if(code1 < code2){
        return NSOrderedAscending;
    }else if(code2 < code1){
        return NSOrderedDescending;
    }else{// the codes are the same, so go on the letter postfix
        return [codeMod1 compare:codeMod2];
    }
}
@end

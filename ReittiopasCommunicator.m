//
//  ReittiopasCommunicator.m
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 19/10/2012.
//  Copyright (c) 2012 David McGookin. All rights reserved.
//
//Interfaces with the Reittiopas Web Service

#import "ReittiopasCommunicator.h"

@implementation ReittiopasCommunicator

//the base string used in all queries
#define BASE_URL_STRING @"http://api.reittiopas.fi/hsl/prod/"



-(id) init{
    self = [super init];
    _username = @"iTransit";
    _password = @"itransitpassword";
    _backgroundQueue = dispatch_queue_create("uk.co.kerrsoftware.helsinkitransitplanner.backgroundqueue", NULL);
    return self;
}

-(NSData *) sendBlockingQuery:(NSString *) query withReturnedCode: (ReturnCodeType *) retCode{
    
    NSString *requestString = [NSString stringWithFormat:@"%@?request=%@&epsg_in=4326&epsg_out=4326&user=%@&pass=%@", BASE_URL_STRING, query, _username,_password]; //use decimal gps coords as well
   // NSLog(@"Sending Request %@", requestString);
    NSURL *requestURL = [NSURL URLWithString:[requestString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]; //spaces cause a problem in the request, so replace with explicit characters
    NSError *error = nil;
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:requestURL];
    NSData *serverReply = [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:nil error:&error];
    if(serverReply != nil){
        NSData * dataResponse = [NSJSONSerialization JSONObjectWithData:serverReply options:0 error: nil];
       // NSLog(@"DEBUG RESPONSE DUMP:%@", dataResponse);
        if(dataResponse == nil){ //if the output is null then there was a sucessful response, but no data in the response
           *retCode =  kNoResults;
           // NSLog(@"NO RESULTS");
        }else{
            *retCode = kResultOK;
        }
        return dataResponse;
    }else{
      //  NSLog(@"Error is %@", error);
        *retCode = kNetworkError;
        return nil;
    }
    
}

-(void) sendQuery:(NSString *)query withCallbackBlock:(void (^)(NSData* data, ReturnCodeType retCode)) communicationRequestCompleted{
    dispatch_async(_backgroundQueue, ^{
        ReturnCodeType returnCode;
        NSData * data = [self sendBlockingQuery:query withReturnedCode:&returnCode];
        
        dispatch_async(dispatch_get_main_queue(),^{
                communicationRequestCompleted(data, returnCode); //call on main  queue
        });
    }); //end background block
}

@end

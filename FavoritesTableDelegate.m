//
//  FavoritesTableDelegate.m
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 14/03/2013.
//  Copyright (c) 2013 David McGookin. All rights reserved.
//

#import "FavoritesTableDelegate.h"
#import "FavoritePlacesStore.h"
#import "ApplicationUtilities.h"

@implementation FavoritesTableDelegate



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    FavoritePlace *currentPlace =   [[[FavoritePlacesStore sharedFavoritePlacesStore] favoritePlaces] objectAtIndex:[indexPath indexAtPosition:1]] ;
    
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FavoriteCell"];
    if(cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"FavoriteCell"];
        [[cell textLabel] setFont:[ApplicationUtilities defaultApplicationFontForTableCell]];
    }
    [cell setBackgroundColor:[ApplicationUtilities defaultImageColorForTableViewTextureBackround]];
    [[cell textLabel] setText:[currentPlace title]];
    [[cell imageView] setImage:[[TransitModelUtilities sharedTransitModelUtilities] largeIconForTransitType: [currentPlace placeType]]];
    return cell;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [[[FavoritePlacesStore sharedFavoritePlacesStore] favoritePlaces] count];
}

-(void) tableView:(UITableView *) tableView didSelectRowAtIndexPath: (NSIndexPath *) indexPath{    
    if ([_delegate respondsToSelector:@selector(favoritePlaceWasSelected:)]) {
        [_delegate favoritePlaceWasSelected:[[[FavoritePlacesStore sharedFavoritePlacesStore] favoritePlaces] objectAtIndex:[indexPath indexAtPosition:1]]];
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if(editingStyle == UITableViewCellEditingStyleDelete){
        [[[FavoritePlacesStore sharedFavoritePlacesStore] favoritePlaces] removeObjectAtIndex:[indexPath indexAtPosition:1]];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationLeft];
        if ([_delegate respondsToSelector:@selector(favoriteWasDeletedWithNumberRemaining:)]){
            [_delegate favoriteWasDeletedWithNumberRemaining:[[[FavoritePlacesStore sharedFavoritePlacesStore] favoritePlaces] count]];
        }
    }
}


@end

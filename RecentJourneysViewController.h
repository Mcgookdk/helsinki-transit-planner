//
//  RecentJourneysViewController.h
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 21/03/2013.
//  Copyright (c) 2013 David McGookin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RecentJourneyStore.h"

@protocol RecentJourneyViewControllerDelegate <NSObject>
-(void) userDidCancelSelection;
-(void) userDidSelectRecentJourney:(RecentJourney *) journey;

@end
@interface RecentJourneysViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>{
    NSDateFormatter * _timeFormatter;
    NSDateFormatter * _dateFormatter;
}

@property id<RecentJourneyViewControllerDelegate> delegate;

@property (weak, nonatomic) IBOutlet UITableView *journeyTable;



- (IBAction)userDidCancel:(id)sender;

@end

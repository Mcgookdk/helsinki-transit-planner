//
//  LocationServer.h
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 18/02/2013.
//  Copyright (c) 2013 David McGookin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@protocol LocationServerDelegate <NSObject>
-(void) didUpdateLocation:(CLLocation *) location;
-(void) locationUpdateDidFail;
-(void) locationServicesAuthorisationStatusDidChange:(CLAuthorizationStatus) status;
@end


@interface LocationServer : NSObject<CLLocationManagerDelegate>{
    CLLocationManager * _locationManager;
    CLRegion *_validRegion;
    BOOL _updatesAreActive;
}

@property (weak, nonatomic) id<LocationServerDelegate> delegate;


-(void) startUpdatingLocation;
-(void) stopUpdatingLocation;
-(void) requestAuthorisationForLocationUpdates;

-(BOOL) isActive; //true iff we are currently searching for a location
-(CLLocation *) getLocation; //returns the most recent CLLocation.
@end



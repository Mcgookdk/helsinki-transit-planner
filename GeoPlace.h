//
//  GeoPlace.h
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 21/03/2013.
//  Copyright (c) 2013 David McGookin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface GeoPlace : NSObject <NSCopying>

@property (strong, nonatomic) NSString * name;
@property (strong, nonatomic) NSString * address;
@property (readwrite, nonatomic) CLLocationCoordinate2D coordinate;

@end

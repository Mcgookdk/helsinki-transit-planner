//
//  TransitService.h
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 20/10/2012.
//  Copyright (c) 2012 David McGookin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TransitStop.h"

@interface TransitService : NSObject{
    NSString *  _userReadableCode;
    TransitType _transitType;
}

@property (strong, nonatomic) NSString *code;
@property (strong, nonatomic) NSString *userReadableCode;
@property (strong, nonatomic) NSString *destination;
@property (readonly, nonatomic) TransitType type;
@property (strong, nonatomic) NSDate *departureDate;

-(NSString *) stringRepresentation;
-(NSComparisonResult) departureDateComparator:(TransitService *)otherService;
@end

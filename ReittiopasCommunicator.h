//
//  ReittiopasCommunicator.h
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 19/10/2012.
//  Copyright (c) 2012 David McGookin. All rights reserved.
//
//Handles basic comms between the Reittiopas System and classes that do more detailed processing
//

#import <Foundation/Foundation.h>

//used for return codes to blocks
typedef enum return_code_type_enum {kResultOK, kNetworkError, kNoResults} ReturnCodeType;

/*@protocol ReittiopasCommunicatorDelegate <NSObject>

-(void) didFailToRecieveData: (int) callbackCode;
-(void) didReceiveData:(NSData *) jsonResponse andCode: (int) callbackCode;

@end
*/
//the typedef of the completion block handler 
typedef void (^communicationRequestCompleted)(NSData* data, ReturnCodeType retCode);

@interface ReittiopasCommunicator : NSObject <NSURLConnectionDelegate>{

NSString * _username;
NSString * _password;
NSMutableData * _downloadedData;
dispatch_queue_t  _backgroundQueue;
int _callbackCode;
}
/*
@property(weak, nonatomic) id<ReittiopasCommunicatorDelegate> delegate;
*/
//send a query using the delegete to get a result
//-(void) sendQuery:(NSString *) query withCallbackCode:(int) callbackCode;
//send a query using a completion block handler to get the result. data is nil if there was an error or no data was recieved from the query
-(void) sendQuery:(NSString *)query withCallbackBlock:(void (^)(NSData* data, ReturnCodeType retCode)) communicationRequestCompleted;


//should not be called in the foreground thread as this requires a block
-(NSData *) sendBlockingQuery:(NSString *) query withReturnedCode: (ReturnCodeType *) retCode;

@end


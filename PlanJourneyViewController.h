//
//  PlanJourneyViewController.h
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 09/11/2012.
//  Copyright (c) 2012 David McGookin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LocationServer.h"
#import "MBProgressHUD.h"
#import "GeoPlace.h"
#import "RecentJourney.h"
#import "RecentJourneyStore.h"
#import "RecentJourneysViewController.h"
#import "SelectRecentPlaceViewController.h"
#import "PresentSearchResultsViewController.h"

@interface PlanJourneyViewController : UIViewController <UITextFieldDelegate, LocationServerDelegate, RecentJourneyViewControllerDelegate, SelectRecentPlaceDelegate, PresentSearchResultsViewControllerDelegate>{
    NSDate * _departureArrivalDate;
    BOOL _dateHasBeenUserChanged;
    NSDateFormatter *_standardDateFormatter;
    LocationServer *_locationServer;
    CLAuthorizationStatus _currentAuthorisationStatus;
    CLLocation *_lastUserLocation;
    MBProgressHUD *_progressHUD;
    UIBarButtonItem *_recentsBarButton;
    GeoPlace * _departurePlace;
    GeoPlace * _arrivalPlace;
    BOOL _shouldSaveDepartureLocation;
    BOOL _shouldSaveArrivalLocation;
    BOOL _shouldSaveJourney; // Will be NO only if the current journey details have or are stored as a recent journey. Is set to yes wherver these are changed
}

@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (strong, nonatomic) IBOutlet UIView *datePickerAccessoryView;
@property (weak, nonatomic) IBOutlet UITextField * departureLocationTextField;
@property (weak, nonatomic) IBOutlet UITextField *arrivalLocationTextField;
@property (weak, nonatomic) IBOutlet UISegmentedControl *arrivalDepartureSegmentedControl;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;
//@property (weak, nonatomic) IBOutlet UILabel *dateTimeDisplayLabel;
@property (weak, nonatomic) IBOutlet UILabel *leavingFromLabel;
@property (weak, nonatomic) IBOutlet UILabel *arrivingAtLabel;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *setDateCancelButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *setDateButton;
@property (strong, nonatomic) IBOutlet UIButton *departureFavoritesButton;
@property (strong, nonatomic) IBOutlet UIButton *destinationFavoritesButton;
@property (weak, nonatomic) IBOutlet UIToolbar *datePickerToolbar;
@property (weak, nonatomic) IBOutlet UIButton *dateChangeButton;

- (IBAction)userRequestedDateChange:(id)sender;
- (IBAction)userRequestedSearch:(id)sender;

//these two are the buttons in the date picker for changing the date
- (IBAction)userDidChangeDate:(id)sender;
- (IBAction)userDidCancelChangeDate:(id)sender;


- (IBAction)userDidSelectDepartureFromFavorites:(id)sender;
- (IBAction)userDidSelectDestinationFromFavorites:(id)sender;


@end

//
//  JourneyDetailsMapViewController.h
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 13/02/2013.
//  Copyright (c) 2013 David McGookin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <QuartzCore/QuartzCore.h>
#import "JourneyRoute.h"
#import "JourneyDetailsMapAnnotation.h"

@interface JourneyDetailsMapViewController : UIViewController<MKMapViewDelegate>{
    NSMutableArray * _polylines;
    MKCoordinateSpan _visibleCoordinateSpan;
    NSMutableArray *_intermediateStopAnnotations;
    BOOL _intermediateStopsAreOnMap;
    UIBarButtonItem *_userHeadingBarButton;
}
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property(weak, nonatomic) IBOutlet UIButton *userHeadingBtn;
@property (strong, nonatomic) JourneyRoute *journeyToDisplay;
@property (weak, nonatomic) IBOutlet UISegmentedControl *mapTypeSelectionControl;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *mapTypeSelectionControlToolbar;
@property (weak, nonatomic) IBOutlet UIToolbar *toolbar;

-(IBAction) startShowingUserHeading:(id)sender;
-(IBAction)changeMapType:(id)sender;

@end

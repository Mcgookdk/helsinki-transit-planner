//
//  NearbyStopsViewController.m
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 21/10/2012.
//  Copyright (c) 2012 David McGookin. All rights reserved.
//

#import "NearbyStopsViewController.h"
#import "TransitStopCellView.h"
#import "TransitService.h"
#import "ServicesFromStopViewController.h"
#import "ApplicationUtilities.h"
#import "TransitModelUtilities.h"
//#import "GADMasterViewController.h"

@interface NearbyStopsViewController ()
-(void) activateAutoPullToRefresh;
-(void) applicationWillResign;
@end

@implementation NearbyStopsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self.navigationItem setTitle:NSLocalizedString(@"Nearby Stops", @"Nearby Stops View Title")];
        _locationServer = [[LocationServer alloc] init];
        [_locationServer setDelegate:self];
        _haveNeverBeenUpdated = YES; //An update has never been attempted
        _currentAuthorisationStatus = kCLAuthorizationStatusNotDetermined;
        [_locationServer requestAuthorisationForLocationUpdates]; // we need to call this post iOS X
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    
       self.edgesForExtendedLayout = UIRectEdgeNone;
       [self setAutomaticallyAdjustsScrollViewInsets:NO];
   
    
    [[self view] setBackgroundColor:[ApplicationUtilities defaultImageColorForTextureBackround]];
    [_nearbyStopsTable setBackgroundColor:[ApplicationUtilities defaultImageColorForTableViewTextureBackround]];
    
    if (_refreshHeaderView == nil) {
        _refreshHeaderView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - self.nearbyStopsTable.bounds.size.height, 320.0f, self.nearbyStopsTable.bounds.size.height)];
        _refreshHeaderView.backgroundColor = [UIColor colorWithRed:226.0/255.0 green:231.0/255.0 blue:237.0/255.0 alpha:1.0];
        _refreshHeaderView.bottomBorderThickness = 1.0;
        [self.nearbyStopsTable addSubview:_refreshHeaderView];
        self.nearbyStopsTable.showsVerticalScrollIndicator = YES;
    }
    
    // Do any additional setup after loading the view from its nib.
    [_nearbyStopsTable setDataSource:self];
    [_nearbyStopsTable setDelegate:self];
    [_nearbyStopsTable registerNib:[UINib nibWithNibName:@"TransitStopCellView" bundle:nil]
            forCellReuseIdentifier:@"TransitStopCellView"];
    
    _queryProcessor = [[ReittiopasQueryProcessor alloc] init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillResign) name:UIApplicationWillResignActiveNotification object:NULL];
    
#ifdef COMPILED_AS_FULL_VERSION
    [_dummyAdSpacerBar removeFromSuperview]; //if this is the full version, remove the spacer bar for the add, and adjust the rest of the view accordingly 
    [self.nearbyStopsTable setFrame:[self.view frame]];
   // [self.view setNeedsLayout];
    
#endif
    
    
}

-(void) viewWillAppear:(BOOL)animated{
      [super viewWillAppear:animated];    
#ifdef COMPILED_AS_LITE_VERSION
    //if it is the free version, put in the add view
 //   [[GADMasterViewController sharedGADMasterViewController] resetAdView:self inFrame:[_dummyAdSpacerBar frame]];
#endif
    
  
    if(_haveNeverBeenUpdated){
           [_locationServer startUpdatingLocation];
        [self activateAutoPullToRefresh];
        [self refreshNearbyStops];
        _haveNeverBeenUpdated = NO; // we have been updated
    }
    
    [self.view setNeedsLayout];

}

-(void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
   // [_locationServer stopUpdatingLocation];
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
  //  [Flurry logEvent:@"NEARBY_STOPS_VIEW_ACTIVATED"];
}

-(void) refreshNearbyStops{
    //kick off the location search.
    //From the delegate do the right thing
    _lastValidLocation = nil;
    [_locationServer startUpdatingLocation];
    [[_refreshHeaderView statusLabel] setText:NSLocalizedString(@"Finding Location", @"Finding Location String")];
}

-(void) searchForNearbyStops{
    CLLocationCoordinate2D coords = [_lastValidLocation coordinate]; //
    [[_refreshHeaderView statusLabel] setText:NSLocalizedString(@"Searching For Nearby Stops", @"Searching for Nearby Stops String")];
    [_queryProcessor findStopsNearLocation:coords withCompletionBlockHandler:^(NSArray* arr, ReturnCodeType retCode){
        _nearbyStops = arr;
        _lastReturnCode = retCode;
        [_nearbyStopsTable reloadData]; //reload the data
        _lastUpdateTime = [NSDate date];
        [_refreshHeaderView setLastRefreshDate:_lastUpdateTime];
        
        
        if(retCode == kNetworkError){
            //network error
            _lastReturnCode = retCode;
            
            UIAlertView *alertView =  [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Error", @"Network Error Alert Title") message:NSLocalizedString(@"Failed to download nearby stops. Check your internet connection.", @"Network Error Alert Message") delegate:nil cancelButtonTitle:NSLocalizedString(@"Dismiss", @"Dismiss Button Title")  otherButtonTitles:nil];
            [alertView show];
            [self dataSourceDidFinishLoadingNewDataWithSucessStatus:NO];
        }else{
            [self dataSourceDidFinishLoadingNewDataWithSucessStatus:YES];
            
        }
    }];
    
}

//uitableview datasource delegates
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(_lastReturnCode == kNoResults){
        //retunr a cell that tells the user this
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"InformationCell"];
        if(cell == nil){
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"InformationCell"];
        }
       
        [[cell textLabel] setText:NSLocalizedString(@"No Nearby Stops Were Found", @"No Nearby Stops Found String")];
        [[cell imageView] setImage:[UIImage imageNamed:@"yellow_warning_icon.png"]];
        return cell;
    }
    
    
    
    //if it isn't a special cell keep going
    TransitStopCellView *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:@"TransitStopCellView"];
   
    TransitStop *currentStop = [_nearbyStops objectAtIndex:[indexPath indexAtPosition:1]] ;
    [[cell services] setTextColor:[UIColor grayColor]];
    [[cell title] setText:[currentStop name]];
    
    NSMutableString *serviceNumbers = [[NSMutableString alloc] init];
    
    NSArray *sortedReadableStopCodes =  [currentStop sortedListOfServiceCodes];//[[NSMutableArray alloc] initWithArray: [currentStop.uniqueUserReadableServiceCodes allObjects]];
    for(NSString * readableStopCode in sortedReadableStopCodes){
        [serviceNumbers appendString:[NSString stringWithFormat:@"%@ ", readableStopCode]];
    }
    
    /* This is where we define the ideal font that the Label wants to use.
     Use the font you want to use and the largest font size you want to use. */
    UIFont *font = [[cell services] font];
    
    int i;
    /* Time to calculate the needed font size.
     This for loop starts at the largest font size, and decreases by two point sizes (i=i-2)
     Until it either hits a size that will fit or hits the minimum size we want to allow (i > 10) */
    for(i = 16; i >= 12; i=i-1)
    {
        // Set the new font size.
        font = [font fontWithSize:i];
        // You can log the size you're trying: NSLog(@"Trying size: %u", i);
        
        /* This step is important: We make a constraint box
         using only the fixed WIDTH of the UILabel. The height will
         be checked later. */
        CGSize constraintSize = CGSizeMake(230.0, MAXFLOAT); //Width of the label
        
        // This step checks how tall the label would be with the desired font.
        CGSize labelSize = [serviceNumbers sizeWithFont:font constrainedToSize:constraintSize lineBreakMode:NSLineBreakByWordWrapping];
        
        /* Here is where you use the height requirement!
         Set the value in the if statement to the height of your UILabel
         If the label fits into your required height, it will break the loop
         and use that font size. */
        if(labelSize.height <= 49.0f){ //height of the label in the Cell
            break;
        }
    }
    // You can see what size the function is using by outputting: NSLog(@"Best size is: %u", i);
    
    // Set the UILabel's font to the newly adjusted font.
    [[cell services] setFont:font];
    
    
    [[cell services]  setText: serviceNumbers];
    [[cell thumbnailImage] setImage:[[TransitModelUtilities sharedTransitModelUtilities] largeIconForTransitType:[currentStop type]]];//get the thumbnail image from the first service object. We implicitly assume that there are not more than one transport type at each stop
    [[cell distanceToStop] setText:[NSString stringWithFormat:@"%dm", [currentStop distanceFromSearchInMeters]]];
    
    
    // NSLog(@"Setting text for %@ withServices %@", [currentStop name], sortedReadableStopCodes);
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70.0;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(_lastReturnCode == kNoResults){ //no results were found we have a special cell
        return 1;
    }else if(_lastReturnCode == kNetworkError){ //netowrk error no data
        return 0;
    }else{
        return [_nearbyStops count];
    }
}



-(void) tableView:(UITableView *) tableView didSelectRowAtIndexPath: (NSIndexPath *) indexPath{
    if(_lastReturnCode == kResultOK){ //otherwise it might crash if we have an error cell
        ServicesFromStopViewController *servicesFromStopViewController = [[ServicesFromStopViewController alloc] init];
        [servicesFromStopViewController setCurrentStop:[_nearbyStops objectAtIndex:[indexPath indexAtPosition:1]]] ;
        [self.navigationController pushViewController:servicesFromStopViewController animated:YES];
    }
}



#pragma mark -
#pragma mark ScrollView Callbacks
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
	
	if (scrollView.isDragging) {
		if (_refreshHeaderView.state == EGOOPullRefreshPulling && scrollView.contentOffset.y > -65.0f && scrollView.contentOffset.y < 0.0f && !_reloading) {
			[_refreshHeaderView setState:EGOOPullRefreshNormal];
		} else if (_refreshHeaderView.state == EGOOPullRefreshNormal && scrollView.contentOffset.y < -65.0f && !_reloading) {
			[_refreshHeaderView setState:EGOOPullRefreshPulling];
		}
	}
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
	
	if (scrollView.contentOffset.y <= - 65.0f && !_reloading) {
		_reloading = YES;
		[self refreshNearbyStops];
		[_refreshHeaderView setState:EGOOPullRefreshLoading];
		[UIView beginAnimations:nil context:NULL];
		[UIView setAnimationDuration:0.2];
		self.nearbyStopsTable.contentInset = UIEdgeInsetsMake(60.0f, 0.0f, 0.0f, 0.0f);
		[UIView commitAnimations];
	}
}
-(void) activateAutoPullToRefresh{
    _reloading = YES;
    [_refreshHeaderView setState:EGOOPullRefreshLoading];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.2];
    self.nearbyStopsTable.contentInset = UIEdgeInsetsMake(60.0f, 0.0f, 0.0f, 0.0f);
    [UIView commitAnimations];
}
#pragma mark -
#pragma mark refreshHeaderView Methods

- (void)dataSourceDidFinishLoadingNewDataWithSucessStatus:(BOOL) updateSuceeded{
    _reloading = NO;
    _refreshHeaderView.backgroundColor = [UIColor colorWithRed:226.0/255.0 green:231.0/255.0 blue:237.0/255.0 alpha:1.0];
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.3];
	[self.nearbyStopsTable setContentInset:UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f)];
	[UIView commitAnimations];
	
    if(updateSuceeded){
        [_refreshHeaderView setState:EGOOPullRefreshNormal];
    }else{
        [_refreshHeaderView setState:EGOPullRefreshFailed];
    }
}


-(void) didUpdateLocation:(CLLocation *) location{
    _lastValidLocation = location;
    [_locationServer stopUpdatingLocation];
    NSLog(@"LOCATION UPDATE %@", location);
    [self searchForNearbyStops];
}

-(void) locationUpdateDidFail{
    [self dataSourceDidFinishLoadingNewDataWithSucessStatus:NO];
    NSLog(@"NEARBY STOPS LOCATION UPDATE FAILED");
    if(_currentAuthorisationStatus == kCLAuthorizationStatusDenied || _currentAuthorisationStatus == kCLAuthorizationStatusRestricted){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Location Services are Turned Off", @"Location Services Are Off Alert Title") message: NSLocalizedString(@"You cannot use Current Location as your departure when Location Services are turned off.", @"Location Services are Turned Off Message") delegate:nil cancelButtonTitle:NSLocalizedString(@"Dismiss", @"Dismiss Button Title") otherButtonTitles: nil];
        [alert show];
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Location Error", @"Location Error Alert Title") message:NSLocalizedString(@"Your location could not be determined. Please try again later", @"Location Error Alert Message") delegate:nil cancelButtonTitle:NSLocalizedString(@"Dismiss", @"Dismiss Button Title") otherButtonTitles: nil];
        [alert show];

    }
}
-(void) locationServicesAuthorisationStatusDidChange:(CLAuthorizationStatus) status{
    
    _currentAuthorisationStatus = status;
      NSLog(@"NEARBY STOPS AUTHORISATION STATUS CHANGED %d", status);
    if(status == kCLAuthorizationStatusDenied || status == kCLAuthorizationStatusRestricted ||
       status == kCLAuthorizationStatusNotDetermined){
        [self dataSourceDidFinishLoadingNewDataWithSucessStatus:NO];
    }else{ // we assume the location services were authorised, so fire an auto refresh
            [self activateAutoPullToRefresh];
            [self refreshNearbyStops];
    }
}

-(void) applicationWillResign{ //if we are searching for location whilst the app goes background. Bad things will happen
    
    if([_locationServer isActive]){
        // NSLog(@"APPLICATION WILL RESIGN");
        [self dataSourceDidFinishLoadingNewDataWithSucessStatus:NO];
        [_locationServer stopUpdatingLocation];
    }
}
@end

//
//  JourneySearchResultsViewController.h
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 28/12/2012.
//  Copyright (c) 2012 David McGookin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JourneyRoute.h"

@interface JourneySearchResultsViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *journeys;
@property (strong, nonatomic) IBOutlet UIView *resultsHeadingView;
@property (weak, nonatomic) IBOutlet UILabel *departureLocation;
@property (weak, nonatomic) IBOutlet UILabel *destinationLocation;
@property (weak, nonatomic) IBOutlet UIView *dummyAdSpacer;
@property (weak, nonatomic) IBOutlet UILabel *fromLabel;
@property (weak, nonatomic) IBOutlet UILabel *toLabel;


@end

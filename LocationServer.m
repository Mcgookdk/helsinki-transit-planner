//
//  LocationServer.m
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 18/02/2013.
//  Copyright (c) 2013 David McGookin. All rights reserved.
//

#import "LocationServer.h"
#import "AppDelegate.h"

#define MINIMUM_ACCEPTABLE_GPS_ACCURACY 150.0

@implementation LocationServer

-(id) init{
    self = [super init];
    _locationManager = [[CLLocationManager alloc] init];
    [_locationManager setDelegate:self];
 
    [_locationManager setDesiredAccuracy:kCLLocationAccuracyBestForNavigation];
    _validRegion  = [[CLRegion alloc] initCircularRegionWithCenter:CLLocationCoordinate2DMake(60.169845,24.938551) radius:101000 identifier:@"VALID REGION FOR REAL COORD RETURN"];// if the location falls out of this area, and is accurate enough, return the center of Helskinki as the coordiate
    return self;
}

-(void) startUpdatingLocation{
    [_locationManager startUpdatingLocation];
    _updatesAreActive = YES;
}

-(void) stopUpdatingLocation{
    [_locationManager stopUpdatingLocation];
    _updatesAreActive = NO;
}


-(BOOL) isActive{
    return _updatesAreActive;
}

-(CLLocation *) getLocation{
   CLLocation *location =  [_locationManager location];
    if(![_validRegion containsCoordinate:location.coordinate]){
        location =  [[CLLocation alloc] initWithLatitude:60.169845 longitude:24.938551];
    }
    return location;
}

-(void) requestAuthorisationForLocationUpdates{
       [_locationManager requestWhenInUseAuthorization ]; //request authorisation

}

//this method is depricated, but necessary for iOS 5.1 compatability
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
    
    NSLog(@"Location Manager Did Update Locations");
   
    CLLocation * location = newLocation;
    if([location horizontalAccuracy] <= MINIMUM_ACCEPTABLE_GPS_ACCURACY){
        
        if([_validRegion containsCoordinate:[location coordinate]]){
        //should return it
        if ([_delegate respondsToSelector:@selector(didUpdateLocation:)]) {
            [_delegate didUpdateLocation:location];
        }
        }else{
            NSLog(@"NOT IN HELSINKI, RETURNING THE FAKE LOCATION");
            [(AppDelegate *)[[UIApplication sharedApplication] delegate] userIsOutsideHelsinkiRegion];
            if ([_delegate respondsToSelector:@selector(didUpdateLocation:)]){
                CLLocation *fakeLocation = [[CLLocation alloc] initWithLatitude:60.169845 longitude:24.938551]; //center of helsinki. Use this one for release
               //  CLLocation *fakeLocation = [[CLLocation alloc] initWithLatitude:  60.168630 longitude: 24.961844];
                
                [_delegate didUpdateLocation:fakeLocation];
            }
        }
    }
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    if(status == kCLAuthorizationStatusAuthorized){
        NSLog(@"Location updates are  Authorised");
    }else if(status == kCLAuthorizationStatusDenied){
        NSLog(@"Location updates are denied");
    }else if(status == kCLAuthorizationStatusNotDetermined){
        NSLog(@"Location update status is unknown");
    }else{
        NSLog(@"Locations updates are restricted");
    }
    if ([_delegate respondsToSelector:@selector(locationServicesAuthorisationStatusDidChange:)]) {
        [_delegate locationServicesAuthorisationStatusDidChange:status];
    }
}




- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    NSLog(@"DID FAIL TO UPDATE LOCATION WITH ERROR %@", error);
    if ([_delegate respondsToSelector:@selector(locationUpdateDidFail)]) {
        [_delegate locationUpdateDidFail];
    }
}

@end

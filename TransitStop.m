//
//  TransitStop.m
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 20/10/2012.
//  Copyright (c) 2012 David McGookin. All rights reserved.
//

#import "TransitStop.h"
#import "TransitService.h"
@implementation TransitStop
    
-(id) init{
      self =  [super init];
        _codes = [[NSMutableArray alloc] init];
        _services = [[NSMutableSet alloc] init];
        _uniqueUserReadableServiceCodes = [[NSMutableSet alloc] init];
        return self;
}

-(NSString *) stringRepresentation{
    NSMutableString *stopCodesString = [[NSMutableString alloc] init];
    for(NSString *ss in _codes){
        [stopCodesString appendString:[NSString stringWithFormat:@", %@", ss]];
    }
    
    
   NSString *baseString = [NSString stringWithFormat:@"%@: %@ transit type %d %d meters away at Lat: %f Lon: %f", stopCodesString, self.name, self.type, self.distanceFromSearchInMeters, self.location.latitude, self.location.longitude];
    
    NSMutableString *servicesString = [[NSMutableString alloc] init];
    for(TransitService *ts in self.services){
        [servicesString appendString:[ts stringRepresentation]];
    }
    
    return [NSString stringWithFormat:@"%@ Services: %@", baseString, servicesString];
}


//implmenetation of the MKannotation properties

-(NSString *) title{
    //for buses and trams we supply the transit code printed on the stop. Otherwise we don't
    if(_type == kBus || _type == kTram){
    return [NSString stringWithFormat:@"%@ (%@)", _name, [_codes objectAtIndex:0]];
    }else{
        return _name;
    }
}

-(NSString *) subtitle{
    return _address;
}

-(CLLocationCoordinate2D) coordinate{
    return _location;
}



-(NSArray *) sortedListOfServiceCodes{
    NSSortDescriptor * sortDesc = [[TransitServiceCodeComparator alloc] initWithKey:@"self" ascending:YES ];
    return [_uniqueUserReadableServiceCodes  sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDesc]];
}

-(double) distanceInMetersFromStop:(TransitStop *) otherStop{
    MKMapPoint p1 = MKMapPointForCoordinate([self coordinate]);
	MKMapPoint p2 = MKMapPointForCoordinate([otherStop coordinate]);
    
	//Calculate distance in meters
	return MKMetersBetweenMapPoints(p1, p2);}
@end

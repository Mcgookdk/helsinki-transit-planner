//
//  JourneyLegStop.h
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 14/02/2013.
//  Copyright (c) 2013 David McGookin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TransitStop.h"

typedef enum stopType_enum {kDepartureStop, kArrivalStop, kIntermediateStop} StopType;

@interface JourneyLegStop : NSObject

@property(strong, nonatomic) NSString * name;
@property (strong, nonatomic) NSDate * departureDate;
@property (readwrite, nonatomic) CLLocationCoordinate2D location;
@property (readwrite, nonatomic) TransitType transitType;
@property (strong,nonatomic) NSString * stopCode;
@property (strong, nonatomic) NSString * userReadableServiceCode; //empty for kWalk
@property (readwrite, nonatomic) StopType stopType; // what kind of stop is this on our route? A destination where we change or just a stop we pass through.
@property (readwrite, nonatomic) double legLengthInMeters; //length of this leg in meters. Valid only with kWalk and KDestinationStopType

@end

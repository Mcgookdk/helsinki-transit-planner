//
//  PlanJourneyViewController.m
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 09/11/2012.
//  Copyright (c) 2012 David McGookin. All rights reserved.
//

#import <AddressBookUI/AddressBookUI.h>
#import "PlanJourneyViewController.h"
#import "ReittiopasQueryProcessor.h"
#import "JourneySearchResultsViewController.h"
#import "ApplicationUtilities.h"
#import "MBProgressHUD.h"
#import "SelectRecentPlaceViewController.h"
#import "FavoritePlacesStore.h"
#import "RecentGeoPlaceStore.h"
#import "PresentSearchResultsViewController.h"
//#import "GADMasterViewController.h"

@interface PlanJourneyViewController ()
-(void) presentDatePicker;
-(void) dismissDatePicker;
-(void) searchForJourneysFromDeparture:(CLLocationCoordinate2D) departureLocation toDestination:(CLLocationCoordinate2D) destinationLocation;
-(void) determineLocationFromName:(GeoPlace *) place isArrivalPlace:(BOOL) isArrival;
@end

@implementation PlanJourneyViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.navigationItem.title = NSLocalizedString(@"Plan Journey", @"Plan Journey View Title");
        _locationServer = [[LocationServer alloc] init];
        [_locationServer setDelegate:self];
        _currentAuthorisationStatus = kCLAuthorizationStatusNotDetermined;
        _shouldSaveJourney = YES; //Should be YES to begin with as these details are not already stored
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [_datePickerToolbar setBarStyle:UIBarStyleDefault];

    // Do any additional setup after loading the view from its nib.
    [_departureLocationTextField setDelegate:self];
    [_arrivalLocationTextField setDelegate:self];
    _standardDateFormatter = [ApplicationUtilities dateFormatterForDateDisplay];
    
    [[self view] setBackgroundColor:[ApplicationUtilities defaultImageColorForTextureBackround]];
    
    UIImage *buttonImage = [[UIImage imageNamed:@"greenButton.png"]
                            resizableImageWithCapInsets:UIEdgeInsetsMake(18, 18, 18, 18)];
    UIImage *buttonImageHighlight = [[UIImage imageNamed:@"greenButtonHighlight.png"]
                                     resizableImageWithCapInsets:UIEdgeInsetsMake(18, 18, 18, 18)];
    // Set the background for any states you plan to use
    [_searchButton setBackgroundImage:buttonImage forState:UIControlStateNormal]
    ;
    [_searchButton setBackgroundImage:buttonImageHighlight forState:UIControlStateHighlighted];
    
    //Setup our HUD for location updates
    _progressHUD =  [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.view addSubview:_progressHUD];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillResign) name:UIApplicationWillResignActiveNotification object:NULL];
    
    //setup the label titles
    [_leavingFromLabel setText:NSLocalizedString(@"Leaving From", @"Leaving From String")];
    [_arrivingAtLabel setText:NSLocalizedString(@"Arriving At", @"Arriving At String")];
    [_departureLocationTextField setPlaceholder:NSLocalizedString(@"Current Location", @"Current Location TextField Placeholder String")];
    [_arrivalLocationTextField setPlaceholder:NSLocalizedString(@"Destination", @"Destination TextField Placeholder String")];
    [_searchButton setTitle:NSLocalizedString(@"Search", @"Search Button Title Text") forState:UIControlStateNormal];
    
    [_arrivalDepartureSegmentedControl setTitle:NSLocalizedString(@"Departing", @"Departing Segmented Control Label") forSegmentAtIndex:0];
    [_arrivalDepartureSegmentedControl setTitle:NSLocalizedString(@"Arriving", @"Arriving Segmented Control Label") forSegmentAtIndex:1];
    
    [_setDateButton setTitle:NSLocalizedString(@"Set Date", @"Set Date Button Label")];
    [_setDateCancelButton setTitle:NSLocalizedString(@"Cancel",@"Cancel Button Text")];
    
#ifdef COMPILED_AS_FULL_VERSION
    _recentsBarButton= [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemBookmarks  target:self action:@selector(chooseRecentJourney:)];
    [[self navigationItem] setLeftBarButtonItem:_recentsBarButton];

    
    _departureFavoritesButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    [_departureFavoritesButton addTarget:self action:@selector(userDidSelectDepartureFromFavorites:) forControlEvents:UIControlEventTouchDown];
    
    [_departureLocationTextField setRightView:_departureFavoritesButton];
    [_departureLocationTextField setRightViewMode:UITextFieldViewModeAlways];
    
    _destinationFavoritesButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    [_destinationFavoritesButton addTarget:self action:@selector(userDidSelectDestinationFromFavorites:) forControlEvents:UIControlEventTouchDown];
    [_arrivalLocationTextField setRightView:_destinationFavoritesButton];
    [_arrivalLocationTextField setRightViewMode:UITextFieldViewModeAlways];
    
#endif

    _arrivalPlace = nil;
    _departurePlace = nil;
    _shouldSaveArrivalLocation = NO;
    _shouldSaveDepartureLocation = NO;
    
}




-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self checkForFavorites];
    
    //add the datapicker view, but place it off the bottom of the view, so it will not be seen
    //needs to be here so that the view is already calculated according to the otherviews it sits in
    CGRect pickerFrame =  _datePickerAccessoryView.frame;
    pickerFrame.origin.x = 0.0;
    pickerFrame.origin.y = self.view.bounds.size.height ;//- pickerFrame.size.height;
    
    _datePickerAccessoryView.frame = pickerFrame;
    [self.view addSubview:_datePickerAccessoryView];
    
    if(!_dateHasBeenUserChanged){ //if the user has selected a custom date, we do not replace it each time the view appears
        _departureArrivalDate = [NSDate date];
    }
    
    [_arrivalDepartureSegmentedControl setTintColor:[ApplicationUtilities defaultApplicationHighlightColor]];
    [_dateChangeButton setTitle:[_standardDateFormatter stringFromDate:_departureArrivalDate] forState:UIControlStateNormal];
    _lastUserLocation = nil; // wipe this out so we don't use it the next time by accident
    
//#ifdef COMPILED_AS_LITE_VERSION
  //  //if it is the free version, put in the add view
    //[[GADMasterViewController sharedGADMasterViewController] resetAdView:self];
//#endif
}



- (IBAction)userDidSelectDepartureFromFavorites:(id)sender{
    UINavigationController *tempNavController = [[UINavigationController alloc] init];
    [[tempNavController navigationBar] setTranslucent:NO];
    
    SelectRecentPlaceViewController *sfpvc = [[SelectRecentPlaceViewController alloc] init];
    [tempNavController setViewControllers:[NSArray arrayWithObject:sfpvc]];
    [sfpvc setCallbackString:@"departure"];
    [sfpvc setTitleString:NSLocalizedString(@"Departure", @"Departure String")];
    [sfpvc setDelegate:self];
    [self presentViewController:tempNavController animated:YES completion:nil];
}
- (IBAction)userDidSelectDestinationFromFavorites:(id)sender{
    UINavigationController *tempNavController = [[UINavigationController alloc] init];
   
        [[tempNavController navigationBar] setTranslucent:NO];
    

    SelectRecentPlaceViewController *sfpvc = [[SelectRecentPlaceViewController alloc] init];
    [tempNavController setViewControllers:[NSArray arrayWithObject:sfpvc]];
    [sfpvc setCallbackString:@"destination"];
    [sfpvc setTitleString:NSLocalizedString(@"Destination", @"Destination TextField Placeholder String")];
    [sfpvc setDelegate:self];
    [self presentViewController:tempNavController animated:YES completion:nil];
}

-(void) userDidSelectPlace:(GeoPlace *) place withCallbackString:(NSString *) callbackStr{
    NSLog(@"DELEGATE CALLED");
    if (place == nil){
        return; //the user just cancelled
    }
    
    if([callbackStr isEqual:@"departure"]){
        _departurePlace = place;
        [_departureLocationTextField setText:[_departurePlace name]];
    }else{
        _arrivalPlace = place;
        [_arrivalLocationTextField setText:[_arrivalPlace name]];
    }
        _shouldSaveJourney = YES; //We assume this is a new journey;
}

-(void) checkForFavorites{
  //  if([[[FavoritePlacesStore sharedFavoritePlacesStore] favoritePlaces] count] != 0){
   ///     [_departureFavoritesButton setEnabled:YES];
      //  [_destinationFavoritesButton setEnabled:YES];
       // [_departureFavoritesButton setAlpha:1.0f];
       // [_destinationFavoritesButton setAlpha:1.0f];
    //}else{
      //  [_departureFavoritesButton setEnabled:NO];
       // [_destinationFavoritesButton setEnabled:NO];
       // [_departureFavoritesButton setAlpha:0.0f];
       // [_destinationFavoritesButton setAlpha:0.0f];
   // }
}

- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    //we only use this method to know that the text has actually changed and thus we should save the journey
    _shouldSaveJourney = YES;
    return YES;
}

-(void) chooseRecentJourney: (id) sender{
    RecentJourneysViewController * rjvc = [[RecentJourneysViewController alloc] init];

    UINavigationController *tempNavController = [[UINavigationController alloc] init];
           [[tempNavController navigationBar] setTranslucent:NO];
    

    [tempNavController setViewControllers:[NSArray arrayWithObject:rjvc]];
       [rjvc setDelegate:self];
    [self presentViewController:tempNavController animated:YES completion:nil];
}

-(void) userDidCancelSelection{
    NSLog(@"Canceled");
}
-(void) userDidSelectRecentJourney:(RecentJourney *) journey{
    
    _shouldSaveJourney = NO;
    
    //change the journey last use date, so it moves up the line for deletion
    [journey setLastSearchDate:[NSDate date]];
    
    _departurePlace = [[GeoPlace alloc] init];
    _arrivalPlace = [[GeoPlace alloc] init];
    //setup UI to fit with the journey received. we need to clone copy this incase anything changes
    _departurePlace.name = [[journey departurePlace] name];
    _departurePlace.coordinate = [[journey departurePlace] coordinate];
    _arrivalPlace.name = [[journey arrivalPlace] name];
    _arrivalPlace.coordinate = [[journey arrivalPlace] coordinate];
   
    [_departureLocationTextField setText:[_departurePlace name]];
    [_arrivalLocationTextField setText:[_arrivalPlace name]];
    [_arrivalDepartureSegmentedControl setSelectedSegmentIndex:[journey arrivalDeparture]];
    _departureArrivalDate = [journey transitDate];
    _dateHasBeenUserChanged = YES;
}

-(void) presentDatePicker{
    
    [_datePicker setDate:_departureArrivalDate];
    [_datePicker setMinimumDate:[NSDate date]];
    
    CGRect pickerFrame = _datePickerAccessoryView.frame;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    
    pickerFrame.origin.y -= pickerFrame.size.height; //slide from off screen to onscreen
    _datePickerAccessoryView.frame = pickerFrame;
    
    [UIView commitAnimations];
}

-(void) dismissDatePicker{
    CGRect pickerFrame = _datePickerAccessoryView.frame;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    
    pickerFrame.origin.y += pickerFrame.size.height; //slide to offscreen
    _datePickerAccessoryView.frame = pickerFrame;
    
    [UIView commitAnimations];
    
    _departureArrivalDate = [_datePicker date];
    _dateHasBeenUserChanged = YES;
    
    [_dateChangeButton setTitle:[_standardDateFormatter stringFromDate:_departureArrivalDate] forState:UIControlStateNormal];
}


- (IBAction)userDidChangeDate:(id)sender {
    [self dismissDatePicker];
    _shouldSaveJourney = YES;
}

- (IBAction)userDidCancelChangeDate:(id)sender {
    [self dismissDatePicker];
}

- (IBAction)userRequestedDateChange:(id)sender {
    _departureArrivalDate = [_datePicker date];
  [_dateChangeButton setTitle:[_standardDateFormatter stringFromDate:_departureArrivalDate] forState:UIControlStateNormal];
    _dateHasBeenUserChanged = YES;
    [self presentDatePicker];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}



-(void) determineLocationFromName:(GeoPlace *) place isArrivalPlace:(BOOL) isArrival{
    
        
        MKLocalSearchRequest *request = [[MKLocalSearchRequest alloc] init];
        request.naturalLanguageQuery = place.name;
        request.region = MKCoordinateRegionMake(CLLocationCoordinate2DMake(60.333275, 24.992381), MKCoordinateSpanMake(3.0, 3.0)); //center above klaukkala with a span of 3 degrees seems about right
    
        MKLocalSearch * localSearch = [[MKLocalSearch alloc] initWithRequest:request];
        
        [localSearch startWithCompletionHandler:^(MKLocalSearchResponse *response, NSError *error){
            
            //We can't distinguisg between a service error and just not getting results. So we don't explicitly deal with errors at the moment
         /*
          NSLog(@"Search Error Occured %@", error);
            if (error != nil) { //we will need to revisit this
                [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Service Error",nil)
                                            message:[error localizedDescription]
                                           delegate:nil
                                  cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil] show];
                  [_progressHUD hide:NO];
                return;
             
            }
           */ 
            if([response.mapItems count] == 0){
                UIAlertView *alert;
                if(isArrival){
                    //we couldn't find this so pop up an alert view
                    alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Destination Not Found", @"Destination Not Found Alert Title") message:[NSString stringWithFormat:@"%@ %@ %@", NSLocalizedString(@"The destination", @"Destination Not Found Alert Message Pt 1"), place.name, NSLocalizedString(@"could not be found. ", @"Destination Not Found Alert Message Pt 2")] delegate:nil cancelButtonTitle:NSLocalizedString(@"Dismiss", @"Dismiss Button Title") otherButtonTitles: nil];
                }else{
                    //is departure
                    alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Departure Not Found", @"Departure Not Found Alert Title") message:[NSString stringWithFormat:@"%@ %@ %@", NSLocalizedString(@"The departure", @"Departure Not Found Alert Message Pt 1"), place.name, NSLocalizedString(@"could not be found.", @"Departure Not Found Alert Message Pt 2")] delegate:nil cancelButtonTitle:NSLocalizedString(@"Dismiss", @"Dismiss Button Title") otherButtonTitles: nil];
                }
                [alert show];
                [_progressHUD hide:NO];
                return; //exit this blockdeter
            }
            
             NSArray * results = response.mapItems;            
            //dump these to console
          //  NSLog(@"DUMP OF SEARCH RESULTS. There were %d results", [results count]);
            //for(MKMapItem *mi in results){
              //  NSLog(@"Name: %@ Lat %f Lon %f\n,", mi.name, mi.placemark.coordinate.latitude, mi.placemark.coordinate.longitude);
            //}
            

            //present a view controller to allow selection from multiple results
            PresentSearchResultsViewController *psrvc = [[PresentSearchResultsViewController alloc] init];
            UINavigationController *tempNavController = [[UINavigationController alloc] init];
            [tempNavController setViewControllers:[NSArray arrayWithObject:psrvc]];
           
                [[tempNavController navigationBar] setTranslucent:NO];
            


            if(isArrival){
                [psrvc setLocationType:kArrival];
            }else{
                [psrvc setLocationType:kDeparture];
            }
            [psrvc setResults:results];
            [psrvc setDelegate:self];
            [self presentViewController:tempNavController animated:YES completion:nil];
     }]; //end of block
}

-(void) userSelectedPlace:(GeoPlace *) place forLocationType:(ArrivalDepartureType) type{ //the delegate for the multiple results view controller, we still need to copy the data to arrival or destination
    
    if(type == kArrival){
        _arrivalPlace = place;
        [_arrivalLocationTextField setText:[_arrivalPlace name]];
    }else{
        _departurePlace = place;
        [_departureLocationTextField setText:[_departurePlace name]];
    }
    //NSLog(@"SELECTED VALUE WAS %@, %f %f", place.name, place.coordinate.latitude, place.coordinate.longitude);
 //   [_progressHUD hide:YES];

[self userRequestedSearch:nil];
    
}

- (IBAction)userRequestedSearch:(id)sender {
    
    [_progressHUD setLabelText:NSLocalizedString(@"Finding Arrival and Destination", @"Finding Arrival and Destindation String")];
    [_progressHUD show:YES];
    
    //Determine if we have 2 valid places yet.
    //The arrivalPlace is invalid if it is nil, or the textin the text field doesn't match the text already there
    //if it is invalid, call the geocoder and then return. The geocode method will recall this method if sucessful
    NSLog(@"BIG IF START");
       
    if([[_departureLocationTextField text] length] == 0){ //we will use current location
            if(_lastUserLocation == nil){
                NSLog(@"departure is based on location, searching for it");
                [_locationServer startUpdatingLocation];
                [_progressHUD setLabelText:NSLocalizedString(@"Searching for Location", @"Searching for Location Message")];
                [_progressHUD show:YES];
                return;
            }else{ //we have the current location, so can proceed
                NSLog(@"departure is based on current location, we are proceeding");
                _departurePlace = [[GeoPlace alloc] init];
                _departurePlace.coordinate = _lastUserLocation.coordinate;
                 _shouldSaveDepartureLocation = NO; //we don't save current location as a place
                _departurePlace.name = @"Current Location";
            }
    }else{//we are not using current location
        if(_departurePlace != nil){
            if([_departurePlace.name compare:[_departureLocationTextField text]] == NSOrderedSame){
                NSLog(@"Have Good Departure Location");
            }else{
                [_departurePlace setName:[_departureLocationTextField text]];
                NSLog(@"Searching for Departure String GEOCODE");
                NSLog(@"SEARCHING FOR DEPARTURE STRING bb%@bb", _departurePlace.name);
                [self determineLocationFromName:_departurePlace isArrivalPlace:NO];
                _shouldSaveDepartureLocation = YES;
                return;
            }
        }else{ // it is nil
            NSLog(@"HAVE NIL DEPARTURE PLACE SEARCHING FOR IT");
            _departurePlace = [[GeoPlace alloc] init];
            _shouldSaveDepartureLocation = YES;
            _departurePlace.name = [_departureLocationTextField text];
            
            [self determineLocationFromName:_departurePlace isArrivalPlace:NO];
            return;
        }
    }
    
    
    //End of determining departure location
    
    //search for arrival
    if(_arrivalPlace != nil){
        if([_arrivalPlace.name compare:[_arrivalLocationTextField text]] == NSOrderedSame){
            NSLog(@"Have Good Arrival Location");
        }else{
            [_arrivalPlace setName:[_arrivalLocationTextField text]];
            NSLog(@"Searching for Arrival String GEOCODE");
            [self determineLocationFromName:_arrivalPlace isArrivalPlace:YES];
            _shouldSaveArrivalLocation = YES;
            return;
        }
    }else{ // it is nil
        _shouldSaveArrivalLocation = YES;
        _arrivalPlace = [[GeoPlace alloc] init];
        _arrivalPlace.name = [_arrivalLocationTextField text];
        [self determineLocationFromName:_arrivalPlace isArrivalPlace:YES];
        return;
    }
    
    
    NSLog(@"HAVE DEPARTURE AND ARRIVAL LOCATION AND COORDINATES");
    NSLog(@"***************************************************");
    NSLog(@"Departure Location Name: %@ at Lat %f Lon: %f", _departurePlace.name, _departurePlace.coordinate.latitude, _departurePlace.coordinate.longitude);
    NSLog(@"Arrival Location Name: %@ at Lat %f Lon: %f", _arrivalPlace.name, _arrivalPlace.coordinate.latitude, _arrivalPlace.coordinate.longitude);
    NSLog(@"***************************************************");
    //ASSERT: We should have two valid, and fully filled GeoPoints here.
   

    if(_shouldSaveDepartureLocation){
        _shouldSaveDepartureLocation = NO;
        RecentGeoPlace *recentPlace  = [[RecentGeoPlace alloc] initWithGeoPlace:_departurePlace andDate:[NSDate date]];
        [[RecentGeoPlaceStore sharedRecentGeoPlaceStore] addRecentGeoPlace:recentPlace];
    }
    
    if(_shouldSaveArrivalLocation){
        _shouldSaveArrivalLocation = NO;
        RecentGeoPlace *recentPlace  = [[RecentGeoPlace alloc] initWithGeoPlace:_arrivalPlace andDate:[NSDate date]];
        [[RecentGeoPlaceStore sharedRecentGeoPlaceStore] addRecentGeoPlace:recentPlace];
    }
    
   
    NSLog(@"SEARCH WAS CARRIED OUT \n\n\n]");
   // [_progressHUD hide:YES];
    // return; // REMOVE THIS LATER FOR DEBUG ONLY
    [self searchForJourneysFromDeparture:_departurePlace.coordinate toDestination:_arrivalPlace.coordinate];
    
}

    


-(void) searchForJourneysFromDeparture:(CLLocationCoordinate2D) departureLocation toDestination:(CLLocationCoordinate2D) destinationLocation{
    NSLog(@"Search for journeys called");
    
   [_progressHUD setLabelText:NSLocalizedString(@"Searching for Journey", @"Searching for Journey Message")];
    [_progressHUD show:YES];
    
    ReittiopasQueryProcessor * queryProcessor = [[ReittiopasQueryProcessor alloc] init];
    NSString *arrivalDepartureString = @"departure";
    if([_arrivalDepartureSegmentedControl selectedSegmentIndex] ==1){
        arrivalDepartureString = @"arrival";
    }
    [queryProcessor findJourneyPlanFrom:departureLocation toLocation:destinationLocation withTime:_departureArrivalDate arrivalDeparture:arrivalDepartureString withCompletionBlock:^(NSArray * arr, ReturnCodeType retCode){
        [_progressHUD hide:YES];
        
        if(retCode == kNetworkError){
            [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Error", @"Network Error Alert Title") message:NSLocalizedString(@"Your search could not be completed. Check your network connection and try again", @"Network Error Alert Message Search Could Not Be Completed") delegate:nil cancelButtonTitle:NSLocalizedString(@"Dismiss", @"Dismiss Button Title") otherButtonTitles: nil] show];
            return;
        }else if (retCode == kNoResults){
            [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"No Results Found",@"No Results Found Alert Title") message:NSLocalizedString(@"No results could be found for your search",@"No Results Found Alert Message") delegate:nil cancelButtonTitle:NSLocalizedString(@"Dismiss", @"Dismiss Button Title") otherButtonTitles: nil] show];
            return;
        }

        for(JourneyRoute *route in arr){
            NSLog(@"%@", [route stringRepresentation]);
            JourneyLeg *tempLeg = [[route journeyLegs] objectAtIndex:0];
            
            if([[_departureLocationTextField text] length] == 0){
                tempLeg.departureName = @"Current Location";
                NSLog(@"DEPARTURE IS NIL");
            }else{
                NSLog(@"DEPARTURE IS NOT NIL *%@*", [_departureLocationTextField text]);
                tempLeg.departureName =[_departureLocationTextField text];
            }
            tempLeg = [[route journeyLegs] objectAtIndex:[[route journeyLegs] count]-1 ];
            tempLeg.arrivalName = [_arrivalLocationTextField text];
        }
        
        
        //save the search to recents, if we should
        if(_shouldSaveJourney){
            RecentJourney *tempRecentJourney = [[RecentJourney alloc] initWithDeparturePlace:[_departurePlace copy] toArrivalPlace:[_arrivalPlace copy] onDate:_departureArrivalDate asArrivalOrDeparture:[_arrivalDepartureSegmentedControl selectedSegmentIndex] withlastSearchDate:[NSDate date]];
            [[RecentJourneyStore sharedRecentJourneyStore] addJourney:tempRecentJourney];
            _shouldSaveJourney = NO;// since we just saved it, we don't want to resave if the user just researches
            
        }
        
        //push a new view controller
        JourneySearchResultsViewController * journeySearchResults = [[JourneySearchResultsViewController alloc] init];
        [journeySearchResults setJourneys:arr];
        [self.navigationController pushViewController:journeySearchResults animated:YES];
    }];
}



-(void) didUpdateLocation:(CLLocation *) location{
    _lastUserLocation = location;
    [_locationServer stopUpdatingLocation];
    [self userRequestedSearch:nil]; //recall the search
}


-(void) locationUpdateDidFail{
    NSLog(@"NEARBY STOPS LOCATION UPDATE FAILED");
    [_locationServer stopUpdatingLocation];
    [_progressHUD hide:NO];
    if(_currentAuthorisationStatus == kCLAuthorizationStatusDenied || _currentAuthorisationStatus == kCLAuthorizationStatusRestricted){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Location Services are Turned Off", @"Location Services Are Off Alert Title") message: NSLocalizedString(@"You cannot use Current Location as your departure when Location Services are turned off.", @"Location Services are Turned Off Message") delegate:nil cancelButtonTitle:NSLocalizedString(@"Dismiss", @"Dismiss Button Title") otherButtonTitles: nil];
        [alert show];
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Location Error", @"Location Error Alert Title") message:NSLocalizedString(@"Your location could not be determined. Please try again later", @"Location Error Alert Message") delegate:nil cancelButtonTitle:NSLocalizedString(@"Dismiss", @"Dismiss Button Title") otherButtonTitles: nil];
        [alert show];
    }
}

-(void) locationServicesAuthorisationStatusDidChange:(CLAuthorizationStatus) status{
    _currentAuthorisationStatus = status;
    NSLog(@"NEARBY STOPS AUTHORISATION STATUS CHANGED");
}


-(void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    _lastUserLocation = nil; // wipe this out so we don't use it the next time by accident
}

-(void)viewDidUnload {
    [self setDatePicker:nil];
    [self setDatePickerAccessoryView:nil];
    [self setDepartureLocationTextField:nil];
    [self setArrivalLocationTextField:nil];
    [self setArrivalDepartureSegmentedControl:nil];
    //[self setDateTimeDisplayLabel:nil];
    [self setSearchButton:nil];
    [self setLeavingFromLabel:nil];
    [self setArrivingAtLabel:nil];
    [self setSetDateCancelButton:nil];
    [self setSetDateButton:nil];
    [self setDepartureFavoritesButton:nil];
    [self setDestinationFavoritesButton:nil];
    
    [super viewDidUnload];
}

-(void) applicationWillResign{ //if we are searching for location whilst the app goes background. Bad things will happen
    
    if([_locationServer isActive]){
        NSLog(@"APPLICATION WILL RESIGN");
        [_progressHUD hide:NO];
        [_locationServer stopUpdatingLocation];
    }
}


@end






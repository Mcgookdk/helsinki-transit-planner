//
//  TransitModelUtilities.h
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 30/12/2012.
//  Copyright (c) 2012 David McGookin. All rights reserved.
//
//Deals with the look and feel of the transit types in the application

#import <Foundation/Foundation.h>


typedef enum transit_type_enum { kMetro, kBus, kTram, kTrain, kFerry, kWalk, kPOI, kUnknownTransitType} TransitType;

@interface TransitModelUtilities : NSObject{
    NSArray *_largeIconImages;
    NSArray *_smallestIconImages;
    NSArray *_smallIconImages;
    NSArray *_microIconImages;
    NSArray *_transitColors;
    NSArray *_transitTypeNames;
    
    
}

+(id) sharedTransitModelUtilities;



-(UIImage *) largeIconForTransitType:(TransitType) type;

-(UIImage *) smallestIconForTransitType:(TransitType) type;

-(UIImage *) smallIconForTransitType:(TransitType) type;

-(UIImage *) microIconForTransitType:(TransitType) type;

-(UIColor *) colorForTransitType:(TransitType) type;

-(NSString *) readableNameForTransitType:(TransitType) type;
@end

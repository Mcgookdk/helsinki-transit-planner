//
//  HSLDisruption.h
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 04/02/2013.
//  Copyright (c) 2013 David McGookin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HSLInfo.h"
@interface HSLDisruption : NSObject

@property (readwrite, nonatomic) int id;
@property (strong, nonatomic) NSString * text;
@property (strong, nonatomic) HSLInfo * information;

-(NSString *) stringRepresentation;

@end

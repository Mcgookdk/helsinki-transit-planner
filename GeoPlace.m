//
//  GeoPlace.m
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 21/03/2013.
//  Copyright (c) 2013 David McGookin. All rights reserved.
//


#import "GeoPlace.h"

@implementation GeoPlace

-(id) init{
    self = [super init];
    self.name = @"";
    self.address = @"";
    return self;
}

-(id) copyWithZone: (NSZone *) zone{
    GeoPlace *copy = [[GeoPlace allocWithZone: zone] init];
    
    [copy setName:[NSString stringWithString:_name]];
    [copy setAddress:[NSString stringWithString:_address]];
    [copy setCoordinate:_coordinate];
    
    return copy;
}

@end

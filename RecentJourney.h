//
//  RecentJourney.h
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 21/03/2013.
//  Copyright (c) 2013 David McGookin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "GeoPlace.h"
typedef enum arrivalDepartureEnum {kDeparture, kArrival} ArrivalDepartureType;

@interface RecentJourney : NSObject

@property (strong, nonatomic) NSDate * lastSearchDate; // the date this was last used, only used to manage culling of journeys
@property (strong, nonatomic) GeoPlace *departurePlace;
@property (strong, nonatomic) GeoPlace * arrivalPlace;
@property (strong, nonatomic) NSDate *transitDate;
@property (readwrite, nonatomic) ArrivalDepartureType arrivalDeparture;


-(id) initWithDeparturePlace:(GeoPlace *) departurePlace toArrivalPlace:(GeoPlace *) arrivalPlace onDate:(NSDate *) date asArrivalOrDeparture:(ArrivalDepartureType) arrDep withlastSearchDate:(NSDate *) sDate;

-(id) initWithDictionary:(NSDictionary *) dictionary;


-(NSDictionary *) dictionaryRepresentationOfJourney;

@end

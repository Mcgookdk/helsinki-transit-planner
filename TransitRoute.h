//
//  TransitRoute.h
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 28/10/2012.
//  Copyright (c) 2012 David McGookin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TransitRoute : NSObject

@property (strong, nonatomic) NSArray *route; // an array of CLLocations that represent the polyline of the route
@property (strong, nonatomic) NSArray *stops; //an array of Transit Stops on the route.

@end

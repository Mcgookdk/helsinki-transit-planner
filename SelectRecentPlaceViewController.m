//
//  SelectFavoritePlaceViewController.m
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 14/03/2013.
//  Copyright (c) 2013 David McGookin. All rights reserved.
//

#import "SelectRecentPlaceViewController.h"
#import "ApplicationUtilities.h"
#import "RecentGeoPlaceStore.h"

@interface SelectRecentPlaceViewController ()

@end

@implementation SelectRecentPlaceViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [[self navigationItem] setTitle:_titleString];
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel", @"Cancel Button Text") style:UIBarButtonItemStylePlain target:self action:@selector(userDidCancelSelection:)];
    [[self navigationItem] setRightBarButtonItem:cancelButton];
    
    [_tableView setDelegate:self];
    [_tableView setDataSource:self];
}


- (IBAction)userDidCancelSelection:(id)sender {
    if ([_delegate respondsToSelector:@selector(userDidSelectFavorite:)]) {
        [_delegate userDidSelectPlace:nil withCallbackString:_callbackString];
    }
     [self dismissViewControllerAnimated:YES completion: nil];

}




-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if([indexPath indexAtPosition:0] == 0){ //the cell is a favorite
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FavoriteCell"];
    if(cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"FavoriteCell"];
        [[cell textLabel] setFont:[ApplicationUtilities defaultApplicationFontForTableCell]];
        [cell setBackgroundColor:[ApplicationUtilities defaultImageColorForTableViewTextureBackround]];
    }
    
    
       FavoritePlace *currentPlace =   [[[FavoritePlacesStore sharedFavoritePlacesStore] favoritePlaces] objectAtIndex:[indexPath indexAtPosition:1]] ;
    [[cell textLabel] setText:[currentPlace title]];
    [[cell imageView] setImage:[[TransitModelUtilities sharedTransitModelUtilities] largeIconForTransitType: [currentPlace placeType]]];
        return cell;
    }else{ //it is a recent place so we use a subtitled cell
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RecentPlaceCell"];
        if(cell == nil){
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"RecentPlaceCell"];
            [[cell textLabel] setFont:[ApplicationUtilities defaultApplicationFontForTableCell]];
            [cell setBackgroundColor:[ApplicationUtilities defaultImageColorForTableViewTextureBackround]];
        }
    
        RecentGeoPlace *place = [[[RecentGeoPlaceStore sharedRecentGeoPlaceStore] recentGeoPlaces] objectAtIndex:[indexPath indexAtPosition:1]];
        [[cell textLabel] setText:[place name]];
        [[cell detailTextLabel] setText:[place address]]; //need to fix the cell
        return cell;
    }
    
    return nil;
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;

}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section ==0){
    return [[[FavoritePlacesStore sharedFavoritePlacesStore] favoritePlaces] count];
    }else{
        return [[[RecentGeoPlaceStore sharedRecentGeoPlaceStore] recentGeoPlaces] count];
    }
}

-(void) tableView:(UITableView *) tableView didSelectRowAtIndexPath: (NSIndexPath *) indexPath{
        NSLog(@"PLACE SELECTED");
    
    GeoPlace *selectedPlace = [[GeoPlace alloc] init];
    
    if([indexPath indexAtPosition:0] == 0){ //the cell is a favorite
        FavoritePlace * place = [[[FavoritePlacesStore sharedFavoritePlacesStore] favoritePlaces] objectAtIndex:[indexPath indexAtPosition:1]];
        selectedPlace.name = place.title;
        selectedPlace.coordinate = place.coordinate;
        
    }else{
        RecentGeoPlace * place = [[[RecentGeoPlaceStore sharedRecentGeoPlaceStore] recentGeoPlaces] objectAtIndex:[indexPath indexAtPosition:1]];
        selectedPlace.name = place.name;
        selectedPlace.coordinate = place.coordinate;
        selectedPlace.address = place.address;
        
    }
    
    if ([_delegate respondsToSelector:@selector(userDidSelectPlace:withCallbackString:)]) {
      
        [_delegate userDidSelectPlace:selectedPlace withCallbackString:_callbackString];
    }
        [self dismissViewControllerAnimated:YES completion: nil];    
}

-(NSString *) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if(section ==0){
        return @"Favorites";
    }else{
        return @"Recent Places";
    }
}
- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];
    [headerView setAlpha:0.8f];
    UILabel *sectionTitle = [[UILabel alloc] initWithFrame:CGRectMake (10,0,200,30)];
    //[sectionTitle setFont:[UIFont fontWithName:@"HelveticaNeue" size:12]];
    [headerView addSubview:sectionTitle];
    [headerView setBackgroundColor:[ApplicationUtilities defaultApplicationHighlightColor]];
    [sectionTitle setBackgroundColor:[UIColor clearColor]];
    [sectionTitle setTextColor:[UIColor whiteColor]];
    
    [sectionTitle setText:[self tableView:nil titleForHeaderInSection:section]];
    
    return headerView;
}

-(CGFloat) tableView: (UITableView *) tableView heightForHeaderInSection:(NSInteger)section {
return 30.0;
}
- (void)viewDidUnload {
    [self setTableView:nil];
    [super viewDidUnload];
}

@end

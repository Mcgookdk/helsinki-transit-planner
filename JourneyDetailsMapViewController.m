//
//  JourneyDetailsMapViewController.m
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 13/02/2013.
//  Copyright (c) 2013 David McGookin. All rights reserved.
//

#import "JourneyDetailsMapViewController.h"
#import "TransitModelUtilities.h"


//this came from an eyeball inspection. Seems to be no better way to get it
//Used to define the point when we add or remove intermediate stops from the Map
#define LATITUDE_DELTA_BOUNDARY 0.033561

@interface JourneyDetailsMapViewController ()
-(JourneyDetailsMapAnnotation *) generateMapAnnotationFromJourneyLeg:(JourneyLeg *) journeyLeg isArrivalPoint:(BOOL) isArrival;
-(JourneyDetailsMapAnnotation *) generateIntermediateMapAnnotationFromJourneyLegStop:(JourneyLegStop *) journeyLegStop;
-(UIImage *) generateSizedThumbnailFromImage:(UIImage *) img;
-(MKCoordinateRegion) coordinateRegionThatSpansJourney; //utility function to pan the map correctly
@end

@implementation JourneyDetailsMapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _polylines = [[NSMutableArray alloc] init];
    }
    return self;
}


-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    //Zoom to the start location of the route
    MKMapRect visibleRect;// = [map mapRectThatFits:overlay.boundingMapRect];
    visibleRect.size.width = 4500;//
    visibleRect.size.height = 4500;//we have this as a third to get a bit closer
    JourneyLegStop *jls = [[[_journeyToDisplay journeyLegs] objectAtIndex:0] departureJourneyLegStop];
    visibleRect.origin = MKMapPointForCoordinate([jls location]);
    [_mapView setVisibleMapRect:visibleRect];
    [_mapView setCenterCoordinate:[jls location]];

}

- (void)viewDidLoad{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [_mapView setDelegate:self];
 
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.automaticallyAdjustsScrollViewInsets = NO;
        //take out the iOS6 app icons
        [_mapTypeSelectionControl removeFromSuperview];
        [_userHeadingBtn removeFromSuperview];
        
        
        _userHeadingBarButton = [[MKUserTrackingBarButtonItem alloc] initWithMapView:_mapView];
        NSMutableArray *items =[[NSMutableArray alloc] initWithArray:[_toolbar items]];
        [items removeObjectAtIndex:0];
        
        [items insertObject:_userHeadingBarButton atIndex:0];
        [_toolbar setItems:items animated:YES];
        
 
    
    UIImage *buttonArrow = [UIImage imageNamed:@"LocationGrey.png"];
    [_userHeadingBtn setImage:buttonArrow forState:UIControlStateNormal];
    
    for (JourneyLeg *j in [_journeyToDisplay journeyLegs]){
        [self addJourneyLegRouteToMap:j];
    }
    
    //add the major markers stops
    for (JourneyLeg *j  in [_journeyToDisplay journeyLegs]){
        [_mapView addAnnotation:[self generateMapAnnotationFromJourneyLeg:j isArrivalPoint:NO]];
    }
    JourneyLeg *destination = [[_journeyToDisplay journeyLegs] objectAtIndex:[[_journeyToDisplay journeyLegs] count] -1];
    [_mapView addAnnotation:[self generateMapAnnotationFromJourneyLeg:destination isArrivalPoint:YES]];
    
    
    //if the leg is not walking, create intermediate stops, but don't add them to the map yet
    _intermediateStopAnnotations = [[NSMutableArray alloc] init];
    _intermediateStopsAreOnMap = NO;
    for (JourneyLeg *j  in [_journeyToDisplay journeyLegs]){
        if ([j transitType] != kWalk){
            for(JourneyLegStop *jls in [j stopsOnLeg]){
                [_intermediateStopAnnotations addObject:[self generateIntermediateMapAnnotationFromJourneyLegStop:jls]]; //we don't add these to the map until the user zooms in
            }
        }
    }
    
    [_mapView showsUserLocation];
    
    }

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated{
    
    NSLog(@"Lat Delta %f Lon Delat %f", [_mapView region].span.latitudeDelta, [_mapView region].span.longitudeDelta);
    MKCoordinateSpan coordSpan = [mapView region].span;
    if(coordSpan.latitudeDelta != _visibleCoordinateSpan.latitudeDelta || coordSpan.longitudeDelta != _visibleCoordinateSpan.longitudeDelta){
        //then we need to consider to act
        if(coordSpan.latitudeDelta <= LATITUDE_DELTA_BOUNDARY && !_intermediateStopsAreOnMap){
            //add the minor annotations
            [_mapView addAnnotations:_intermediateStopAnnotations];
            _intermediateStopsAreOnMap = YES;
        }else if (coordSpan.latitudeDelta > LATITUDE_DELTA_BOUNDARY && _intermediateStopsAreOnMap){ //remove them
            [_mapView removeAnnotations:_intermediateStopAnnotations];
            _intermediateStopsAreOnMap = NO;
            
        }
    }
    
}

-(JourneyDetailsMapAnnotation *) generateIntermediateMapAnnotationFromJourneyLegStop:(JourneyLegStop *) journeyLegStop{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"HH:mm"];
    
    NSString *  subtitle = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"Departs", @"Departs String"),[dateFormat stringFromDate:[journeyLegStop departureDate]]];
    
    
    JourneyDetailsMapAnnotation *jdma =  [[JourneyDetailsMapAnnotation alloc] initWithCoordinate:[journeyLegStop location] title:[journeyLegStop name] subTitle: subtitle code: [journeyLegStop stopCode] andTransitType:[journeyLegStop transitType]];
    
    [jdma setAnnotationType:kMinorMarker];
    return jdma;
}

-(JourneyDetailsMapAnnotation *) generateMapAnnotationFromJourneyLeg:(JourneyLeg *) journeyLeg isArrivalPoint:(BOOL) isArrival{
    
    NSString * title;
    NSString *subtitle;
    CLLocationCoordinate2D coord;
    NSString *stopCode;
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"HH:mm"];
    TransitType type = [journeyLeg transitType];
    if(!isArrival){ //its a departure
        coord = [[[journeyLeg polyline] objectAtIndex:0] coordinate];
        stopCode = [journeyLeg arrivalStopCode];
        if([journeyLeg departureName] != nil){
            title = [journeyLeg departureName];
        }else{
            title = NSLocalizedString(@"Departure", @"Departure String");
        }
        if(type == kWalk){
            subtitle = [NSString stringWithFormat:@"%@, %@ %@", NSLocalizedString(@"Walk", @"Walk String"), NSLocalizedString(@"Depart at ", @"Depart at String"), [dateFormat stringFromDate:[journeyLeg departureDate]]];
        }else{
            subtitle = [NSString stringWithFormat:@"%@ %@, %@ %@", NSLocalizedString(@"Service", @"Service String"), [[journeyLeg service] userReadableCode], NSLocalizedString(@"Departs", @"Departs String"), [dateFormat stringFromDate:[journeyLeg departureDate]]];
        }
        
    }else{ //is an arrival point
        coord = [[[journeyLeg polyline] objectAtIndex:[[journeyLeg polyline] count] -1] coordinate];
        stopCode = [journeyLeg departureStopCode];
        if([journeyLeg arrivalName] != nil){
            title = [journeyLeg arrivalName];
        }else{
            title = @"Arrival";
        }
        if(type == kWalk){
            subtitle = [NSString stringWithFormat:@"%@, %@ %@", NSLocalizedString(@"Walk", @"Walk String"), NSLocalizedString(@"Arrive at", @"Arrive at String"), [dateFormat stringFromDate:[journeyLeg arrivalDate]]];
        }else{
            subtitle = [NSString stringWithFormat:@"%@ %@, %@ %@",NSLocalizedString(@"Service", @"Service String"), [[journeyLeg service] userReadableCode], NSLocalizedString(@"Arrives", @"Arrives String"), [dateFormat stringFromDate:[journeyLeg arrivalDate]]];
        }
        
    }
    
    JourneyDetailsMapAnnotation *jdma = [[JourneyDetailsMapAnnotation alloc] initWithCoordinate:coord title:title subTitle:subtitle code:stopCode andTransitType:type];
    if(isArrival){
        [jdma setAnnotationType:kMajorAndDestinationMarker];
    }
    return jdma;
}

-(void) addJourneyLegRouteToMap:(JourneyLeg *) leg{
	MKMapPoint * mapPointsArr = malloc(sizeof(MKMapPoint) * [[leg polyline] count]);
	int i = 0;
	for(CLLocation * loc in [leg polyline]){
        MKMapPoint  mapPoint = MKMapPointForCoordinate(loc.coordinate);
        mapPointsArr[i] = mapPoint;
        i++;
	}
	
	MKPolyline *routePolyline = [MKPolyline polylineWithPoints:mapPointsArr count:[[leg polyline]count]];
    [_polylines addObject:routePolyline]; //need to store this for later
	[_mapView addOverlay:routePolyline];
    free(mapPointsArr); //clean this up, its a lot of memory
}

- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id <MKOverlay>)overlay{
    int i = [_polylines indexOfObject:overlay];
    
    JourneyLeg *currentLeg = [[_journeyToDisplay journeyLegs] objectAtIndex:i];
    MKPolylineView *polylineView;
    polylineView = [[MKPolylineView	 alloc] initWithPolyline: overlay];
    [polylineView setFillColor:[[TransitModelUtilities sharedTransitModelUtilities] colorForTransitType:[currentLeg transitType]]];
    [polylineView setStrokeColor:[[TransitModelUtilities sharedTransitModelUtilities] colorForTransitType:[currentLeg transitType]]];
    [polylineView setLineWidth:5.0f];
    return polylineView;
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation{
	MKPinAnnotationView *pinView = nil;
	
	if([annotation isMemberOfClass:[JourneyDetailsMapAnnotation class]]){
        JourneyDetailsMapAnnotation *jdma = (JourneyDetailsMapAnnotation *)annotation;
		pinView = (MKPinAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:[[JourneyDetailsMapAnnotation class] description]];
		if(pinView == nil) {
			MKAnnotationView *annotationView;
			annotationView =  [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:[[TransitStop class] description]];
			annotationView.opaque = NO;
            if(jdma.annotationType == kMinorMarker){
                annotationView.image = [[TransitModelUtilities sharedTransitModelUtilities] microIconForTransitType:[jdma type]];
                annotationView.leftCalloutAccessoryView = [[UIImageView alloc] initWithImage:[[TransitModelUtilities sharedTransitModelUtilities] smallestIconForTransitType:[jdma type]]];
            }else{
                if([jdma annotationType] == kMajorAndDestinationMarker){
                    annotationView.image = [UIImage imageNamed:@"destination_icon.png"];
                    annotationView.leftCalloutAccessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"destination_icon_smallest.png"]];
                }else{
                    annotationView.image = [[TransitModelUtilities sharedTransitModelUtilities] smallIconForTransitType:[jdma type]];
                    annotationView.leftCalloutAccessoryView = [[UIImageView alloc] initWithImage:[[TransitModelUtilities sharedTransitModelUtilities]  smallestIconForTransitType:[jdma type]]];
                }
            }
            
            annotationView.canShowCallout = YES;
            return  annotationView;
        }else{ //We got an annotation, so we reuse it
            pinView.annotation = annotation;
            pinView.leftCalloutAccessoryView = [[UIImageView alloc] initWithImage:[[TransitModelUtilities sharedTransitModelUtilities] smallestIconForTransitType:[jdma type]]];
            if(jdma.annotationType == kMinorMarker){
                pinView.image = [[TransitModelUtilities sharedTransitModelUtilities] microIconForTransitType:[jdma type]];
            }else{
                if([jdma annotationType] == kMajorAndDestinationMarker){
                    pinView.image = [UIImage imageNamed:@"destination_icon.png"];
                    pinView.leftCalloutAccessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"destination_icon_smallest.png"]];
                }else{
                    pinView.image = [[TransitModelUtilities sharedTransitModelUtilities] smallIconForTransitType:[jdma type]];
                }
            }
            return  pinView;
        }
    }
    return nil; //it isn't one we are interested in, so we just go with it.
}


//resizes an image to fit. We aren't currently, and probably won't use this
-(UIImage *) generateSizedThumbnailFromImage:(UIImage *) img{
    CGRect resizeRect;
    resizeRect.size.width = 32.0;
    resizeRect.size.height = 32.0;//image should be about this many pixels on each side
    CGSize maxSize = CGRectInset(self.view.bounds,
                                 10.0f,
                                 10.0f).size;
    maxSize.height -= self.navigationController.navigationBar.frame.size.height + 32.0f;
    if (resizeRect.size.width > maxSize.width)
        resizeRect.size = CGSizeMake(maxSize.width, resizeRect.size.height / resizeRect.size.width * maxSize.width);
    if (resizeRect.size.height > maxSize.height)
        resizeRect.size = CGSizeMake(resizeRect.size.width / resizeRect.size.height * maxSize.height, maxSize.height);
    resizeRect.origin = (CGPoint){0.0f, 0.0f};
    UIGraphicsBeginImageContextWithOptions(resizeRect.size,NO,[[UIScreen mainScreen ]scale]);  //use scale to deal with retina
    [img drawInRect:resizeRect];
    UIImage *resizedThumbnailImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return resizedThumbnailImage;
}


-(MKCoordinateRegion) coordinateRegionThatSpansJourney{
    
    double minLat, minLon, maxLat, maxLon;
    //get the min and max lats and lons
    
    JourneyLeg *legOne = [[_journeyToDisplay journeyLegs] objectAtIndex:0];
    NSArray * polyArr = [legOne polyline];
    CLLocation * loc = [polyArr objectAtIndex:0];
    minLat = loc.coordinate.latitude;
    maxLat = loc.coordinate.latitude;
    minLon  = loc.coordinate.longitude;
    maxLat = loc.coordinate.longitude;
    
    for(JourneyLeg * leg in _journeyToDisplay.journeyLegs){
        for(CLLocation *loc in [leg polyline]){
            minLat = minLat < loc.coordinate.latitude ? minLat : loc.coordinate.latitude;
            maxLat = maxLat > loc.coordinate.latitude ? maxLat : loc.coordinate.latitude;
            minLon = minLon < loc.coordinate.longitude ? minLon : loc.coordinate.longitude;
            maxLon = maxLon > loc.coordinate.longitude ? maxLon : loc.coordinate.longitude;
        }
    }
    CLLocationCoordinate2D centerCoord = CLLocationCoordinate2DMake(((maxLat -minLat)/2.0)+minLat, ((maxLon - minLon)/2.0)+minLon);
    MKCoordinateSpan coordSpan = MKCoordinateSpanMake((maxLat-minLat) +0.005, (maxLon-minLon) + 0.005);
    
    return MKCoordinateRegionMake(centerCoord, coordSpan);
    
}

///Methods to support the location button
- (IBAction) startShowingUserHeading:(id)sender{
    
    if(self.mapView.userTrackingMode == 0){
        [self.mapView setUserTrackingMode: MKUserTrackingModeFollow animated: YES];
        
        //Turn on the position arrow
        UIImage *buttonArrow = [UIImage imageNamed:@"LocationBlue.png"];
        [_userHeadingBtn setImage:buttonArrow forState:UIControlStateNormal];
        
    }
    else if(self.mapView.userTrackingMode == 1){
        [self.mapView setUserTrackingMode: MKUserTrackingModeFollowWithHeading animated: YES];
        
        //Change it to heading angle
        UIImage *buttonArrow = [UIImage imageNamed:@"LocationHeadingBlue.png"];
        [_userHeadingBtn setImage:buttonArrow forState:UIControlStateNormal];
    }
    else if(self.mapView.userTrackingMode == 2){
        [self.mapView setUserTrackingMode: MKUserTrackingModeNone animated: YES];
        
        //Put it back again
        UIImage *buttonArrow = [UIImage imageNamed:@"LocationGrey.png"];
        [_userHeadingBtn setImage:buttonArrow forState:UIControlStateNormal];
    }
}

-(void)mapView:(MKMapView *)mapView didChangeUserTrackingMode:(MKUserTrackingMode)mode animated:(BOOL)animated{
    if(self.mapView.userTrackingMode == 0){
        [self.mapView setUserTrackingMode: MKUserTrackingModeNone animated: YES];
        
        //Put it back again
        UIImage *buttonArrow = [UIImage imageNamed:@"LocationGrey.png"];
        [_userHeadingBtn setImage:buttonArrow forState:UIControlStateNormal];
    }
    
}

-(IBAction)changeMapType:(id)sender {
    UISegmentedControl * mapControl = (UISegmentedControl *) sender;
    if([mapControl selectedSegmentIndex] == 0){
        [_mapView setMapType:MKMapTypeStandard];
    }else{
        [_mapView setMapType:MKMapTypeHybrid];
    }
    
}

- (void)viewDidUnload {
    [self setMapView:nil];
    [super viewDidUnload];
}

@end

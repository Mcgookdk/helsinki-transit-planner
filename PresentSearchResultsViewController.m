//
//  PresentSearchResultsViewController.m
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 16/05/2013.
//  Copyright (c) 2013 David McGookin. All rights reserved.
//

#import "PresentSearchResultsViewController.h"
#import "ApplicationUtilities.h"
#import <MapKit/MapKit.h>
//#import "GADMasterViewController.h"

@implementation PresentSearchResultsViewController

-(void) viewDidLoad{
    [super viewDidLoad];
    [_tableView setDataSource:self];
    [_tableView setDelegate:self];
   
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    if(_locationType == kArrival){
        [[self navigationItem] setTitle:@"Select Destination"];
    }else{
        [[self navigationItem] setTitle:@"Select Departure"];
    }
    
    [self.view setBackgroundColor:[UIColor yellowColor]];
    
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
#ifdef COMPILED_AS_LITE_VERSION
    //if it is the free version, put in the add view
    [[GADMasterViewController sharedGADMasterViewController] resetAdView:self inFrame:[_dummyAdSpacer frame]];
#endif
#ifdef COMPILED_AS_FULL_VERSION
    [_dummyAdSpacer removeFromSuperview]; //if this is the full version, remove the spacer bar for the add, and adjust the rest of the view accordingly
    CGRect tableViewFrame = [_tableView frame];
    tableViewFrame.origin.y -= _dummyAdSpacer.frame.size.height;
    tableViewFrame.size.height += _dummyAdSpacer.frame.size.height;
    [_tableView setFrame:tableViewFrame];
#endif

}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SearchResult"];
    if(cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"SearchResult"];
        [[cell textLabel] setFont:[ApplicationUtilities defaultApplicationFontForTableCell]];
        [cell setBackgroundColor:[ApplicationUtilities defaultImageColorForTableViewTextureBackround]];
    }    
        MKMapItem *mapItem = [_results objectAtIndex:[indexPath indexAtPosition:1]];
        [[cell textLabel] setText:[mapItem name]];
        [[cell detailTextLabel] setText:mapItem.placemark.addressDictionary[@"Street"]];

       
    return cell;
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_results count];
}

-(void) tableView:(UITableView *) tableView didSelectRowAtIndexPath: (NSIndexPath *) indexPath{
    MKMapItem *mapItem = [_results objectAtIndex:[indexPath indexAtPosition:1]];
    GeoPlace * selectedPlace = [[GeoPlace alloc] init];
    selectedPlace.name = [mapItem name];
    selectedPlace.coordinate = [[mapItem placemark] coordinate];
    if(mapItem.placemark.addressDictionary[@"Street"] != nil){
        selectedPlace.address = mapItem.placemark.addressDictionary[@"Street"];
    }
    
    [self dismissViewControllerAnimated:YES completion:^{
        if([_delegate respondsToSelector:@selector(userSelectedPlace:forLocationType:)]){
            [_delegate userSelectedPlace:selectedPlace forLocationType:_locationType];
        }
    }];
}



- (void)viewDidUnload {
    [self setNavigationBar:nil];
    [self setTableView:nil];
    [super viewDidUnload];
}
@end

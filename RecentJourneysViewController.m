//
//  RecentJourneysViewController.m
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 21/03/2013.
//  Copyright (c) 2013 David McGookin. All rights reserved.
//

#import "RecentJourneysViewController.h"
#import "ApplicationUtilities.h"
#import "RecentJourneyCellView.h"

@interface RecentJourneysViewController ()

@end

@implementation RecentJourneysViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    
    [[self navigationItem] setTitle:NSLocalizedString(@"Select Journey", @"Select Journey Title view")];
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel", @"Cancel Button Text") style:UIBarButtonItemStylePlain target:self action:@selector(userDidCancel:)];
    [[self navigationItem] setLeftBarButtonItem:cancelButton];
    
       
    [_journeyTable setDataSource:self];
    [_journeyTable setDelegate:self];
    
    [_journeyTable registerNib:[UINib nibWithNibName:@"RecentJourneyCellView" bundle:nil]
            forCellReuseIdentifier:@"RecentJourneyCellView"];

    _timeFormatter = [[NSDateFormatter alloc] init];
    [_timeFormatter setDateFormat:@"HH:mm"]; //time only
    _dateFormatter = [[NSDateFormatter alloc] init];
    [_dateFormatter setDateFormat:@"d/MM/yy"]; //full month name

}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    RecentJourney *journey =   [[[RecentJourneyStore sharedRecentJourneyStore] recentJourneys] objectAtIndex:[indexPath indexAtPosition:1]] ;
    
    
    RecentJourneyCellView *cell = [tableView dequeueReusableCellWithIdentifier:@"RecentJourneyCellView"];
    if(cell == nil){
        cell = [[RecentJourneyCellView alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"RecentJourneyCellView"];
        [[cell textLabel] setFont:[ApplicationUtilities defaultApplicationFontForTableCell]];
    }
    [cell setBackgroundColor:[ApplicationUtilities defaultImageColorForTableViewTextureBackround]];
    [[cell dateLabel]  setText:[_dateFormatter stringFromDate:[journey transitDate]]];
    [[cell timeLabel] setText:[_timeFormatter stringFromDate:[journey transitDate]]];
    [[cell fromLabel] setText:[[journey departurePlace] name]];
    [[cell toLabel] setText:[[journey arrivalPlace] name]];
    [[cell fromLabelDescriptor] setText: NSLocalizedString(@"from:", @"From Label with Colon")];
    [[cell toLabelDescriptor] setText: NSLocalizedString(@"to:", @"To Label with Colon")];
    return cell;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [[[RecentJourneyStore sharedRecentJourneyStore] recentJourneys] count];
}

-(void) tableView:(UITableView *) tableView didSelectRowAtIndexPath: (NSIndexPath *) indexPath{
    if ([_delegate respondsToSelector:@selector(userDidSelectRecentJourney:)]) {
        [_delegate userDidSelectRecentJourney:[[[RecentJourneyStore sharedRecentJourneyStore] recentJourneys] objectAtIndex:[indexPath indexAtPosition:1]]];
    }
     [self dismissViewControllerAnimated:YES completion: nil];
 
}


- (IBAction)userDidCancel:(id)sender {
    if ([_delegate respondsToSelector:@selector(userDidCancelSelection)]) {
        [_delegate userDidCancelSelection];
    }
    [self dismissViewControllerAnimated:YES completion: nil];
}



- (void)viewDidUnload {
    [self setJourneyTable:nil];
    [super viewDidUnload];
}
@end

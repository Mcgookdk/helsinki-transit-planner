//
//  TransitRouteViewController.m
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 28/10/2012.
//  Copyright (c) 2012 David McGookin. All rights reserved.
//

#import "TransitRouteViewController.h"
#import "ReittiopasQueryProcessor.h"
#import "TransitModelUtilities.h"
#import "MBProgressHUD.h"
//#import "GADMasterViewController.h"

@implementation TransitRouteViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    
   
        self.edgesForExtendedLayout = UIRectEdgeNone;
        
        [_mapTypeSelectionControl removeFromSuperview];
        [_userHeadingBtn removeFromSuperview];
        [_toolbarMapTypeSegmentedControl setTitle:NSLocalizedString(@"Map", @"Map Segmented Control Label") forSegmentAtIndex:0];
        [_toolbarMapTypeSegmentedControl setTitle:NSLocalizedString(@"Satellite", @"Satellite Segmented Control Label") forSegmentAtIndex:1];
        _userHeadingBarButton = [[MKUserTrackingBarButtonItem alloc] initWithMapView:_mapView];
        NSMutableArray *items =[[NSMutableArray alloc] initWithArray:[_toolbar items]];
        [items removeObjectAtIndex:0];
        
        [items insertObject:_userHeadingBarButton atIndex:0];
        [_toolbar setItems:items animated:YES];
        
        
    [[self navigationItem] setTitle:[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"Service", @"Service String"),[_service userReadableCode]]];
    [_mapView setDelegate:self];
    [_mapView setShowsUserLocation:YES];
    MBProgressHUD  * progressHUD =  [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.view addSubview:progressHUD];
    [progressHUD setLabelText:NSLocalizedString(@"Loading Route", @"Loading Route String")];
    [progressHUD show:YES];
    
    //set the text for the satelite / map button
    [_mapTypeSelectionControl setTitle:NSLocalizedString(@"Map", @"Map Segmented Control Label") forSegmentAtIndex:0];
       [_mapTypeSelectionControl setTitle:NSLocalizedString(@"Satellite", @"Satellite Segmented Control Label") forSegmentAtIndex:1];
    
   //Start the query
    ReittiopasQueryProcessor * _queryProcessor = [[ReittiopasQueryProcessor alloc] init];
    [_queryProcessor findFullRouteForService:_service.code withCompletionBlockHandler:^(TransitRoute * transitRoute, ReturnCodeType retCode) {
        
        [progressHUD hide:YES];

        if(retCode == kNoResults || retCode == kNetworkError){
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Download Error", @"Download Error Alert Title") message:NSLocalizedString(@"The selected line could not be found", @"Download Error Alert Message") delegate:nil cancelButtonTitle:NSLocalizedString(@"Dismiss", @"Dismiss Button Title")  otherButtonTitles: nil];
            [alertView show];
        }else{
 
        _route = transitRoute;
        //add the stops as Annotations
        for(TransitStop * stop in [_route stops]){
            [_mapView addAnnotation:stop];
        }
        [self addRouteToMap]; //also sets visible map rect
        [[self mapView] setVisibleMapRect:_visibleMapRect edgePadding:UIEdgeInsetsMake(10.0, 10.0, 10.0, 10.0) animated:NO];
        }
    }];
    
#ifdef COMPILED_AS_FULL_VERSION
    [_dummyAdSpacer removeFromSuperview]; //if this is the full version, remove the spacer bar for the add, and adjust the rest of the view accordingly
    [self.mapView setFrame:[self.view frame]];
    
#endif
    
    
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
#ifdef COMPILED_AS_LITE_VERSION
    //if it is the free version, put in the add view
    [[GADMasterViewController sharedGADMasterViewController] resetAdView:self inFrame:[_dummyAdSpacer frame]];
#endif
}



-(void) addRouteToMap{
    
	MKMapPoint * mapPointsArr = malloc(sizeof(MKMapPoint) * [[_route route ] count]);
	
	int i = 0;
	for(CLLocation * loc in [_route route]){
        MKMapPoint  mapPoint = MKMapPointForCoordinate(loc.coordinate);
        mapPointsArr[i] = mapPoint;
        i++;
	}
	
	MKPolyline *routePolyline = [MKPolyline polylineWithPoints:mapPointsArr count:[[_route route]count]];
	[_mapView addOverlay:routePolyline];
    _visibleMapRect = [routePolyline boundingMapRect];
     free(mapPointsArr); //clean this up, its a lot of memory
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation{
	
	MKPinAnnotationView *pinView = nil;
	
	if([annotation isMemberOfClass:[TransitStop class]]){
		pinView = (MKPinAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:[[TransitStop class] description]];
		if(pinView == nil) {
			MKAnnotationView *annotationView;
			annotationView =  [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:[[TransitStop class] description]];
			annotationView.opaque = NO;
            annotationView.image = [[TransitModelUtilities sharedTransitModelUtilities] microIconForTransitType:[_service type]];
            annotationView.canShowCallout = YES;
            return  annotationView;
        }else{
            pinView.annotation = annotation;
            pinView.image = [[TransitModelUtilities sharedTransitModelUtilities] microIconForTransitType:[_service type]];
            return  pinView;
        }
    }
    return nil;
}

- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id <MKOverlay>)overlay{
    MKPolylineView *polylineView;
    polylineView = [[MKPolylineView	 alloc] initWithPolyline: overlay];
    [polylineView setFillColor:[[TransitModelUtilities  sharedTransitModelUtilities] colorForTransitType:[_service type]]];
    [polylineView setStrokeColor:[[TransitModelUtilities sharedTransitModelUtilities] colorForTransitType:[_service type]]];
    [polylineView setLineWidth:5.0f];
    return polylineView;
}

///Methods to support the location button
- (IBAction) startShowingUserHeading:(id)sender{
    if(self.mapView.userTrackingMode == 0){
        [self.mapView setUserTrackingMode: MKUserTrackingModeFollow animated: YES];
        
        //Turn on the position arrow
        UIImage *buttonArrow = [UIImage imageNamed:@"LocationBlue.png"];
        [_userHeadingBtn setImage:buttonArrow forState:UIControlStateNormal];
        
    }else if(self.mapView.userTrackingMode == 1){
        [self.mapView setUserTrackingMode: MKUserTrackingModeFollowWithHeading animated: YES];
        
        //Change it to heading angle
        UIImage *buttonArrow = [UIImage imageNamed:@"LocationHeadingBlue.png"];
        [_userHeadingBtn setImage:buttonArrow forState:UIControlStateNormal];
    }else if(self.mapView.userTrackingMode == 2){
        [self.mapView setUserTrackingMode: MKUserTrackingModeNone animated: YES];
        
        //Put it back again
        UIImage *buttonArrow = [UIImage imageNamed:@"LocationGrey.png"];
        [_userHeadingBtn setImage:buttonArrow forState:UIControlStateNormal];
    }
}

- (IBAction)changeMapType:(id)sender {
    if([sender selectedSegmentIndex] == 0){
        [_mapView setMapType:MKMapTypeStandard];
    }else{
        [_mapView setMapType:MKMapTypeHybrid];
    }

}

- (void)mapView:(MKMapView *)mapView didChangeUserTrackingMode:(MKUserTrackingMode)mode animated:(BOOL)animated{
    if(self.mapView.userTrackingMode == 0){
        [self.mapView setUserTrackingMode: MKUserTrackingModeNone animated: YES];
        
        //Put it back again
        UIImage *buttonArrow = [UIImage imageNamed:@"LocationGrey.png"];
        [_userHeadingBtn setImage:buttonArrow forState:UIControlStateNormal];
    }
    
}



- (void)viewDidUnload {
    [self setMapTypeSelectionControl:nil];
    [super viewDidUnload];
}
@end

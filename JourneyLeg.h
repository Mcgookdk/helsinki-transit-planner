//
//  JourneyLeg.h
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 27/12/2012.
//  Copyright (c) 2012 David McGookin. All rights reserved.
//
//  Stores one leg of a planned journey

#import <Foundation/Foundation.h>
#import "TransitStop.h"
#import "TransitRoute.h"
#import "transitService.h"
#import "JourneyLegStop.h"

@interface JourneyLeg : NSObject


@property(readwrite, nonatomic) TransitType transitType; //the form of transport used on this leg
@property(strong, nonatomic) NSDate * departureDate;
@property(strong, nonatomic) NSDate * arrivalDate;
@property(strong, nonatomic) NSString *departureName;
@property(strong, nonatomic) NSString *arrivalName;
@property(strong, nonatomic) NSString *departureStopCode; //may be null //currently doesn't exist
@property(strong, nonatomic) NSString *arrivalStopCode;// may be null //currently doesn't exist
@property(strong, nonatomic) TransitService *service;// may be nil if we are walking. Check transit type
@property (readwrite, nonatomic) int legDurationInSeconds;
@property(strong, nonatomic) NSArray *polyline;
@property(readwrite, nonatomic) double legLengthInMeters;
@property (strong, nonatomic) NSArray * stopsOnLeg; //nil for a walking segment

-(NSString *) stringRepresentation;
-(TransitStop *) transitStopFromLeg;
-(NSArray *) journeyLegAsJourneyLegStops;
-(JourneyLegStop *) arrivalJourneyLegStop;
-(JourneyLegStop *) departureJourneyLegStop;

@end

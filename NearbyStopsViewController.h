//
//  NearbyStopsViewController.h
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 21/10/2012.
//  Copyright (c) 2012 David McGookin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReittiopasQueryProcessor.h"
#import "EGORefreshTableHeaderView.h"
#import "LocationServer.h"

@interface NearbyStopsViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, LocationServerDelegate>{
    NSArray * _nearbyStops;
    ReittiopasQueryProcessor * _queryProcessor;
    EGORefreshTableHeaderView *_refreshHeaderView;
	BOOL _reloading;
    ReturnCodeType _lastReturnCode;
    NSDate * _lastUpdateTime;
    LocationServer *_locationServer;
    CLLocation *_lastValidLocation;
    BOOL _haveNeverBeenUpdated;
    CLAuthorizationStatus _currentAuthorisationStatus;

}

@property (weak, nonatomic) IBOutlet UITableView *nearbyStopsTable;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UIView *dummyAdSpacerBar;

- (void)dataSourceDidFinishLoadingNewDataWithSucessStatus:(BOOL) updateSuceeded;

@end

//
//  JourneyRoute.m
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 27/12/2012.
//  Copyright (c) 2012 David McGookin. All rights reserved.
//

#import "JourneyRoute.h"

@implementation JourneyRoute

-(NSString *) stringRepresentation{
    NSString *tempStr = @"+JOURNEY START\n";
    
    for(JourneyLeg * tempLeg in _journeyLegs){
        tempStr = [tempStr stringByAppendingString:[tempLeg stringRepresentation]];
    }
    return tempStr;
}

-(NSString *) journeyDepartureName{
    JourneyLeg * leg = [_journeyLegs objectAtIndex:0];
    return leg.departureName;
}

-(NSString *) journeyDestinationName{
    JourneyLeg * leg = [_journeyLegs lastObject];
    return leg.arrivalName;
}

-(NSArray *) journeyAsJourneyLegStops{
    
    NSMutableArray * journey = [[NSMutableArray alloc] init];
    
    
    for(JourneyLeg *j in _journeyLegs){
        [journey addObjectsFromArray:[j journeyLegAsJourneyLegStops]];
    }
    
    [journey addObject:[[_journeyLegs lastObject] arrivalJourneyLegStop]];
    return journey;
}
@end

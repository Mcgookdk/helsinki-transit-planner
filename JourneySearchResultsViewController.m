//
//  JourneySearchResultsViewController.m
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 28/12/2012.
//  Copyright (c) 2012 David McGookin. All rights reserved.
//

#import "JourneySearchResultsViewController.h"
#import "JourneyDetailsViewController.h"
#import "ApplicationUtilities.h"
//#import "GADMasterViewController.h"
#import "JourneySearchResultsCell.h"



@implementation JourneySearchResultsViewController

- (void)viewDidLoad{
    [super viewDidLoad];
   
    
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.automaticallyAdjustsScrollViewInsets = NO;
    //    [_departureLocation setTextColor:[ApplicationUtilities defaultApplicationHighlightColor]];
     //   [_destinationLocation setTextColor:[ApplicationUtilities defaultApplicationHighlightColor]];
      //    [_fromLabel setTextColor:[ApplicationUtilities defaultApplicationHighlightColor]];
      //[_toLabel setTextColor:[ApplicationUtilities defaultApplicationHighlightColor]];
    
    [self.navigationItem setTitleView:_resultsHeadingView];
    [_tableView setDataSource:self];
    [_tableView setDelegate:self];
    [_tableView registerNib:[UINib nibWithNibName:@"JourneySearchResultsCell" bundle:nil]
             forCellReuseIdentifier:@"JourneySearchResultsCell"];
    [_tableView setBackgroundColor:[ApplicationUtilities defaultImageColorForTableViewTextureBackround]];
#ifdef COMPILED_AS_FULL_VERSION
    [_dummyAdSpacer removeFromSuperview]; //if this is the full version, remove the spacer bar for the add, and adjust the rest of the view accordingly
    [_tableView setFrame:[self.view frame]];
#endif
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
#ifdef COMPILED_AS_LITE_VERSION
    //if it is the free version, put in the add view
    [[GADMasterViewController sharedGADMasterViewController] resetAdView:self inFrame:[_dummyAdSpacer frame]];
#endif
    
    JourneyRoute *journeyRoute = [_journeys objectAtIndex:0];
    [_departureLocation setText:[journeyRoute journeyDepartureName]];
    [_destinationLocation setText:[journeyRoute journeyDestinationName]];
    
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
   // [Flurry logEvent:@"JOURNEY_SEARCH_RESULTS_VIEW_DID_APPEAR"];
}

- (void)viewDidUnload{
    [self setTableView:nil];
    [self setResultsHeadingView:nil];
    [self setDepartureLocation:nil];
    [self setDestinationLocation:nil];
    [super viewDidUnload];
}

//uitableview datasource delegates
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    JourneySearchResultsCell  *cell;
    
    cell = [tableView dequeueReusableCellWithIdentifier:@"JourneySearchResultsCell"];
    if (cell == nil){ //i.e. there isn't a cell already available, make a new one
    		cell = [[JourneySearchResultsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"JourneySearchResultsCell"];//
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    [cell setJourneyRoute:[_journeys objectAtIndex:[indexPath indexAtPosition:1]]];
    [cell updateJourneyCell];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 90.0; //check this with JourneySearchResultsCellViewController. They need to be the same
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_journeys count];
}

-(void) tableView:(UITableView *) tableView didSelectRowAtIndexPath: (NSIndexPath *) indexPath{
    JourneyDetailsViewController *detailsViewController = [[JourneyDetailsViewController alloc] init];
    [detailsViewController setJourneyToDisplay:[_journeys objectAtIndex:[indexPath indexAtPosition:1]]];
    [self.navigationController pushViewController:detailsViewController animated:YES];
}



@end

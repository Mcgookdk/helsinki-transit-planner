//
//  RecentJourneyStore.h
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 21/03/2013.
//  Copyright (c) 2013 David McGookin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RecentJourney.h"
#import "GeoPlace.h"

@interface RecentJourneyStore : NSObject


@property (strong, nonatomic) NSMutableArray * recentJourneys;

+(RecentJourneyStore *) sharedRecentJourneyStore; //don't create using init. ask for the shared instance

-(void) addJourney:(RecentJourney *) journey;


@end

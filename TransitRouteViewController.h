//
//  TransitRouteViewController.h
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 28/10/2012.
//  Copyright (c) 2012 David McGookin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <MapKit/MapKit.h>
#import "TransitRoute.h"
#import "TransitService.h"


@interface TransitRouteViewController : UIViewController<MKMapViewDelegate>{
    UIBarButtonItem *_userHeadingBarButton;
    
}

@property (strong, nonatomic) TransitRoute *route;
@property (strong, nonatomic) TransitService *service;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (readwrite, nonatomic) MKMapRect visibleMapRect;
@property(weak, nonatomic) IBOutlet UIButton *userHeadingBtn;
@property (weak, nonatomic) IBOutlet UISegmentedControl *mapTypeSelectionControl;
@property (weak, nonatomic) IBOutlet UIView *dummyAdSpacer;
@property (weak, nonatomic) IBOutlet UIToolbar *toolbar;
@property (weak, nonatomic) IBOutlet UISegmentedControl *toolbarMapTypeSegmentedControl;

-(void) addRouteToMap;

- (IBAction) startShowingUserHeading:(id)sender;
- (IBAction)changeMapType:(id)sender;

@end
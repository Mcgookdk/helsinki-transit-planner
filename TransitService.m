//
//  TransitService.m
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 20/10/2012.
//  Copyright (c) 2012 David McGookin. All rights reserved.
//

#import "TransitService.h"

@implementation TransitService

-(id) init{
    self = [super init];
    _transitType = kUnknownTransitType;
    _userReadableCode = nil;
    return self;
}
-(NSString *) userReadableCode{ //this a actually pretty complicated 
    
    if(_userReadableCode == nil){
NSString *tempStr =  [[self code] substringWithRange:NSMakeRange(1,3)]; //the 2,3,4 chars are the code used on nuses, trains etc. Obviously we start counting a 0
        int numericCodePart = [tempStr intValue];
         char firstDigitCharacter = [[self code] characterAtIndex:0]; //sometimes important to distinguish
        char fourthDigitCharacter = [[self code] characterAtIndex:4];
        
        if((numericCodePart == 300) && (firstDigitCharacter!='4')){//then it is a metro
            _userReadableCode = @"Metro";
        } else if ([[self code] characterAtIndex:4] != ' '){ //if there is an additional char code then use it
            _userReadableCode = [NSString stringWithFormat:@"%d%c", numericCodePart, [[self code] characterAtIndex:4]];
        }else{
        _userReadableCode = [NSString stringWithFormat:@"%d", [tempStr intValue]];
        }
        if ([self type] == kTrain){ //use on the letter code for this
            _userReadableCode = [NSString stringWithFormat:@"%c", fourthDigitCharacter];
        }
    }
    return _userReadableCode;
}


-(TransitType) type{
    if(_transitType == kUnknownTransitType){
        int numericServiceCode = [[[self code] substringWithRange:NSMakeRange(1,3)] intValue];// we can use the 3 letter code for a basic idea of what service we are using
      char firstDigitCharacter = [[self code] characterAtIndex:0]; //sometimes important to distinguish between trains and trams
        if((numericServiceCode <=10) && (firstDigitCharacter != '3')){
            _transitType = kTram;
        }else if((numericServiceCode <=10) && (firstDigitCharacter == '3')){
            _transitType = kTrain;
        }else if ((numericServiceCode == 300) && (firstDigitCharacter != '4')){
            _transitType = kMetro;
        }else if(numericServiceCode == 19){
            _transitType = kFerry;
        }else{
            _transitType = kBus;
        }
    }
    return _transitType;
}


-(NSString *) stringRepresentation{
    return [NSString stringWithFormat:@"Service Code:%@, Short Code:%@, Destination:%@, Date:%@", self.code, self.userReadableCode, self.destination, self.departureDate];
}

//returns an NSComparisonResult based ont he departure time of this and the other Transit Service
-(NSComparisonResult) departureDateComparator:(TransitService *)otherService{
    NSLog(@"COMPARATOR CALLED");
    return [_departureDate compare:[otherService departureDate]];
}

@end

//
//  JourneyDetailsViewController.m
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 13/02/2013.
//  Copyright (c) 2013 David McGookin. All rights reserved.
//

#import "JourneyDetailsViewController.h"
//#import "GADMasterViewController.h"
#import "ApplicationUtilities.h"
#import "TransitModelUtilities.h"

@interface JourneyDetailsViewController ()
-(void) showHideListView:(id) sender;
-(NSString *) generateJourneyEmailBodyFromJourney;
-(NSString *) generateJourneySMSFromJourney;
@end

@implementation JourneyDetailsViewController


- (void)viewDidLoad{
    [super viewDidLoad];
            self.edgesForExtendedLayout = UIRectEdgeNone;
        self.automaticallyAdjustsScrollViewInsets = NO;
        
  
    
        _showHideListViewButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"887-notepad.png"] style:UIBarButtonItemStyleBordered target:self action:@selector(showHideListView:)];
       // [_departureLocationLabel setTextColor:[ApplicationUtilities defaultApplicationHighlightColor]];
        //[_destinationLocationLabel setTextColor:[ApplicationUtilities defaultApplicationHighlightColor]];
         // [_fromLabel setTextColor:[ApplicationUtilities defaultApplicationHighlightColor]];
         // [_toLabel setTextColor:[ApplicationUtilities defaultApplicationHighlightColor]];
    
        
    _shareJourneyDetailsButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(shareJourneyDetails)];
    [[self.navigationItem leftBarButtonItem] setTitle:@"Back"];
    [self.navigationItem setRightBarButtonItems:@[_shareJourneyDetailsButton, _showHideListViewButton]]; //first use of new obj-c shorthand syntax

    [self.view setBackgroundColor:[UIColor yellowColor]];
    _journeyDetailsList = [[JourneyDetailsListViewController alloc] init];
    [_journeyDetailsList setJourneyToDisplay:_journeyToDisplay];
    _journeyDetailsMap = [[JourneyDetailsMapViewController alloc] init];
    [_journeyDetailsMap setJourneyToDisplay:_journeyToDisplay];
    [self.view addSubview:_journeyDetailsMap.view];
    [_departureLocationLabel setText:[_journeyToDisplay journeyDepartureName]];
    [_destinationLocationLabel setText:[_journeyToDisplay journeyDestinationName]];
    [self.navigationItem setTitleView:_headerView];
    _firstTime = YES;
    [_journeyDetailsList setDelegate:self];
#ifdef COMPILED_AS_FULL_VERSION
    [_dummyAdSpacer removeFromSuperview]; //if this is the full version, remove the spacer bar for the add, and adjust the rest of the view accordingly
#endif
    //Set the frame to allow the entire list view to be scrolled
    CGRect frame = [self.view frame];
    
    
    [_journeyDetailsMap.view setFrame:frame];
    
    frame.size.height -= 160.0;//self.tabBarController.tabBar.frame.size.height; //Need to take a bit off as we now go under the tab bar
    [_journeyDetailsList.view setFrame:frame];

}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
#ifdef COMPILED_AS_LITE_VERSION
    [[GADMasterViewController sharedGADMasterViewController] resetAdView:self inFrame:[_dummyAdSpacer frame]];
    #endif
    
   
    
    
       //because we have added the following as subviews, we need to explicitly call viewWillAppear. mostly for the map view to sort out its bounds and size
    [_journeyDetailsList viewWillAppear:NO];
    [_journeyDetailsMap viewWillAppear:NO];
}


-(void) userDidSelectJourneyLegStop:(JourneyLegStop *) stop{ //if the user taps an item in the list, then center the map on that location
    [[_journeyDetailsMap mapView] setCenterCoordinate:[stop location] animated:YES];
}
    
  
    
    
-(void) showHideListView:(id) sender{
    if(_listViewIsVisible){
        [_journeyDetailsList.view removeFromSuperview];
        [self.view setNeedsDisplay];
       
        
               [_showHideListViewButton setImage:[UIImage imageNamed:@"887-notepad.png"]];
              self.view.tintAdjustmentMode = UIViewTintAdjustmentModeNormal;
             _journeyDetailsMap.view.tintAdjustmentMode = UIViewTintAdjustmentModeNormal;
            }else{
        [self.view addSubview:_journeyDetailsList.view];
       
            [_showHideListViewButton setImage:[UIImage imageNamed:@"852-map.png"]];
            self.view.tintAdjustmentMode = UIViewTintAdjustmentModeDimmed;
            _journeyDetailsMap.view.tintAdjustmentMode = UIViewTintAdjustmentModeDimmed;
           }
    _listViewIsVisible = ! _listViewIsVisible;
}


-(void) shareJourneyDetails{
    NSLog(@"SHARING THE JOURNEY DETAILS");
    UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:@"Share Journey Details via" delegate:self cancelButtonTitle:@"Dismiss" destructiveButtonTitle:nil otherButtonTitles:@"SMS", @"email", nil];
    [actionSheet showFromTabBar:self.tabBarController.tabBar];
}


-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
  
    MFMailComposeViewController *mailViewController;
    MFMessageComposeViewController *messageViewController; //both of these need to be here as they cannot be declared within the switch statement
    
    switch(buttonIndex){
        case 0:
            //sms
            if (![MFMessageComposeViewController canSendText]){
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Cannot Text Journey" message:@"You iOS device cannot send SMS messages" delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil];
                [alertView show];
                return;
            }
            
            
            messageViewController = [[MFMessageComposeViewController alloc] init];
            [messageViewController setBody:[self generateJourneySMSFromJourney]];
            [messageViewController setMessageComposeDelegate:self];
            [self presentViewController: messageViewController animated:YES completion:nil];
            break;
            
        case 1:
            //email
            if(![MFMailComposeViewController canSendMail]){
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Cannot Mail Journey" message:@"You have not configured an email account on this device to send an email" delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil];
                [alertView show];
                return;
            }
            
            mailViewController = [[MFMailComposeViewController alloc] init];
            [mailViewController setSubject:@"HELTran Journey Details"];
            [mailViewController setMessageBody:[self generateJourneyEmailBodyFromJourney] isHTML:YES];
            [mailViewController setMailComposeDelegate:self];
            [self presentViewController:mailViewController animated:YES completion:nil];
       break;
        default:
            //?????
            break;
            
    };
}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error{
    [controller dismissViewControllerAnimated:YES completion:nil];
}

-(void) messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result{
    [controller dismissViewControllerAnimated:YES completion:nil];
}


-(NSString *) generateJourneySMSFromJourney{
   
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat:@"HH:mm"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    NSArray *journeyLegs = [_journeyToDisplay journeyAsJourneyLegStops];
    
    NSMutableString * journeyString = [[NSMutableString alloc] init];
    
    [journeyString appendString:[NSString stringWithFormat:@"Departure From: %@ at: %@ on: %@. \n", [_journeyToDisplay journeyDepartureName], [timeFormatter stringFromDate:[[journeyLegs objectAtIndex:0] departureDate]], [dateFormatter stringFromDate:[[journeyLegs objectAtIndex:0] departureDate]]]];
    [journeyString appendString:[NSString stringWithFormat:@"Arrival At: %@ at: %@.  Journey generated by HELTran.", [_journeyToDisplay journeyDestinationName],  [timeFormatter stringFromDate:[[journeyLegs objectAtIndex:[journeyLegs count]-1] departureDate]]]];
    return journeyString;
}
-(NSString *) generateJourneyEmailBodyFromJourney{
    
    BOOL inIntermediateList = NO;
    NSMutableString *journeyString = [[NSMutableString alloc] init];
    NSArray *journeyLegs = [_journeyToDisplay journeyAsJourneyLegStops];
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat:@"HH:mm"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    [journeyString appendString:@"<style TYPE=\"text/css\">ul, li { margin-left: 0; padding-left: 0; list-style-type: none } ol, li { list-style-type: none }</style>"];
    [journeyString appendString:@"<h2>Your Journey</h2>"];
    [journeyString appendString:@"<ul>"];
    [journeyString appendString:[NSString stringWithFormat:@"<li><b>Departure From:</b> %@ at: %@ on: %@</li>", [_journeyToDisplay journeyDepartureName], [timeFormatter stringFromDate:[[journeyLegs objectAtIndex:0] departureDate]], [dateFormatter stringFromDate:[[journeyLegs objectAtIndex:0] departureDate]]]];
    [journeyString appendString:[NSString stringWithFormat:@"<li><b>Arrival At:</b> %@ at: %@ </P><P></li>", [_journeyToDisplay journeyDestinationName],  [timeFormatter stringFromDate:[[journeyLegs objectAtIndex:[journeyLegs count]-1] departureDate]]]];
    [journeyString appendString:@"</ul>"];
    [journeyString appendString:@"<h2>Details</h2>"];
    
    NSString *arrivalString;
    NSString *transitTypeString;
    NSString *stopCodeString;
    [journeyString appendString:@"<UL style=\"list-style: none;\">"];
    for(JourneyLegStop * jls in journeyLegs){
        //NSMutableString * stopString = [[NSMutableString alloc] init];
        //figure out if this is an arrival stop
        
        transitTypeString =[NSString stringWithFormat:@"%@:", [[TransitModelUtilities sharedTransitModelUtilities] readableNameForTransitType:[jls transitType]]];
      
        if([jls stopCode] == nil){
            stopCodeString =@":";
        }else{
            stopCodeString = [NSString stringWithFormat:@" (%@):", [jls stopCode]];
        }
        
        if([jls stopType] == kArrivalStop){
            arrivalString =@"Arrive";
        }else{
            arrivalString = @"";
        }
        
        
        //intermediate stops are indented in an <ol> and italic
        if(jls == [journeyLegs lastObject]){
            NSLog(@"ARRIVAL LOCATION");
            if(inIntermediateList){
                inIntermediateList = NO;
                [journeyString appendString:@"</OL>"];
            }
            [journeyString appendString:[NSString stringWithFormat:@"<li><b>%@</b>%@%@ %@</li>", @"Arrive:", [jls name] ,stopCodeString, [timeFormatter stringFromDate:[jls departureDate]]]];
        }
        else if( [jls stopType] == kIntermediateStop){
            NSLog(@"INTERMEDIATE");
            [journeyString appendString:[NSString stringWithFormat:@"<li><i>%@%@ %@</i></li>", [jls name],  stopCodeString, [timeFormatter stringFromDate:[jls departureDate]]]];
    }else if(!([jls stopType] == kIntermediateStop)){// &&!([jls transitType] == kWalk)){ //is departure stop and not walking
            if(inIntermediateList){
                inIntermediateList = NO;
                [journeyString appendString:@"</OL>"];
            }
                            [journeyString appendString:[NSString stringWithFormat:@"<li><b>%@</b> %@%@ <i>%@</i> %@</li> <OL>", transitTypeString, [jls name], stopCodeString, arrivalString, [timeFormatter stringFromDate:[jls departureDate]]]];
            inIntermediateList = YES;
            NSLog(@"DEPARTURE NO WALK");
        }
        
    }
    [journeyString appendString:@"</UL>"];
    
    [journeyString appendString:@"This route was generated by <a href=\"https://itunes.apple.com/gb/app/heltran/id658300512?mt=8&uo=4\" target=\"itunes_store\">HELTran</a>"];
    return journeyString;
}
-(void)viewDidUnload {
    [self setHeaderView:nil];
    [self setDepartureLocationLabel:nil];
    [self setDestinationLocationLabel:nil];
    [super viewDidUnload];
}
@end

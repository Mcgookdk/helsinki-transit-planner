//
//  ServicesFromStopViewController.h
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 27/10/2012.
//  Copyright (c) 2012 David McGookin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "TransitStop.h"
#import "TransitService.h"
#import "ReittiopasQueryProcessor.h"
#import "EGORefreshTableHeaderView.h"

@interface ServicesFromStopViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, MKMapViewDelegate>{
    ReittiopasQueryProcessor * _queryProcessor;
    UIBarButtonItem * _favoriteStopButton;
    NSArray * _servicesFromThisStop;
    EGORefreshTableHeaderView *_refreshHeaderView;
	BOOL _reloading;
    ReturnCodeType _lastReturnCode;
    NSDate * _lastUpdateTime;
    BOOL _isFavoriteStop;
}

@property (weak, nonatomic) IBOutlet MKMapView *stopMapView;
@property (weak, nonatomic) IBOutlet UITableView *servicesTableView;
@property (strong, nonatomic) TransitStop * currentStop;
@property (strong, nonatomic) IBOutlet UIView *customTableHeaderView;
@property (weak, nonatomic) IBOutlet UILabel *customTableHeaderViewLabel;
@property (weak, nonatomic) IBOutlet UIView *dummyAdViewSpacer;

-(void) dataSourceDidFinishLoadingNewDataWithSucessStatus:(BOOL) updateSuceeded;
-(IBAction) favouriteStopButtonAction:(id) caller;


@end

//
//  JourneyLeg.m
//  HelsinkiTransitPlanner
//
//  Created by David McGookin on 27/12/2012.
//  Copyright (c) 2012 David McGookin. All rights reserved.
//

#import "JourneyLeg.h"

@implementation JourneyLeg


-(TransitStop *) transitStopFromLeg{ //returns a transit stop where this leg starts
    TransitStop * stop = [[TransitStop alloc] init];
    stop.location = [[[self polyline] objectAtIndex:0] coordinate];
    stop.name = self.departureName;
    stop.type = self.transitType;
    return stop;
}

-(NSString *) stringRepresentation{
    
    NSString * tempStr = [NSString stringWithFormat:@"- %@ at %@ to %@ at %@ withTransitType %d, takes %d seconds to travel %f meters\n", _departureName, _departureDate, _arrivalName, _arrivalDate, _transitType, _legDurationInSeconds, _legLengthInMeters];
    
    for(CLLocation * point in _polyline){
        NSString *str = [NSString stringWithFormat:@"\t %lf,%lf\n",point.coordinate.latitude, point.coordinate.longitude];
        tempStr = [tempStr stringByAppendingString:str];
    }
    
    return tempStr;
}

-(NSArray *) journeyLegAsJourneyLegStops{
    NSMutableArray * journeyLegs = [[NSMutableArray alloc] init];
    
    JourneyLegStop *jlsDep = [self departureJourneyLegStop];
    JourneyLegStop *jlsArrival = [self arrivalJourneyLegStop];
    [journeyLegs addObject:jlsDep];
   
    if(_transitType !=kWalk){
        
        for(JourneyLegStop *jls in _stopsOnLeg){ //set the codes incase we haven't
            [jls setUserReadableServiceCode:[_service userReadableCode]];
        }
        
        [journeyLegs addObjectsFromArray:_stopsOnLeg];
        [journeyLegs addObject:jlsArrival]; //if we are walking there is no arrival as such
    }else{
        jlsDep.legLengthInMeters = _legLengthInMeters;
    }
    
    return journeyLegs;
}

-(JourneyLegStop *) arrivalJourneyLegStop{
    JourneyLegStop * jls = [[JourneyLegStop alloc] init];
    jls.departureDate = self.arrivalDate;
    jls.name    = self.arrivalName;
    jls.stopType = kArrivalStop;
    jls.transitType = self.transitType;
    jls.stopCode = _arrivalStopCode;
    jls.userReadableServiceCode = [_service userReadableCode];
    jls.location =  [[[self polyline] lastObject] coordinate];
    
    return jls;
}
-(JourneyLegStop *) departureJourneyLegStop{
    
    JourneyLegStop * jls = [[JourneyLegStop alloc] init];
    jls.departureDate = self.departureDate;
    jls.name    = self.departureName;
    jls.stopType = kDepartureStop;
    jls.stopCode = _departureStopCode;
    jls.transitType = self.transitType;
    jls.userReadableServiceCode = [_service userReadableCode];
    jls.location =  [[[self polyline] objectAtIndex:0] coordinate];
    
    return  jls;
}
@end
